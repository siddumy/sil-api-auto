package smokeTestScripts;

import java.util.Map;


import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.gf.app.SiView;
import com.gf.app.Space;
import com.gf.util.GlobalVariables;
import com.gf.util.Helper;
import com.gf.util.InitaiteTestdata;


public class ManualCA {
	
	@BeforeClass
	public void initaiteData()
	{
		InitaiteTestdata.getInstance();
		Helper.getInstance().clearSILLogs();
	}
	@BeforeMethod
	public void VerifyServerConnection()
	{
		Helper.getInstance().VerfiyServerConnection();
	}
	
	@AfterMethod
	public void ReleaseLotAndDeleteChannel()
	{
		SiView.getInstance().releaseFutureLotHold(GlobalVariables.LotID);
		Space.getInstance().DeleteChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"));
		commonScripts.common.ReportEnd();
	}
	
	@Test(enabled=false)
	public void GF_SIL_TC003and004__SIL960And961_ReleasingCAs_withoutDefault()
	{	
			SoftAssert Assert= new SoftAssert();
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Smoke Test,ManualCA");
			Map<String,String> Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannel(Input.get("SpecLimitFile1"), Input.get("PDSetupFile1"), Input.get("Metrology_DCR1"), Input.get("Process_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyChannel(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationEvents(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("CA1")));
			Assert.assertTrue(commonScripts.common.ReuploadDCR(Input.get("Metrology_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"), Input.get("ManualCA1"),Input.get("ViolationComment1"),Input.get("ExpectionMgs")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			Assert.assertTrue(SiView.getInstance().getChamberInhibitStatus(GlobalVariables.EquipmentID,GlobalVariables.ChamberId));
			Assert.assertTrue(SiView.getInstance().cancelChamberInhibit(GlobalVariables.EquipmentID,GlobalVariables.ChamberId));
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();
			
	}
	

	@Test
	public void GF_SIL_TC007__SIL535_ManualRel_ReleaseEquipmentInhibit_violationcomment_with_Wrong_CH1()
	{	
	
			SoftAssert Assert= new SoftAssert();
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Smoke Test,ManualCA");
			Map<String,String> Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannel(Input.get("SpecLimitFile1"), Input.get("PDSetupFile1"), Input.get("Metrology_DCR1"), Input.get("Process_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyChannel(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationEvents(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("CA1")));
			Assert.assertTrue(commonScripts.common.ReuploadDCR(Input.get("Metrology_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"), Input.get("ManualCA1"),Input.get("ViolationComment1"),Input.get("ExpectionMgs")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			Assert.assertTrue(SiView.getInstance().getEquipmentInhibitStatus(Input.get("EquipmentID1")));
			Assert.assertTrue(SiView.getInstance().cancelEquipmentInhibit(Input.get("EquipmentID1")));
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();
			
	}


	@Test
	public void GF_SIL_TC008__SIL536_ManualRel_ReleaseChamberInhibit_violationcomment_with_Wrong_CH1()
	{	
			SoftAssert Assert= new SoftAssert();
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Smoke Test,ManualCA");
			Map<String,String> Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannel(Input.get("SpecLimitFile1"), Input.get("PDSetupFile1"), Input.get("Metrology_DCR1"), Input.get("Process_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyChannel(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationEvents(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("CA1")));
			Assert.assertTrue(commonScripts.common.ReuploadDCR(Input.get("Metrology_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"), Input.get("ManualCA1"),Input.get("ViolationComment1"),Input.get("ExpectionMgs")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			Assert.assertTrue(SiView.getInstance().getChamberInhibitStatus(GlobalVariables.EquipmentID,GlobalVariables.ChamberId));
			Assert.assertTrue(SiView.getInstance().cancelChamberInhibit(GlobalVariables.EquipmentID,GlobalVariables.ChamberId));
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();
			
	}
	
	@Test
	public void GF_SIL_TC010__SIL1079211R_Execute_RouteInhibitCA_forSingleLotWIPDCRandverifySiViewscreens()
	{	
			SoftAssert Assert= new SoftAssert();
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Smoke Test,ManualCA");
			Map<String,String> Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannel(Input.get("SpecLimitFile1"), Input.get("PDSetupFile1"), Input.get("Metrology_DCR1"), Input.get("Process_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyChannel(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationts(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("ValuationName1")));
			Assert.assertTrue(Space.getInstance().AddValuationEvents(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("CA1")));
			Assert.assertTrue(commonScripts.common.ReuploadDCR(Input.get("Metrology_DCR1")));
			Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"), Input.get("ManualCA1"),Input.get("ViolationComment1"),Input.get("ExpectionMgs")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			Assert.assertTrue(SiView.getInstance().getRouteInhibitStatus(GlobalVariables.RouteId));
			Assert.assertTrue(	SiView.getInstance().cancelRouteInhibit(GlobalVariables.RouteId));
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();
			
	}
	
	@Test
	public void GF_SIL_TC011__MSR_14_TC_2Perform_LotHoldCA_forMultiLot_andValidateClaimMemo()
	{	
			SoftAssert Assert= new SoftAssert();
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Smoke Test,ManualCA");
			Map<String,String> Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannel(Input.get("SpecLimitFile1"), Input.get("PDSetupFile1"), Input.get("Metrology_DCR1"), Input.get("Process_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyChannel(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationEvents(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("CA1")));
			Assert.assertTrue(commonScripts.common.ReuploadDCR(Input.get("Metrology_DCR1")));
			Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"), Input.get("ManualCA1"),Input.get("ViolationComment1"),Input.get("ExpectionMgs")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			Assert.assertTrue(SiView.getInstance().getFutureLotStatus(Input.get("LotID1").trim()));
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();
			
	}
	
}
