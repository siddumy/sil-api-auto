package smokeTestScripts;

import java.util.Map;


import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.gf.app.SiView;
import com.gf.app.Space;
import com.gf.util.GlobalVariables;
import com.gf.util.Helper;
import com.gf.util.InitaiteTestdata;


public class AutoCA {
	public static String Title;
	@BeforeClass
	public void initaiteData()
	{
		InitaiteTestdata.getInstance();
		Helper.getInstance().clearSILLogs();
	}
	@BeforeMethod
	public void VerifyServerConnection()
	{
		Helper.getInstance().VerfiyServerConnection();
	}
	
	@AfterMethod
	public void ReleaseLotAndDeleteChannel()
	{
		SiView.getInstance().releaseFutureLotHold(GlobalVariables.LotID);
		Space.getInstance().DeleteChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"));
		commonScripts.common.ReportEnd();
	}
	
	@Test
	public void GF_SIL_TC001__SIL741_ImplementCorrectHandling_WhenReleasing_OOC_OOS_Other_Responses()
	{	
			SoftAssert Assert= new SoftAssert();
		 	Title=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(Title,"Smoke Test,AutoCA");
			Map<String,String> Input=	commonScripts.common.Testdata(Title);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannel(Input.get("SpecLimitFile1"), Input.get("PDSetupFile1"), Input.get("Metrology_DCR1"), Input.get("Process_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyChannel(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationEvents(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("CA1")));
			Assert.assertTrue(commonScripts.common.ReuploadDCR(Input.get("Metrology_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			Assert.assertTrue(SiView.getInstance().getEquipmentInhibitStatus(GlobalVariables.EquipmentID));
			Assert.assertTrue(SiView.getInstance().cancelEquipmentInhibit(GlobalVariables.EquipmentID));
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();
			
	}
	
	
	@Test
	public void GF_SIL_TC002__SIL131_ReleaseLotAnd_ReleaseInhibitEquipmentby_SiView()
	{	
			SoftAssert Assert= new SoftAssert();
	 		Title=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(Title,"Smoke Test,AutoCA");
			Map<String,String> Input=	commonScripts.common.Testdata(Title);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannel(Input.get("SpecLimitFile1"), Input.get("PDSetupFile1"), Input.get("Metrology_DCR1"), Input.get("Process_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyChannel(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationts(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("ValuationName1")));
			Assert.assertTrue(	Space.getInstance().AddValuationEvents(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("CA1")));
			Assert.assertTrue(Space.getInstance().editControlLimits(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("ControlLimits")));
			Assert.assertTrue(commonScripts.common.ReuploadDCR(Input.get("Metrology_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			Assert.assertTrue(SiView.getInstance().getEquipmentInhibitStatus(GlobalVariables.EquipmentID));
			Assert.assertTrue(SiView.getInstance().cancelEquipmentInhibit(GlobalVariables.EquipmentID));
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();
			
	}
	
	@Test
	public void GF_SIL_TC005__SIL566_ListeningModeIgnoredBy_SIL_LOT_GroupingParameters()
	{
		SoftAssert Assert= new SoftAssert();
 		Title=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(Title,"Smoke Test,AutoCA");
		Map<String,String> Input=	commonScripts.common.Testdata(Title);
		Assert.assertTrue(Space.getInstance().ResetCAAttributes(Input.get("CA1")),"Removing CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateChannel(Input.get("SpecLimitFile1"), Input.get("PDSetupFile1"), Input.get("Metrology_DCR1"), Input.get("Process_DCR1")));
		Assert.assertTrue(Space.getInstance().VerifyChannel(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1")));
		Assert.assertTrue(Space.getInstance().VerifyChannel(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName2")));
		Assert.assertTrue(Space.getInstance().AddValuationEvents(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("CA1")));
		Assert.assertTrue(Space.getInstance().AddValuationEvents(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName2"),Input.get("CA1")));
		Assert.assertTrue(commonScripts.common.ReuploadDCR(Input.get("Metrology_DCR1")));
		Assert.assertTrue(commonScripts.common.WaitInSeconds(GlobalVariables.Input.get("Wait1")),"waiting for few seconds");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName2"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
		Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
		Assert.assertTrue(SiView.getInstance().getFutureLotStatus(GlobalVariables.LotID));
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	}
	@Test
	public void GF_SIL_TC006__SIL144_Inhibit_RouteAfter_OOC_violations()
		{	
			SoftAssert Assert= new SoftAssert();
			Title=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(Title,"Smoke Test,AutoCA");
			Map<String,String> Input=	commonScripts.common.Testdata(Title);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannel(Input.get("SpecLimitFile1"), Input.get("PDSetupFile1"), Input.get("Metrology_DCR1"), Input.get("Process_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyChannel(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationEvents(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("CA1")));
			Assert.assertTrue(Space.getInstance().editControlLimits(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("ControlLimits")));
			Assert.assertTrue(commonScripts.common.ReuploadDCR(Input.get("Metrology_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			Assert.assertTrue(SiView.getInstance().getRouteInhibitStatus(GlobalVariables.RouteId));
			Assert.assertTrue(SiView.getInstance().cancelRouteInhibit(GlobalVariables.RouteId));
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();
			
		}
	
	
	@Test
	public void GF_SIL_TC009__SIL1079211_Execute_AutoEquipmentAndRecipeInhibit_CA_forSingleLotWIPDCR()
		{	
			SoftAssert Assert= new SoftAssert();
 			Title=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(Title,"Smoke Test,AutoCA");
			Map<String,String> Input=	commonScripts.common.Testdata(Title);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannel(Input.get("SpecLimitFile1"), Input.get("PDSetupFile1"), Input.get("Metrology_DCR1"), Input.get("Process_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyChannel(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationEvents(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("CA1")));
			Assert.assertTrue(commonScripts.common.ReuploadDCR(Input.get("Metrology_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			Assert.assertTrue(SiView.getInstance().getEquipmentAndRecipeInhibitStatus(GlobalVariables.EquipmentID ,GlobalVariables.RecipeId));
			Assert.assertTrue(SiView.getInstance().cancelEquipmentAndRecipeInhibit(GlobalVariables.EquipmentID ,GlobalVariables.RecipeId));
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();
			
		}
	
	
	

}
