package smokeTestScripts;

import java.util.Map;


import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.gf.app.SiView;
import com.gf.app.Space;
import com.gf.util.GlobalVariables;
import com.gf.util.Helper;
import com.gf.util.InitaiteTestdata;

public class PropertyChange {
	@BeforeClass
	public void initaiteData()
	{
		InitaiteTestdata.getInstance();
		Helper.getInstance().clearSILLogs();
	}
	
	@AfterMethod
	public void ReleaseLotAndDeleteChannel()
	{
		SiView.getInstance().releaseFutureLotHold(GlobalVariables.LotID);
		Space.getInstance().DeleteChannel(GlobalVariables.LDSName, GlobalVariables.LDSFolderName, GlobalVariables.SPCChannelPathName, GlobalVariables.ChannelName);
		commonScripts.common.ReportEnd();
	}
	@BeforeMethod
	public void VerifyServerConnection()
	{
		Helper.getInstance().VerfiyServerConnection();
	}
	
	@Test
	public void GF_SIL_TC012__MSR_3_TC_2InlineLDS_ChartCheckFailure()
	{	
			SoftAssert Assert= new SoftAssert();
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Smoke Test,PropertyChange_AutoCA");
			Map<String,String> Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannel(Input.get("SpecLimitFile1"), Input.get("PDSetupFile1"), Input.get("Metrology_DCR1"), Input.get("Process_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyChannel(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1")));
			Assert.assertTrue(Helper.getInstance().SetConfigPropertyValue(Input.get("UpdateConfigFiles"),Input.get("UpdateConfigPropValues")));
			Assert.assertTrue(commonScripts.common.ReuploadDCR(Input.get("Metrology_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			Assert.assertTrue(SiView.getInstance().getFutureLotStatus(GlobalVariables.LotID));
			Assert.assertTrue(	Helper.getInstance().PropConfigFileReplace(Input.get("UpdateConfigFiles")));
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();
		
	}
	
}
