package smokeTestScripts;


import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.gf.app.SiView;
import com.gf.app.Space;
import com.gf.util.GlobalVariables;
import com.gf.util.Helper;
import com.gf.util.InitaiteTestdata;

public class R_ManualCA

{

	@BeforeClass
	public void initaiteData()
	{
		InitaiteTestdata.getInstance();
		Helper.getInstance().clearSILLogs();
	}
	@BeforeMethod
	public void VerifyServerConnection()
	{
		Helper.getInstance().VerfiyServerConnection();
	}
	
	@AfterMethod
	public void ReleaseLotAndDeleteChannel()
	{
		Space.getInstance().DeleteChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"));
		commonScripts.common.ReportEnd();
	}
	

	@Test
	public void GF_CA_TC004__MSR_2_TC_2_ManualCAusingExtendedDCR()
	{		SoftAssert Assert= new SoftAssert();
		    GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,ManualCA");
			 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannelExt(GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("Metrology_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")));
			Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")));
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")));
			Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1"),GlobalVariables.Input.get("ExpectionMgs")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")));
			Assert.assertTrue(SiView.getInstance().getEquipmentInhibitStatus(GlobalVariables.Input.get("EquipmentID1").trim()));
			Assert.assertTrue(SiView.getInstance().cancelEquipmentInhibit(GlobalVariables.Input.get("EquipmentID1").trim()));
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();	
	
	}
	
	
}
