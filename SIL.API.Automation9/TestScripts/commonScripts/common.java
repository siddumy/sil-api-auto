package commonScripts;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gf.app.SiView;
import com.gf.app.Space;
import com.gf.util.InitaiteTestdata;
import com.gf.util.MessageQueue;
import com.gf.util.SpecUpload;
import com.gf.util.TestReport;

public class common {
	static Logger logger = LogManager.getLogger(common.class);

	public static Map<String, String> Testdata(String TestName) {
		String TestFolderName = TestName;
		return InitaiteTestdata.Readtestdata(TestFolderName.split("__")[0]);

	}

	public static boolean ReportStart(String TestName, String TestType) {
		try {
			TestReport.RemoveInstance();
			if (TestName.split("__").length > 1)
				TestReport.GetInstance(TestName.split("__")[0], TestName.split("__")[1], TestType);
			else
				TestReport.GetInstance(TestName.split("__")[0], "", TestType);
			return true;
		} catch (Exception e) {
			logger.error("Unable to start Report Logging:-" + e);
			return false;
		}
	}

	public static boolean WaitInSeconds(String TimeInMilliSeconds) {
		try {
			if (TimeInMilliSeconds != null && !TimeInMilliSeconds.isEmpty()) {
				long wait = Integer.parseInt(TimeInMilliSeconds);
				Thread.sleep(wait);
			}

			return true;
		} catch (Exception e) {
			return true;
		}
	}

	public static void ReportEnd() {
		TestReport.Endtest();
	}

	public static boolean CreateChannel(String SpecLimit, String ResponsiblePD, String MetrologyDCR, String ProcessDCR)

	{

		boolean b1=false,b2=false,b3=false,b4=false;
		b1=SpecUpload.getInstance().Uploadspeclimit(SpecLimit);

		b2=SpecUpload.getInstance().UploadResponsiblePD(ResponsiblePD);
		b4=MessageQueue.getInstance().Load_ProcessDCR(ProcessDCR);
		b3=MessageQueue.getInstance().Load_MetrologyDCR(MetrologyDCR);
		
		if(b1&&b2&&b3&&b4)
		return true;
		else 
		return false;
	}

	

	
public static boolean CreateChannelExt(String MetrologyDCR, String ProcessDCR) {
		try {
			boolean b1 = false, b2 = false;
			b1 = MessageQueue.getInstance().Load_ProcessDCR(ProcessDCR);
			b2 = MessageQueue.getInstance().Load_MetrologyDCR(MetrologyDCR);
			if (b1 && b2)
				return true;
			else
				return false;
		} catch (Exception e) {
			logger.error("Error occured uploading the files due to:-" + e);
			return false;
		}

	}


	public static boolean ChamberFilterUpload(String ChamberFilterFile)

	{

		return SpecUpload.getInstance().UploadChamberLifter(ChamberFilterFile);

	}

	public static boolean GenericKeyFileUpload(String GenericKeyFile) {

		return SpecUpload.getInstance().uploadGenericKeys(GenericKeyFile);

	}

	public static boolean MonitoringSpecUpload(String MonitorSpecFile, String ResponsiblePD) {
		try {
			boolean b1 = false, b2 = false;
			b1 = SpecUpload.getInstance().uploadMonitorSpec(MonitorSpecFile);
			b2 = SpecUpload.getInstance().UploadResponsiblePD(ResponsiblePD);
			if (b1 && b2)
				return true;
			else
				return false;
		} catch (Exception e) {
			logger.error("Error occured uploading the files due to:-" + e);
			return false;
		}

	}

	public static boolean CreateBaseChannel(String SpecLimit, String ResponsiblePD, String MetrologyDCR,
			String ProcessDCR, String LDSName, String LDSFolderName, String SPCChannelPathName, String ChannelName,
			String LotID)

	{
		try {
			boolean b1 = false, b2 = false, b3 = false, b4 = false, b5 = false, b6 = false;
			b1 = SpecUpload.getInstance().Uploadspeclimit(SpecLimit);
			b2 = SpecUpload.getInstance().UploadResponsiblePD(ResponsiblePD);
			b4 = MessageQueue.getInstance().Load_ProcessDCR(ProcessDCR);
			b3 = MessageQueue.getInstance().Load_MetrologyDCR(MetrologyDCR);
			b5 = Space.getInstance().RemoveAttachedValuations(LDSName, LDSFolderName, SPCChannelPathName, ChannelName);
			b6 = SiView.getInstance().releaseFutureLotHold(LotID);
			if (b1 && b2 && b3 && b4 && b5 && b6)
				return true;
			else
				return false;
		} catch (Exception e) {
			logger.error("Error occured uploading the files due to:-" + e);
			return false;
		}
	}

	public static boolean CreateBaseChannel_MultiProcesDCR(String SpecLimit, String ResponsiblePD, String MetrologyDCR,
			String ProcessDCR1, String ProcessDCR2, String ProcessDCR3, String LDSName, String LDSFolderName,
			String SPCChannelPathName, String ChannelName, String LotID)

	{
		try {
			boolean b1 = true, b2 = true, b3 = true, b4 = true, b5 = true, b6 = true;
			b1 = SpecUpload.getInstance().Uploadspeclimit(SpecLimit);
			b2 = SpecUpload.getInstance().UploadResponsiblePD(ResponsiblePD);
			b4 = MessageQueue.getInstance().Load_ProcessDCR(ProcessDCR1);
			b4 = MessageQueue.getInstance().Load_ProcessDCR(ProcessDCR2);
			b4 = MessageQueue.getInstance().Load_ProcessDCR(ProcessDCR3);
			b3 = MessageQueue.getInstance().Load_MetrologyDCR(MetrologyDCR);
			b5 = Space.getInstance().RemoveAttachedValuations(LDSName, LDSFolderName, SPCChannelPathName, ChannelName);
			//b6 = SiView.getInstance().releaseFutureLotHold(LotID);
			if (b1 && b2 && b3 && b4 && b5 && b6)
				return true;
			else
				return false;
		} catch (Exception e) {
			logger.error("Error occured uploading the files due to:-" + e);
			return false;
		}
	}

	public static boolean ReuploadDCR(String Param1) {
		return MessageQueue.getInstance().Load_MetrologyDCR(Param1);
	}

	public static boolean CreateBaseChannel_Extnd(String MetrologyDCR, String ProcessDCR, String LDSName,
			String LDSFolderName, String SPCChannelPathName, String ChannelName, String LotID) {
		try {
			boolean b1 = MessageQueue.getInstance().Load_ProcessDCR(ProcessDCR);
			boolean b2 = MessageQueue.getInstance().Load_MetrologyDCR(MetrologyDCR);
			boolean b3 = Space.getInstance().RemoveAttachedValuations(LDSName, LDSFolderName, SPCChannelPathName,
					ChannelName);
			boolean b4 = SiView.getInstance().releaseFutureLotHold(LotID);
			if (b1 && b2 && b3 && b4)
				return true;
			else
				return false;
		} catch (Exception e) {
			logger.error("Error occured uploading the files due to:-" + e);
			return false;
		}
	}
}
