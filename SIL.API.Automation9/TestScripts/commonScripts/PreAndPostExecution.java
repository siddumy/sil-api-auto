package commonScripts;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.gf.app.Space;
import com.gf.util.GlobalVariables;
import com.gf.util.Helper;

public class PreAndPostExecution {

	@BeforeSuite(enabled=true)
	public void Pre_Execution()
	{
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Pre and Post Execution");
		Helper.getInstance().clearSILLogs();
		Helper.getInstance().backupSILConfigPorperty();
		Helper.getInstance().VerfiyServerConnection();
		commonScripts.common.ReportEnd();
	}
	
	@AfterSuite(enabled=true)
	public void Post_Execution()
	{
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Pre and Post Execution");
		Helper.getInstance().RestoreSILConfigPorperty();
		Helper.getInstance().CloseSession();
		Space.getInstance().CloseSpaceConnection();
		commonScripts.common.ReportEnd();
	}
	@Test(enabled=false)
	public void DummyTest()
	{
		System.out.println("executed");
	}
}
