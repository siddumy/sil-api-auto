package regressionSuite;


import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.gf.app.SiView;
import com.gf.app.Space;
import com.gf.util.GlobalVariables;
import com.gf.util.Helper;
import com.gf.util.InitaiteTestdata;

public class No_CA {

	@BeforeClass
	public void initaiteData()
	{
		InitaiteTestdata.getInstance();
		Helper.getInstance().clearSILLogs();
	}
	@BeforeMethod
	public void VerifyServerConnection()
	{
		Helper.getInstance().VerfiyServerConnection();
	}
	
	@AfterMethod
	public void ReleaseLotAndDeleteChannel()
	{
		Space.getInstance().DeleteChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"));
		commonScripts.common.ReportEnd();
	}
	
	@Test
	public void GF_SIL_TC022__SIL127Err11_HoldLot_OOS_withNoCA_defined_UpperLimits()
	{	
		 GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,No_CA");
			 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID));
			Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
			Assert.assertTrue(Helper.getInstance().SILlogs_StartDateTime());
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")));
			Assert.assertTrue(Helper.getInstance().SearchNoSilLog(GlobalVariables.Input.get("SILLogs_Text")));
			Assert.assertTrue(SiView.getInstance().getFutureLotStatus(GlobalVariables.LotID,GlobalVariables.Input.get("Status_Flag")),"Future lot hold is verified in Siview");
			
	}
	
	@Test
	public void GF_SIL_TC023__SIL128Err11_HoldLot_OOS_withNoCA_defined_LowerLimits()
	{	
		 GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,No_CA");
			 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID));
			Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
			Assert.assertTrue(Helper.getInstance().SILlogs_StartDateTime());
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")));
			Assert.assertTrue(Helper.getInstance().SearchNoSilLog(GlobalVariables.Input.get("SILLogs_Text")));
			Assert.assertTrue(SiView.getInstance().getFutureLotStatus(GlobalVariables.LotID,GlobalVariables.Input.get("Status_Flag")),"Future lot hold is verified in Siview");
			
	}
	@Test
	public void GF_SIL_TC024__SIL129Err11_HoldLot_OOS_withNoCA_defined_OutLimits()
	{	
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,No_CA");
			 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID));
			Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
			Assert.assertTrue(Helper.getInstance().SILlogs_StartDateTime());
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")));
			Assert.assertTrue(Helper.getInstance().SearchNoSilLog(GlobalVariables.Input.get("SILLogs_Text")));
			Assert.assertTrue(SiView.getInstance().getFutureLotStatus(GlobalVariables.LotID,GlobalVariables.Input.get("Status_Flag")),"Future lot hold is verified in Siview");
			
	}
	@Test
	public void GF_SIL_TC025__SIL135_HoldLotandInhibitChamber_NoCorrectiveActions()
	{	
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,No_CA");
			 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID));
			Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().editControlLimits(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ControlLimits")));
			Assert.assertTrue(Helper.getInstance().SILlogs_StartDateTime());
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")));
			Assert.assertTrue(Helper.getInstance().SearchNoSilLog(GlobalVariables.Input.get("SILLogs_Text")));
			Assert.assertTrue(SiView.getInstance().getFutureLotStatus(GlobalVariables.LotID,GlobalVariables.Input.get("Status_Flag")),"Future lot hold is verified in Siview");
	}
	
	
	
}
