package regressionSuite;



import java.util.Map;


import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.gf.app.SiView;
import com.gf.app.Space;
import com.gf.util.GlobalVariables;
import com.gf.util.Helper;
import com.gf.util.InitaiteTestdata;


public class AutoCA {
	@BeforeClass
	public void initaiteData()
	{
		InitaiteTestdata.getInstance();
		Helper.getInstance().clearSILLogs();
	}
	
	
	@BeforeMethod
	public void VerifyServerConnection()
	{
		Helper.getInstance().VerfiyServerConnection();
	}
	
	@AfterMethod
	public void ReleaseLotAndDeleteChannel()
	{
		SiView.getInstance().releaseFutureLotHold(GlobalVariables.LotID);
		Space.getInstance().DeleteChannel(GlobalVariables.LDSName, GlobalVariables.LDSFolderName, GlobalVariables.SPCChannelPathName, GlobalVariables.ChannelName);
		commonScripts.common.ReportEnd();
	}
	

	@Test(priority=-1)
	public void GF_SIL_TC013__SIL_124_LotHold_OOS_Upper_Limits()
	{		SoftAssert Assert= new SoftAssert();	
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,AutoCA");
			Map<String,String> Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
	//		Assert.assertTrue(Space.getInstance().ResetCAAttributes(Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannel(Input.get("SpecLimitFile1"), Input.get("PDSetupFile1"), Input.get("Metrology_DCR1"), Input.get("Process_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyChannel(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationts(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("ValuationName1")));
			Assert.assertTrue(Space.getInstance().AddValuationEvents(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("CA1")));
			Assert.assertTrue(commonScripts.common.ReuploadDCR(Input.get("Metrology_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			Assert.assertTrue(SiView.getInstance().getFutureLotStatus(GlobalVariables.LotID));
	//		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();	
			
	}
	@Test
	public void GF_SIL_TC014__SIL_125_LotHold_OOS_Lower_Limits()
	{SoftAssert Assert= new SoftAssert();	
		 	GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,AutoCA");
			Map<String,String> Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannel(Input.get("SpecLimitFile1"), Input.get("PDSetupFile1"), Input.get("Metrology_DCR1"), Input.get("Process_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyChannel(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationts(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("ValuationName1")));
			Assert.assertTrue(Space.getInstance().AddValuationEvents(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("CA1")));
			Assert.assertTrue(commonScripts.common.ReuploadDCR(Input.get("Metrology_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			Assert.assertTrue(SiView.getInstance().getFutureLotStatus(GlobalVariables.LotID));
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();	
			
	}
	@Test
	public void GF_SIL_TC015__SIL_126_LotHold_OOS_out_Limits()
	{SoftAssert Assert= new SoftAssert();	
		 	GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,AutoCA");
			Map<String,String> Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannel(Input.get("SpecLimitFile1"), Input.get("PDSetupFile1"), Input.get("Metrology_DCR1"), Input.get("Process_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyChannel(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationts(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("ValuationName1")));
			Assert.assertTrue(Space.getInstance().AddValuationEvents(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("CA1")));
			Assert.assertTrue(commonScripts.common.ReuploadDCR(Input.get("Metrology_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			Assert.assertTrue(SiView.getInstance().getFutureLotStatus(GlobalVariables.LotID));
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();	
			
	}
	@Test
	public void GF_SIL_TC016__SIL_742_ImplementCorrectHandling_whenReleasing_OOC_OOS_OtherResponses()
	{SoftAssert Assert= new SoftAssert();	
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,AutoCA");
			Map<String,String> Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannel(Input.get("SpecLimitFile1"), Input.get("PDSetupFile1"), Input.get("Metrology_DCR1"), Input.get("Process_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyChannel(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationts(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("ValuationName1")));
			Assert.assertTrue(Space.getInstance().AddValuationEvents(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("CA1")));
			Assert.assertTrue(commonScripts.common.ReuploadDCR(Input.get("Metrology_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			Assert.assertTrue(SiView.getInstance().getFutureLotStatus(GlobalVariables.LotID));
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();	
			
	}
	
	@Test
	public void GF_SIL_TC017__SIL_118_InhibitEquipment_OOC_Upper_Limits()
	{
			SoftAssert Assert= new SoftAssert();	
		 	GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,AutoCA");
			Map<String,String> Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannel(Input.get("SpecLimitFile1"), Input.get("PDSetupFile1"), Input.get("Metrology_DCR1"), Input.get("Process_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyChannel(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationts(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("ValuationName1")));
			Assert.assertTrue(Space.getInstance().AddValuationEvents(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("CA1")));
			Assert.assertTrue(Space.getInstance().editControlLimits(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("ControlLimits")));
			Assert.assertTrue(commonScripts.common.ReuploadDCR(Input.get("Metrology_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			Assert.assertTrue(SiView.getInstance().getEquipmentInhibitStatus(GlobalVariables.EquipmentID));
			Assert.assertTrue(SiView.getInstance().cancelEquipmentInhibit(GlobalVariables.EquipmentID));
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();	
			
	}
	
	@Test(priority=-2)
	public void GF_SIL_TC018__SIL_119_InhibitEquipment_OOC_Lower_Limits()
	{	
			SoftAssert Assert= new SoftAssert();	
		 	GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,AutoCA");
			Map<String,String> Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannel(Input.get("SpecLimitFile1"), Input.get("PDSetupFile1"), Input.get("Metrology_DCR1"), Input.get("Process_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyChannel(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationts(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("ValuationName1")));
			Assert.assertTrue(Space.getInstance().AddValuationEvents(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("CA1")));
			Assert.assertTrue(Space.getInstance().editControlLimits(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("ControlLimits")));
			Assert.assertTrue(commonScripts.common.ReuploadDCR(Input.get("Metrology_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			Assert.assertTrue(SiView.getInstance().getEquipmentInhibitStatus(GlobalVariables.EquipmentID));
			Assert.assertTrue(SiView.getInstance().cancelEquipmentInhibit(GlobalVariables.EquipmentID));
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();	
			
	}
	
	@Test
	public void GF_SIL_TC019__SIL_120_InhibitEquipment_OOC_Outer_Limits()
	{
			SoftAssert Assert= new SoftAssert();	
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,AutoCA");
			Map<String,String> Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannel(Input.get("SpecLimitFile1"), Input.get("PDSetupFile1"), Input.get("Metrology_DCR1"), Input.get("Process_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyChannel(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationts(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("ValuationName1")));
			Assert.assertTrue(Space.getInstance().AddValuationEvents(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("CA1")));
			Assert.assertTrue(Space.getInstance().editControlLimits(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("ControlLimits")));
			Assert.assertTrue(commonScripts.common.ReuploadDCR(Input.get("Metrology_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			Assert.assertTrue(SiView.getInstance().getEquipmentInhibitStatus(GlobalVariables.EquipmentID));
			Assert.assertTrue(SiView.getInstance().cancelEquipmentInhibit(GlobalVariables.EquipmentID));
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();	
			
	}
	@Test
	public void GF_SIL_TC020andTC021__SIL_130And136_HoldLotandInhibitEquipmentCorrectiveActionsAndHoldLotChamberInhibitforOOSAndOOC()
	{
			SoftAssert Assert= new SoftAssert();	
		 	GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,AutoCA");
			Map<String,String> Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannel(Input.get("SpecLimitFile1"), Input.get("PDSetupFile1"), Input.get("Metrology_DCR1"), Input.get("Process_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyChannel(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationts(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("ValuationName1")));
			Assert.assertTrue(Space.getInstance().AddValuationEvents(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("CA1")));
			Assert.assertTrue(Space.getInstance().editControlLimits(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("ControlLimits")));
			Assert.assertTrue(commonScripts.common.ReuploadDCR(Input.get("Metrology_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			Assert.assertTrue(SiView.getInstance().getFutureLotStatus(GlobalVariables.LotID));
			Assert.assertTrue(SiView.getInstance().getEquipmentInhibitStatus(GlobalVariables.EquipmentID));
			Assert.assertTrue(SiView.getInstance().getChamberInhibitStatus(GlobalVariables.EquipmentID,GlobalVariables.ChamberId));
			Assert.assertTrue(SiView.getInstance().cancelChamberInhibit(GlobalVariables.EquipmentID,GlobalVariables.ChamberId));
			Assert.assertTrue(SiView.getInstance().cancelEquipmentInhibit(GlobalVariables.EquipmentID));
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();	
			
	}
	

	@Test
	public void GF_CA_TC001__MSR_1_TC_1_AutoEquipmentAndRecipeInhibitCAforSingleLotWIPDCR()
	{
			SoftAssert Assert= new SoftAssert();	
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,AutoCA");
			Map<String,String> Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannel(Input.get("SpecLimitFile1"), Input.get("PDSetupFile1"), Input.get("Metrology_DCR1"), Input.get("Process_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyChannel(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationts(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("ValuationName1")));
			Assert.assertTrue(Space.getInstance().AddValuationEvents(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("CA1")));
			Assert.assertTrue(Space.getInstance().editControlLimits(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("ControlLimits")));
			Assert.assertTrue(commonScripts.common.ReuploadDCR(Input.get("Metrology_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			Assert.assertTrue(SiView.getInstance().cancelEquipmentAndRecipeInhibit(GlobalVariables.EquipmentID,GlobalVariables.RecipeId));
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();	
			
	}
	
	@Test
	public void GF_CA_TC005__Exist_16_TC_1_AutoEquipmentInhibitforSingleLotWIPDCR()
	{SoftAssert Assert= new SoftAssert();	
		 GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,AutoCA");
			Map<String,String> Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannel(Input.get("SpecLimitFile1"), Input.get("PDSetupFile1"), Input.get("Metrology_DCR1"), Input.get("Process_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyChannel(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationts(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("ValuationName1")));
			Assert.assertTrue(Space.getInstance().AddValuationEvents(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("CA1")));
			Assert.assertTrue(Space.getInstance().editControlLimits(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("ControlLimits")));
			Assert.assertTrue(commonScripts.common.ReuploadDCR(Input.get("Metrology_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			Assert.assertTrue(SiView.getInstance().cancelEquipmentInhibit(GlobalVariables.EquipmentID));
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();	
			
	}
	
	@Test
	public void GF_CA_TC006__Exist_16_TC_4_AutoMachineRecipeInhibitforSingleLotWIPDCR()
	{	SoftAssert Assert= new SoftAssert();	
		 GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,AutoCA");
			Map<String,String> Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannel(Input.get("SpecLimitFile1"), Input.get("PDSetupFile1"), Input.get("Metrology_DCR1"), Input.get("Process_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyChannel(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1")));
			Assert.assertTrue(Space.getInstance().AddValuationts(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("ValuationName1")));
			Assert.assertTrue(Space.getInstance().AddValuationEvents(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("CA1")));
			Assert.assertTrue(Space.getInstance().editControlLimits(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("ControlLimits")));
			Assert.assertTrue(commonScripts.common.ReuploadDCR(Input.get("Metrology_DCR1")));
			Assert.assertTrue(Space.getInstance().VerifyAttributes(Input.get("LDSName1"),Input.get("LDSFolderName1"),Input.get("SPCChannelPathName1"),Input.get("ChannelName1"),Input.get("Startdate1"),Input.get("Enddate1"),Input.get("SampleIndex1"),Input.get("Attributes1")));
			//Assert.assertTrue(SiView.getInstance().cancelEntityInhibit("Recipe",GlobalVariables.RecipeId));
			Assert.assertTrue(SiView.getInstance().cancelRecipeInhibit(GlobalVariables.RecipeId));
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();
					
	}
	
	

	
}
