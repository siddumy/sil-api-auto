package regressionSuite;





import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.gf.app.SiView;
import com.gf.app.Space;
import com.gf.util.GlobalVariables;
import com.gf.util.Helper;
import com.gf.util.InitaiteTestdata;

public class CorrectiveAction {
	
	
	@BeforeClass
	public void initaiteData()
	{
			InitaiteTestdata.getInstance();
			Helper.getInstance().clearSILLogs();
			
	}
	@BeforeMethod
	public void VerifyServerConnection()
	{
		Helper.getInstance().VerfiyServerConnection();
	}
	
	@AfterMethod
	public void DeleteChannel()
	{
		//Assert.assertTrue(Space.getInstance().DeleteChannelByFolder(GlobalVariables.LDSName, GlobalVariables.LDSFolderName, GlobalVariables.SPCChannelPathName));
		Space.getInstance().DeleteChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"));
		commonScripts.common.ReportEnd();
	}
	
	
	@Test(enabled=true)
	public void GF_SIL_CA002__AutoEquipmentAndRecipeInhibit_CA_for_SingleLot_using_3processDCR()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
			GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateBaseChannel_MultiProcesDCR(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"), GlobalVariables.Input.get("Process_DCR2"), GlobalVariables.Input.get("Process_DCR3"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
			Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
			Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
			Assert.assertTrue(SiView.getInstance().getEquipmentAndRecipeInhibitStatus(GlobalVariables.EquipmentID, GlobalVariables.RecipeId),"Inhibit status is verified in Siview");
			Assert.assertTrue(SiView.getInstance().cancelEquipmentAndRecipeInhibit(GlobalVariables.EquipmentID, GlobalVariables.RecipeId),"Cancelled/release the inhibit");
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();	
}
	@Test(enabled=true)
	public void GF_SIL_CA003__AutoCA_Responsible_PD_information_provided_inthe_ExtendedDCR()
	{	

		SoftAssert Assert= new SoftAssert();
	    GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,ManualCA");
		 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		 Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_MultiProcesDCR(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"), GlobalVariables.Input.get("Process_DCR2"), GlobalVariables.Input.get("Process_DCR3"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")));
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")));
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")));
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")));
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")));
		Assert.assertTrue(SiView.getInstance().getEquipmentInhibitStatus(GlobalVariables.Input.get("EquipmentID1").trim()),"verified equipment is on hold");
		Assert.assertTrue(SiView.getInstance().cancelEquipmentInhibit(GlobalVariables.Input.get("EquipmentID1").trim()),"cancelled equipment");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();		
}

	@Test
	public void GF_SIL_CA007__AutoMachineRecipeInhibit_for_SingleLotWIP_DCR()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
			GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
			Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
	//		Assert.assertTrue(Helper.getInstance().SetConfigPropertyValue(GlobalVariables.Input.get("UpdateConfigFiles"),GlobalVariables.Input.get("UpdateConfigPropValues")),"SIL config prop are updated");
			Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
			Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
			Assert.assertTrue(SiView.getInstance().getRecipeInhibitStatus(GlobalVariables.RecipeId),"Verified the machine recipe inhibit status in Siview");
			Assert.assertTrue(SiView.getInstance().cancelRecipeInhibit(GlobalVariables.RecipeId),"Cancelled/released the machine recipe inhibit from siview ");
	//	 	Assert.assertTrue(Helper.getInstance().PropConfigFileReplace(GlobalVariables.Input.get("UpdateConfigFiles")),"Replace the modified Config SIL property file");
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();	
}
	@Test
	public void GF_SIL_CA008__EquipmentInhibit_for_SingleLotWIP_DCR()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
			 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
			Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
	//		Assert.assertTrue(Helper.getInstance().SetConfigPropertyValue(GlobalVariables.Input.get("UpdateConfigFiles"),GlobalVariables.Input.get("UpdateConfigPropValues")),"SIL config prop are updated");
			Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
			Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
			Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
			Assert.assertTrue(SiView.getInstance().getEquipmentInhibitStatus(GlobalVariables.EquipmentID),"Verified the Equipment inhibit status in Siview");
			Assert.assertTrue(SiView.getInstance().cancelEquipmentInhibit(GlobalVariables.EquipmentID),"Cancelled/released the EquipmentID inhibit from siview ");
	//	 	Assert.assertTrue(Helper.getInstance().PropConfigFileReplace(GlobalVariables.Input.get("UpdateConfigFiles")),"Replace the modified Config SIL property file");
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			
			Assert.assertAll();	
}
	@Test
	public void GF_SIL_CA009__ReticleInhibit_for_SingleLot_WIPDCR()
	{	

			SoftAssert Assert= new SoftAssert();
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
			 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.GenericKeyFileUpload(GlobalVariables.Input.get("GenericKeyFile1")),"GenericKey uploaded");
			Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
			Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
	//		Assert.assertTrue(Helper.getInstance().SetConfigPropertyValue(GlobalVariables.Input.get("UpdateConfigFiles"),GlobalVariables.Input.get("UpdateConfigPropValues")),"SIL config prop are updated");
			Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
			Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
			Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
			Assert.assertTrue(SiView.getInstance().getReticleInhibitStatus(GlobalVariables.ReticleId),"Verified the Reticle inhibit status in Siview");
			Assert.assertTrue(SiView.getInstance().cancelReticleInhibit(GlobalVariables.ReticleId),"Cancelled/released the Reticle inhibit from siview ");
	//	 	Assert.assertTrue(Helper.getInstance().PropConfigFileReplace(GlobalVariables.Input.get("UpdateConfigFiles")),"Replace the modified Config SIL property file");
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();	
}
	
	
	@Test
	public void GF_SIL_CA010__ReleaseReticleInhibit_for_SingleLot_WIPDCR()
	{	

			SoftAssert Assert= new SoftAssert();
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
			 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			 Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
			 Assert.assertTrue(commonScripts.common.GenericKeyFileUpload(GlobalVariables.Input.get("GenericKeyFile1")),"GenericKey uploaded");
			Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");	
			Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
	//		Assert.assertTrue(Helper.getInstance().SetConfigPropertyValue(GlobalVariables.Input.get("UpdateConfigFiles"),GlobalVariables.Input.get("UpdateConfigPropValues")),"SIL config prop are updated");
			Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
			Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
			Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
			Assert.assertTrue(SiView.getInstance().getReticleInhibitStatus(GlobalVariables.ReticleId,GlobalVariables.Input.get("Status_Flag")),"Verified the Reticle inhibit status in Siview");
	//	 	Assert.assertTrue(Helper.getInstance().PropConfigFileReplace(GlobalVariables.Input.get("UpdateConfigFiles")),"Replace the modified Config SIL property file");
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();	
}
	
	@Test
	public void GF_SIL_CA011__AutoHoldAtFuturePD_for_SingleLotWIP_DCR()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");	
		Assert.assertTrue(SiView.getInstance().getFutureLotStatus(GlobalVariables.LotID));
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	
}
	
	@Test
	public void GF_SIL_CA012__HoldAtFuturePD_for_SingleLotWIP_DCR()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");	
		Assert.assertTrue(SiView.getInstance().getFutureLotStatus(GlobalVariables.LotID));
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	
}
	
	@Test
	public void GF_SIL_CA013__Non_WIPDCRexecute_ReticleInhibit()
	{	

		SoftAssert Assert= new SoftAssert();
		
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
			GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.GenericKeyFileUpload(GlobalVariables.Input.get("GenericKeyFile1")),"GenericKey uploaded");
			Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1")),"Non WIP channel is created");
			Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
			Assert.assertTrue(Space.getInstance().EnableControlLimits(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ControlLimit_Flag")),"Enabled the control limits");
			Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
			Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
	//		Assert.assertTrue(Space.getInstance().editControlLimits(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ControlLimits")));
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
			Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
			Assert.assertTrue(SiView.getInstance().getReticleInhibitStatus(GlobalVariables.ReticleId),"Verified the Reticle inhibit status in Siview");
			Assert.assertTrue(SiView.getInstance().cancelReticleInhibit(GlobalVariables.ReticleId),"Cancelled/released the Reticle inhibit from siview ");
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			
			Assert.assertAll();	
}
	
	@Test
	public void GF_SIL_CA014__MSR_1_TC_2_AutoChamberAndRecipeInhibit_for_SingleLotWIPDCR()
	{	

	
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
//		Assert.assertTrue(Helper.getInstance().SetConfigPropertyValue(GlobalVariables.Input.get("UpdateConfigFiles"),GlobalVariables.Input.get("UpdateConfigPropValues")),"SIL config prop are updated");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");
		Assert.assertTrue(SiView.getInstance().getChamberAndRecipeInhibitStatus(GlobalVariables.EquipmentID ,GlobalVariables.RecipeId),"Inhibit status is verified in Siview");
		Assert.assertTrue(SiView.getInstance().cancelChamberAndRecipeInhibit(GlobalVariables.EquipmentID, GlobalVariables.RecipeId),"Cancelled/release the inhibit");
//	 	Assert.assertTrue(Helper.getInstance().PropConfigFileReplace(GlobalVariables.Input.get("UpdateConfigFiles")),"Replace the modified Config SIL property file");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
	
}
	
	@Test
	public void GF_SIL_CA015__AutoReticleInhibitCA_for_SingleLotWIPDCR()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
			GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.GenericKeyFileUpload(GlobalVariables.Input.get("GenericKeyFile1")),"GenericKey uploaded");
			Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
			Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
			Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
			Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
			Assert.assertTrue(SiView.getInstance().getReticleInhibitStatus(GlobalVariables.ReticleId),"Verified the Reticle inhibit status in Siview");
			Assert.assertTrue(SiView.getInstance().cancelReticleInhibit(GlobalVariables.ReticleId),"Cancelled/released the Reticle inhibit from siview ");
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			
	Assert.assertAll();	
}

	@Test
	public void GF_SIL_CA018__LotHoldCA_for_MultiLot()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateChannel( GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"),GlobalVariables.Input.get("Process_DCR1")),"Base chanel is created for Extended DCR");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex2"),GlobalVariables.Input.get("Attributes2")),"Attributes are verified in Space Navigator");
		Assert.assertTrue(SiView.getInstance().getFutureLotStatus(GlobalVariables.Input.get("LotID1")),"Future lot hold is verified in Siview");
		Assert.assertTrue(SiView.getInstance().getFutureLotStatus(GlobalVariables.Input.get("LotID2")),"Future lot hold is verified in Siview");
		Assert.assertTrue(SiView.getInstance().releaseFutureLotHold(GlobalVariables.Input.get("LotID1")),"Future lot hold is released from Siview");
		Assert.assertTrue(SiView.getInstance().releaseFutureLotHold(GlobalVariables.Input.get("LotID2")),"Future lot hold is released from Siview");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		
	Assert.assertAll();	
}
	
	
	@Test
	public void GF_SIL_CA019__MSR_14_AutoCA_usingExtended_DCR()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_Extnd( GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created for Extended DCR");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");
		Assert.assertTrue(SiView.getInstance().getFutureLotStatus(GlobalVariables.LotID),"Future lot hold is verified in Siview");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			
	Assert.assertAll();	
}
	
	@Test
	public void GF_SIL_CA020__MSR_14_LotHoldCA_using_ExtendedDCR()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_Extnd( GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created for Extended DCR");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");
		Assert.assertTrue(SiView.getInstance().getFutureLotStatus(GlobalVariables.LotID),"Future lot hold is verified in Siview");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			
	Assert.assertAll();	
}
	
	@Test(enabled=false)
	public void GF_SIL_CA021__MSR_14__ReleaseLotHoldmanually()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_Extnd( GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created for Extended DCR");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");
		Assert.assertTrue(SiView.getInstance().getFutureLotStatus(GlobalVariables.LotID,GlobalVariables.Input.get("Status_Flag")),"Future lot hold is verified in Siview");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			
	Assert.assertAll();	
}
	@Test
	public void GF_SIL_CA022__Exist_AutoChamberInhibit_forSingleLotWIPDCR()
	{	

		SoftAssert Assert= new SoftAssert();
		
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
			GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.ChamberFilterUpload(GlobalVariables.Input.get("ChamberFilterFile1")),"Chamber Filter is uploaded");
			Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
			Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
			Assert.assertTrue(Helper.getInstance().SetConfigPropertyValue(GlobalVariables.Input.get("UpdateConfigFiles"),GlobalVariables.Input.get("UpdateConfigPropValues")),"SIL config prop are updated");
			Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
			Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
			Assert.assertTrue(SiView.getInstance().getChamberInhibitStatus(GlobalVariables.EquipmentID,GlobalVariables.ChamberId),"Verified the Chamberinhibit status in Siview");
			Assert.assertTrue(SiView.getInstance().cancelChamberInhibit(GlobalVariables.EquipmentID,GlobalVariables.ChamberId),"Cancelled/released the Chamberinhibit from siview ");
		 	Assert.assertTrue(Helper.getInstance().PropConfigFileReplace(GlobalVariables.Input.get("UpdateConfigFiles")),"Replace the modified Config SIL property file");
		 	Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			 	Assert.assertAll();
		 	
	
}
	
	@Test
	public void GF_SIL_CA023__Exist_AutoRouteInhibit_forSingleLotWIPDCR()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Helper.getInstance().SetConfigPropertyValue(GlobalVariables.Input.get("UpdateConfigFiles"),GlobalVariables.Input.get("UpdateConfigPropValues")),"SIL config prop are updated");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(SiView.getInstance().getRouteInhibitStatus(GlobalVariables.RouteId));
		Assert.assertTrue(	SiView.getInstance().cancelRouteInhibit(GlobalVariables.RouteId));
		Assert.assertTrue(Helper.getInstance().PropConfigFileReplace(GlobalVariables.Input.get("UpdateConfigFiles")),"Replace the modified Config SIL property file");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();
	
}
	
	@Test
	public void GF_SIL_CA024__Exist_MachineRecipeInhibit_forSingleLotWIPDCR()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Helper.getInstance().SetConfigPropertyValue(GlobalVariables.Input.get("UpdateConfigFiles"),GlobalVariables.Input.get("UpdateConfigPropValues")),"SIL config prop are updated");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(SiView.getInstance().getRecipeInhibitStatus(GlobalVariables.RecipeId),"Verified the RecipeInhibit status in Siview");
		Assert.assertTrue(	SiView.getInstance().cancelRecipeInhibit(GlobalVariables.RecipeId),"Cancelled/released the RecipeInhibit from Siview");
		Assert.assertTrue(Helper.getInstance().PropConfigFileReplace(GlobalVariables.Input.get("UpdateConfigFiles")),"Replace the modified Config SIL property file");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();	
}
	

	@Test
	public void GF_SIL_CA025__ReleaseEquipmentInhibit_forSingleLotWIPDCR()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(SiView.getInstance().getEquipmentInhibitStatus(GlobalVariables.EquipmentID,GlobalVariables.Input.get("Status_Flag")));
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();	
}
	
	
	@Test(enabled=false)
	public void GF_SIL_CA026__AutoBatchLotHold_for_SingleLotWIPDCR()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(commonScripts.common.WaitInSeconds(GlobalVariables.Input.get("Wait1")),"waiting for few seconds");		
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")));
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();	
}
	
	@Test
	public void GF_SIL_CA027__CancelLotHold_for_SingleLotWIPDCR()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
		Assert.assertTrue(SiView.getInstance().getFutureLotStatus(GlobalVariables.LotID,GlobalVariables.Input.get("Status_Flag")),"Future lot hold is verified in Siview");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();	
}
	@Test
	public void GF_SIL_CA029__AutoEquipmentInhibit_for_NonWIP_DCR()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
			GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1")),"Non WIP channel is created");
			Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
			Assert.assertTrue(Space.getInstance().EnableControlLimits(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ControlLimit_Flag")),"Enabled the control limits");
			Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
			Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
			Assert.assertTrue(SiView.getInstance().getEquipmentInhibitStatus(GlobalVariables.EquipmentID),"Verified the Equipment inhibit status in Siview");
			Assert.assertTrue(SiView.getInstance().cancelEquipmentInhibit(GlobalVariables.EquipmentID),"Cancelled/released the Equipment inhibit from siview ");
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			
	Assert.assertAll();	
}
	@Test
	public void GF_SIL_CA030__EquipmentInhibit_for_NonWIP_DCR()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
			GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1")),"Non WIP channel is created");
			Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
			Assert.assertTrue(Space.getInstance().EnableControlLimits(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ControlLimit_Flag")),"Enabled the control limits");
			Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
			Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
			Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
			Assert.assertTrue(SiView.getInstance().getEquipmentInhibitStatus(GlobalVariables.EquipmentID),"Verified the Equipment inhibit status in Siview");
			Assert.assertTrue(SiView.getInstance().cancelEquipmentInhibit(GlobalVariables.EquipmentID),"Cancelled/released the Equipment inhibit from siview ");
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();	
}
	@Test
	public void GF_SIL_CA031__AutoReticalInhibit_for_NonWIP_DCR()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
			GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
			Assert.assertTrue(commonScripts.common.GenericKeyFileUpload(GlobalVariables.Input.get("GenericKeyFile1")),"GenericKey uploaded");
			Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1")),"Non WIP channel is created");
			Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
			Assert.assertTrue(Space.getInstance().EnableControlLimits(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ControlLimit_Flag")),"Enabled the control limits");
			Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
			Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
			Assert.assertTrue(SiView.getInstance().getReticleInhibitStatus(GlobalVariables.ReticleId),"Verified the Reticle inhibit status in Siview");
			Assert.assertTrue(SiView.getInstance().cancelReticleInhibit(GlobalVariables.ReticleId),"Cancelled/released the Reticle inhibit from siview ");
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();	
}
	
	@Test
	public void GF_SIL_CA032__MSR1251726_01_MRB_request_for_AutoRaiseMRB()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");	
		Assert.assertTrue(Helper.getInstance().SearchSilLog(GlobalVariables.Input.get("SIL_Logs")));
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();	
}
	@Test
	public void GF_SIL_CA033__MSR1251726_03AutoRaiseMRB_functionality_for_nonWIP_DCR()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().EnableControlLimits(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ControlLimit_Flag")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");	
		Assert.assertTrue(Helper.getInstance().SearchSilLog(GlobalVariables.Input.get("SIL_Logs")));
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();	
}
	@Test
	public void GF_SIL_CA036__MSR1311935_02_AutoSendPCLWfrProp_CA()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
//		Assert.assertTrue(Space.getInstance().CreateNewCorrectiveAction(GlobalVariables.Input.get("CA_Name"), GlobalVariables.Input.get("CA_Description"), GlobalVariables.Input.get("CA_Author"), GlobalVariables.Input.get("CA_Department"), GlobalVariables.Input.get("CA_CostSection"), GlobalVariables.Input.get("CA_Text"), GlobalVariables.Input.get("CAAttributes")));
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");	
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	
}
	@Test
	public void GF_SIL_CA037__MSR1311935_07_AutoSendPCLLotProp_CA()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
//		Assert.assertTrue(Space.getInstance().CreateNewCorrectiveAction(GlobalVariables.Input.get("CA_Name"), GlobalVariables.Input.get("CA_Description"), GlobalVariables.Input.get("CA_Author"), GlobalVariables.Input.get("CA_Department"), GlobalVariables.Input.get("CA_CostSection"), GlobalVariables.Input.get("CA_Text"), GlobalVariables.Input.get("CAAttributes")));
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");	
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
	
}
	

	public void GF_SIL_CA038__MSR1305071_2_SendPCLWfrProp_CA()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
	//	Assert.assertTrue(Space.getInstance().CreateNewCorrectiveAction(GlobalVariables.Input.get("CA_Name"), GlobalVariables.Input.get("CA_Description"), GlobalVariables.Input.get("CA_Author"), GlobalVariables.Input.get("CA_Department"), GlobalVariables.Input.get("CA_CostSection"), GlobalVariables.Input.get("CA_Text"), GlobalVariables.Input.get("CAAttributes")));
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");	
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
	
}
	@Test
	public void GF_SIL_CA039__MSR1305071_7_SendPCLLotProp_CA()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
	//	Assert.assertTrue(Space.getInstance().CreateNewCorrectiveAction(GlobalVariables.Input.get("CA_Name"), GlobalVariables.Input.get("CA_Description"), GlobalVariables.Input.get("CA_Author"), GlobalVariables.Input.get("CA_Department"), GlobalVariables.Input.get("CA_CostSection"), GlobalVariables.Input.get("CA_Text"), GlobalVariables.Input.get("CAAttributes")));
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");	
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
	
}
	
	@Test
	public void GF_SIL_CA040__MSR1108802_01_EquipmentAndRecipeInhibit_CA_for_singleLotWIP_DCRwithNo_attributes_set()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");
		Assert.assertTrue(SiView.getInstance().getEquipmentAndRecipeInhibitStatus(GlobalVariables.EquipmentID, GlobalVariables.RecipeId),"Inhibit status is verified in Siview");
		Assert.assertTrue(SiView.getInstance().cancelEquipmentAndRecipeInhibit(GlobalVariables.EquipmentID, GlobalVariables.RecipeId),"Cancelled/release the inhibit");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	
}
	

	@Test
	public void GF_SIL_CA041__MSR1108802_19_ChamberAndRecipeInhibitCA_withNo_attributes_set()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");
		Assert.assertTrue(SiView.getInstance().getChamberAndRecipeInhibitStatus(GlobalVariables.EquipmentID ,GlobalVariables.RecipeId),"Inhibit status is verified in Siview");
		Assert.assertTrue(SiView.getInstance().cancelChamberAndRecipeInhibit(GlobalVariables.EquipmentID, GlobalVariables.RecipeId),"Cancelled/release the inhibit");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
	
}
	
	@Test
	public void GF_SIL_CA042__691919_1_AutoLotHold_CAforExtended_DCRs()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		 Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1")),"Non WIP channel is created");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_Extnd( GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created for Extended DCR");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");
		Assert.assertTrue(SiView.getInstance().getFutureLotStatus(GlobalVariables.LotID),"Future lot hold is verified in Siview");
		Assert.assertTrue(SiView.getInstance().releaseFutureLotHold(GlobalVariables.LotID),"Cancelled/release the future Lot hold");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();	
}
	@Test
	public void GF_SIL_CA043__1132965_1_EquipmentInhibit_CA_usingExtDCR()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		 Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1")),"Non WIP channel is created");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_Extnd( GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created for Extended DCR");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");
		Assert.assertTrue(SiView.getInstance().getEquipmentInhibitStatus(GlobalVariables.EquipmentID),"Equipment inhibit is verified in Siview");
		Assert.assertTrue(SiView.getInstance().cancelEquipmentInhibit(GlobalVariables.EquipmentID),"Cancelled/released equipment inhibit");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();	
}
	@Test
	public void GF_SIL_CA044__1132965_2_ChamberInhibit_CA_usingExtDCR()
	{	

		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1")),"Non WIP channel is created");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_Extnd( GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created for Extended DCR");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");
		Assert.assertTrue(SiView.getInstance().getChamberInhibitStatus(GlobalVariables.EquipmentID,GlobalVariables.ChamberId),"Chamber inhibit is verified in Siview");
		Assert.assertTrue(SiView.getInstance().cancelChamberInhibit(GlobalVariables.EquipmentID,GlobalVariables.ChamberId),"Cancelled/release the chamber inhibit");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();	
}
	@Test
	public void GF_SIL_CA045__CAfailurewithnoreasoncodes()
	{	

		SoftAssert Assert= new SoftAssert();
		
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
			 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			 Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1")),"Non WIP channel is created");
			Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1")),"Non WIP channel is created");
			Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
			Assert.assertTrue(SiView.getInstance().releaseFutureLotHold(GlobalVariables.Input.get("LotID1")),"released the future lot hold ");
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
	Assert.assertAll();	
}
	@Test
	public void GF_SIL_CA048__Offlinechannel_with_noCA()
	{	

			SoftAssert Assert= new SoftAssert();
		
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
			 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Removing CA Attributes");Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1")),"Non WIP channel is created");
			Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
			Assert.assertTrue(Space.getInstance().UpdateChannelState(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ChannelState1")),"Updating the Channel State");
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
			Assert.assertTrue(SiView.getInstance().releaseFutureLotHold(GlobalVariables.Input.get("LotID1")),"released the future lot hold ");
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertAll();	
}
	@Test
	public void GF_SIL_CA070__MSR1211201_1_VerifyifAutoSendPCLData_BatchProdDICAworksusingattributes()
	{	

		
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"),GlobalVariables.Input.get("PDSetupFile1"),GlobalVariables.Input.get("Metrology_DCR1"),GlobalVariables.Input.get("Process_DCR1")));
		Assert.assertTrue(commonScripts.common.WaitInSeconds(GlobalVariables.Input.get("Wait1")),"waiting for few seconds");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().RemoveAttachedValuations(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")));
		//Assert.assertTrue(SiView.getInstance().releaseFutureLotHold(GlobalVariables.Input.get("LotID1")));
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(Space.getInstance().editControlLimits(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ControlLimits")));
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");	
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
	
}
	
	@Test
	public void GF_SIL_CA071__MSR1211201_2_VerifyifSendPCLData_BatchProdDICAworksusingattributes()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"),GlobalVariables.Input.get("PDSetupFile1"),GlobalVariables.Input.get("Metrology_DCR1"),GlobalVariables.Input.get("Process_DCR1")));
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().RemoveAttachedValuations(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")));
		//Assert.assertTrue(SiView.getInstance().releaseFutureLotHold(GlobalVariables.Input.get("LotID1")));
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(Space.getInstance().editControlLimits(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ControlLimits")));
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")));
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("ViolationComment1"),GlobalVariables.Input.get("ExpectionMgs")));
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");	
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
	
}
	
	@Test
	public void GF_SIL_CA072__MSR1211201_3_VerifyifAutoSendPCLData_BatchProdDICAdoesnotworkwhennoPDfound()
	{	

		
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"),GlobalVariables.Input.get("PDSetupFile1"),GlobalVariables.Input.get("Metrology_DCR1"),GlobalVariables.Input.get("Process_DCR1")));
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().RemoveAttachedValuations(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")));
		//Assert.assertTrue(SiView.getInstance().releaseFutureLotHold(GlobalVariables.Input.get("LotID1")));
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(Space.getInstance().editControlLimits(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ControlLimits")));
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");	
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
	
}
	
	@Test
	public void GF_SIL_CA073__MSR1211201_4_VerifyifSendPCLData_BatchProdDICAdoesnotworkwhennoPDfound()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"),GlobalVariables.Input.get("PDSetupFile1"),GlobalVariables.Input.get("Metrology_DCR1"),GlobalVariables.Input.get("Process_DCR1")));
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().RemoveAttachedValuations(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")));
		//Assert.assertTrue(SiView.getInstance().releaseFutureLotHold(GlobalVariables.Input.get("LotID1")));
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(Space.getInstance().editControlLimits(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ControlLimits")));
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")));
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("ViolationComment1"),GlobalVariables.Input.get("ExpectionMgs")));
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");	
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
	

}
	@Test
	public void GF_SIL_CA054__MSR1547987_1_SetLotScriptParameterCAusingattributes()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"),GlobalVariables.Input.get("PDSetupFile1"),GlobalVariables.Input.get("Metrology_DCR1"),GlobalVariables.Input.get("Process_DCR1")));
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().RemoveAttachedValuations(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")));
		//Assert.assertTrue(SiView.getInstance().releaseFutureLotHold(GlobalVariables.Input.get("LotID1")));
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")));
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("ViolationComment1"),GlobalVariables.Input.get("ExpectionMgs")));
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");	
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertTrue(SiView.getInstance().VerifyScriptParameterValue(GlobalVariables.Input.get("ScriptParam_ClassName"),GlobalVariables.Input.get("ScriptParam_Identifier"),GlobalVariables.Input.get("ScriptParam_Name"),GlobalVariables.Input.get("ScriptParam_Value")));
		Assert.assertAll();
		
	

}
	
	@Test
	public void GF_SIL_CA055__MSR1547987_2_SetLotScriptParameterCAusingattributeswithnoCAcomment()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"),GlobalVariables.Input.get("PDSetupFile1"),GlobalVariables.Input.get("Metrology_DCR1"),GlobalVariables.Input.get("Process_DCR1")));
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().RemoveAttachedValuations(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")));
		//Assert.assertTrue(SiView.getInstance().releaseFutureLotHold(GlobalVariables.Input.get("LotID1")));
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")));
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("ViolationComment1"),GlobalVariables.Input.get("ExpectionMgs")));
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");	
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertTrue(SiView.getInstance().VerifyScriptParameterValue(GlobalVariables.Input.get("ScriptParam_ClassName"),GlobalVariables.Input.get("ScriptParam_Identifier"),GlobalVariables.Input.get("ScriptParam_Name"),GlobalVariables.Input.get("ScriptParam_Value")));
		Assert.assertAll();
		
	

}
	
	@Test
	public void GF_SIL_CA056__MSR1547987_3_SetLotScriptParameterCAwithoutPullScriptParameterValueattribute()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"),GlobalVariables.Input.get("PDSetupFile1"),GlobalVariables.Input.get("Metrology_DCR1"),GlobalVariables.Input.get("Process_DCR1")));
		Assert.assertTrue(commonScripts.common.WaitInSeconds(GlobalVariables.Input.get("Wait1")),"waiting for few seconds");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().RemoveAttachedValuations(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")));
		//Assert.assertTrue(SiView.getInstance().releaseFutureLotHold(GlobalVariables.Input.get("LotID1")));
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")));
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("ViolationComment1"),GlobalVariables.Input.get("ExpectionMgs")));
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");	
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertTrue(SiView.getInstance().VerifyScriptParameterValue(GlobalVariables.Input.get("ScriptParam_ClassName"),GlobalVariables.Input.get("ScriptParam_Identifier"),GlobalVariables.Input.get("ScriptParam_Name"),GlobalVariables.Input.get("ScriptParam_Value")));
		Assert.assertAll();
		
	

}
	
	@Test
	public void GF_SIL_CA059__MSR1547987_6_SetWaferScriptParameterCAusingattributesforallwafers()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"),GlobalVariables.Input.get("PDSetupFile1"),GlobalVariables.Input.get("Metrology_DCR1"),GlobalVariables.Input.get("Process_DCR1")));
		Assert.assertTrue(commonScripts.common.WaitInSeconds(GlobalVariables.Input.get("Wait1")),"waiting for few seconds");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().RemoveAttachedValuations(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")));
		//Assert.assertTrue(SiView.getInstance().releaseFutureLotHold(GlobalVariables.Input.get("LotID1")));
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")));
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("ViolationComment1"),GlobalVariables.Input.get("ExpectionMgs")));
		Assert.assertTrue(commonScripts.common.WaitInSeconds(GlobalVariables.Input.get("Wait1")),"waiting for few seconds");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");	
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertTrue(SiView.getInstance().VerifyScriptParameterValue(GlobalVariables.Input.get("ScriptParam_ClassName"),GlobalVariables.Input.get("ScriptParam_Identifier"),GlobalVariables.Input.get("ScriptParam_Name"),GlobalVariables.Input.get("ScriptParam_Value")));
		Assert.assertTrue(SiView.getInstance().VerifyScriptParameterValue(GlobalVariables.Input.get("ScriptParam_ClassName2"),GlobalVariables.Input.get("ScriptParam_Identifier2"),GlobalVariables.Input.get("ScriptParam_Name"),GlobalVariables.Input.get("ScriptParam_Value")));
		Assert.assertAll();
		
	}
	
	@Test
	public void GF_SIL_CA060__MSR1547987_7_SetWaferScriptParameterCAusingattributes()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
		 GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"),GlobalVariables.Input.get("PDSetupFile1"),GlobalVariables.Input.get("Metrology_DCR1"),GlobalVariables.Input.get("Process_DCR1")));
		Assert.assertTrue(commonScripts.common.WaitInSeconds(GlobalVariables.Input.get("Wait1")),"waiting for few seconds");
		Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
		Assert.assertTrue(Space.getInstance().RemoveAttachedValuations(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")));
		//Assert.assertTrue(SiView.getInstance().releaseFutureLotHold(GlobalVariables.Input.get("LotID1")));
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")));
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("ViolationComment1"),GlobalVariables.Input.get("ExpectionMgs")));
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes are verified in Space Navigator");	
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertTrue(SiView.getInstance().VerifyScriptParameterValue(GlobalVariables.Input.get("ScriptParam_ClassName"),GlobalVariables.Input.get("ScriptParam_Identifier"),GlobalVariables.Input.get("ScriptParam_Name"),GlobalVariables.Input.get("ScriptParam_Value")));
		Assert.assertAll();
		
	

}
	
	

}
	
	
	


