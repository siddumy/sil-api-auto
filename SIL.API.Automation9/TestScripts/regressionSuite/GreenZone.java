package regressionSuite;



import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.gf.app.Space;
import com.gf.util.GlobalVariables;
import com.gf.util.Helper;
import com.gf.util.InitaiteTestdata;

public class GreenZone {
	
	@BeforeClass(enabled=false)
	public void initaiteData()
	{
			InitaiteTestdata.getInstance();
			Helper.getInstance().clearSILLogs();
	}
	@BeforeSuite(enabled=false)
	public void Pre_Execution()
	{
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Pre and Post Execution");
		Helper.getInstance().clearSILLogs();
		Helper.getInstance().backupSILConfigPorperty();
		Helper.getInstance().VerfiyServerConnection();
		commonScripts.common.ReportEnd();
	}
	
	@BeforeMethod(enabled=false)
	public void VerifyServerConnection()
	{
		Helper.getInstance().VerfiyServerConnection();
	}
	
	@AfterMethod
	public void DeleteChannel()
	{
		//Assert.assertTrue(Space.getInstance().DeleteChannelByFolder(GlobalVariables.LDSName, GlobalVariables.LDSFolderName, GlobalVariables.SPCChannelPathName));
		Space.getInstance().DeleteChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"));
		commonScripts.common.ReportEnd();
	}
	@Test(enabled=false)
	public void SampleGreenZoneandGreenzone_type_Value()
	{
			SoftAssert Assert= new SoftAssert();
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Corrective_Action");
			GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Helper.getInstance().SetConfigPropertyValue(GlobalVariables.Input.get("UpdateConfigFiles"),GlobalVariables.Input.get("UpdateConfigPropValues")),"SIL config prop are updated");
			Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Resetting CA Attributes");
			Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
			Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
			Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
			Assert.assertTrue(Space.getInstance().CreateTemplate(GlobalVariables.Input.get("LDSName1"), GlobalVariables.Input.get("TemplateFolder1"), GlobalVariables.Input.get("TemplateName1"), GlobalVariables.Input.get("ParameterName1"), GlobalVariables.Input.get("ParameterName1")),"Template is created");
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
			Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
			Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
			Assert.assertTrue(Helper.getInstance().PropConfigFileReplace(GlobalVariables.Input.get("UpdateConfigFiles")),"Replace the modified Config SIL property file");
			Assert.assertAll();	
	}
	
	
}

