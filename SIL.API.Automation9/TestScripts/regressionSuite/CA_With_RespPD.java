package regressionSuite;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.gf.app.SiView;
import com.gf.app.Space;
import com.gf.util.GlobalVariables;
import com.gf.util.Helper;
import com.gf.util.InitaiteTestdata;

public class CA_With_RespPD {
	/* 
	 * Siddharth- Note for Release LotHold
	 
	 * Default_LIT_Template is using in test data as a default template and this template doesn't hold Lot while uploading DCR
	 * Hence CreateBaseChannel_MultiProcesDCR is not coming with release lothold
	 
	 */
	
	@BeforeClass
	public void initaiteData()
	{
			InitaiteTestdata.getInstance();
		//	Helper.getInstance().clearSILLogs();
	}
	@BeforeMethod
	public void VerifyServerConnection()
	{
		Helper.getInstance().VerfiyServerConnection();
	}
	
	@AfterMethod
	public void DeleteChannel()
	{
		//Assert.assertTrue(Space.getInstance().DeleteChannelByFolder(GlobalVariables.LDSName, GlobalVariables.LDSFolderName, GlobalVariables.SPCChannelPathName));
		Space.getInstance().DeleteChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"));
		commonScripts.common.ReportEnd();
	}
	@Test
	public void GF_SIL_CARP001__verify_new_CAs_AutoEquipmentAndPDAndProdGrpInhibi_functionality_with_CAattribute_ResponsiblePD1()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,CA's With RespPD's");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_MultiProcesDCR(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"), GlobalVariables.Input.get("Process_DCR2"), GlobalVariables.Input.get("Process_DCR3"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(SiView.getInstance().getProductGrpPDandEquipmentStatus(GlobalVariables.Input.get("ProductGroup1"), GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1"), GlobalVariables.Input.get("Status_Flag")),"Verified the Product group PD and equipment inhibit");
		Assert.assertTrue(SiView.getInstance().cancelProductGrpAndPDAndEquipment(GlobalVariables.Input.get("ProductGroup1"), GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1")),"Cancelled Product group PD and equipment inhibit");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	}
	@Test
	public void GF_SIL_CARP002__verify_new_CAs_AutoEquipmentAndPDAndProdGrpInhibi_functionality_without_CAattribute_ResponsiblePD1()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,CA's With RespPD's");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Resetting CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_MultiProcesDCR(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"), GlobalVariables.Input.get("Process_DCR2"), GlobalVariables.Input.get("Process_DCR3"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(SiView.getInstance().getProductGrpPDandEquipmentStatus(GlobalVariables.Input.get("ProductGroup1"), GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1"), GlobalVariables.Input.get("Status_Flag")),"Verified the Product group PD and equipment inhibit");
		Assert.assertTrue(SiView.getInstance().cancelProductGrpAndPDAndEquipment(GlobalVariables.Input.get("ProductGroup1"), GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1")),"Cancelled Product group PD and equipment inhibit");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	}
	@Test
	public void GF_SIL_CARP003__verify_new_CAs_AutoEquipmentAndPDAndProdGrpInhibi_functionality_with_CAattribute_ResponsiblePD1_2()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,CA's With RespPD's");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_MultiProcesDCR(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"), GlobalVariables.Input.get("Process_DCR2"), GlobalVariables.Input.get("Process_DCR3"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(SiView.getInstance().getProductGrpPDandEquipmentStatus(GlobalVariables.Input.get("ProductGroup1"), GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1"), GlobalVariables.Input.get("Status_Flag")),"Verified the Product group PD and equipment inhibit");
		Assert.assertTrue(SiView.getInstance().getProductGrpPDandEquipmentStatus(GlobalVariables.Input.get("ProductGroup2"), GlobalVariables.Input.get("ProductID2"), GlobalVariables.Input.get("EquipmentID2"), GlobalVariables.Input.get("Status_Flag")),"Verified the Product group PD and equipment inhibit");
		Assert.assertTrue(SiView.getInstance().cancelProductGrpAndPDAndEquipment(GlobalVariables.Input.get("ProductGroup1"), GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1")),"Cancelled Product group PD and equipment inhibit");
		Assert.assertTrue(SiView.getInstance().cancelProductGrpAndPDAndEquipment(GlobalVariables.Input.get("ProductGroup2"), GlobalVariables.Input.get("ProductID2"), GlobalVariables.Input.get("EquipmentID2")),"Cancelled Product group PD and equipment inhibit");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	}
	@Test
	public void GF_SIL_CARP004__verify_new_CAs_AutoEquipmentAndPDAndProdGrpInhibi_functionality_with_CAattribute_ResponsiblePD0()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,CA's With RespPD's");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_MultiProcesDCR(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"), GlobalVariables.Input.get("Process_DCR2"), GlobalVariables.Input.get("Process_DCR3"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(SiView.getInstance().getEquipmentInhibitStatus(GlobalVariables.Input.get("EquipmentID1")),"Verified the Equipment inhibit status in Siview");
		Assert.assertTrue(SiView.getInstance().cancelEquipmentInhibit(GlobalVariables.Input.get("EquipmentID1")),"Cancelled/released the Equipment inhibit from siview ");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	}
	@Test
	public void GF_SIL_CARP005__verify_new_CAs_AutoEquipmentAndPDAndProdGrpInhibi_functionality_withInvalid_equipment_passed_in_DCR()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,CA's With RespPD's");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_MultiProcesDCR(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"), GlobalVariables.Input.get("Process_DCR2"), GlobalVariables.Input.get("Process_DCR3"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	}
	
	@Test
	public void GF_SIL_CARP006__verify_new_CAs_EquipmentAndPDAndProdGrpInhibi_functionality_with_CAattribute_ResponsiblePD1()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,CA's With RespPD's");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_MultiProcesDCR(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"), GlobalVariables.Input.get("Process_DCR2"), GlobalVariables.Input.get("Process_DCR3"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(SiView.getInstance().getProductGrpPDandEquipmentStatus(GlobalVariables.Input.get("ProductGroup1"), GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1"), GlobalVariables.Input.get("Status_Flag")),"Verified the Product group PD and equipment inhibit");
		Assert.assertTrue(SiView.getInstance().cancelProductGrpAndPDAndEquipment(GlobalVariables.Input.get("ProductGroup1"), GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1")),"Cancelled Product group PD and equipment inhibit");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	}
	@Test
	public void GF_SIL_CARP007__verify_new_CAs_EquipmentAndPDAndProdGrpInhibi_functionality_without_CAattribute_ResponsiblePD1()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,CA's With RespPD's");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Resetting CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_MultiProcesDCR(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"), GlobalVariables.Input.get("Process_DCR2"), GlobalVariables.Input.get("Process_DCR3"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(SiView.getInstance().getProductGrpPDandEquipmentStatus(GlobalVariables.Input.get("ProductGroup1"), GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1"), GlobalVariables.Input.get("Status_Flag")),"Verified the Product group PD and equipment inhibit");
		Assert.assertTrue(SiView.getInstance().cancelProductGrpAndPDAndEquipment(GlobalVariables.Input.get("ProductGroup1"), GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1")),"Cancelled Product group PD and equipment inhibit");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	}
	@Test
	public void GF_SIL_CARP008__verify_new_CAs_EquipmentAndPDAndProdGrpInhibi_functionality_with_CAattribute_ResponsiblePD1_2()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,CA's With RespPD's");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_MultiProcesDCR(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"), GlobalVariables.Input.get("Process_DCR2"), GlobalVariables.Input.get("Process_DCR3"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(SiView.getInstance().getProductGrpPDandEquipmentStatus(GlobalVariables.Input.get("ProductGroup1"), GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1"), GlobalVariables.Input.get("Status_Flag")),"Verified the Product group PD and equipment inhibit");
		Assert.assertTrue(SiView.getInstance().getProductGrpPDandEquipmentStatus(GlobalVariables.Input.get("ProductGroup2"), GlobalVariables.Input.get("ProductID2"), GlobalVariables.Input.get("EquipmentID2"), GlobalVariables.Input.get("Status_Flag")),"Verified the Product group PD and equipment inhibit");
		Assert.assertTrue(SiView.getInstance().cancelProductGrpAndPDAndEquipment(GlobalVariables.Input.get("ProductGroup1"), GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1")),"Cancelled Product group PD and equipment inhibit");
		Assert.assertTrue(SiView.getInstance().cancelProductGrpAndPDAndEquipment(GlobalVariables.Input.get("ProductGroup2"), GlobalVariables.Input.get("ProductID2"), GlobalVariables.Input.get("EquipmentID2")),"Cancelled Product group PD and equipment inhibit");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	}
	@Test
	public void GF_SIL_CARP009__verify_new_CAs_EquipmentAndPDAndProdGrpInhibi_functionality_with_CAattribute_ResponsiblePD0()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,CA's With RespPD's");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_MultiProcesDCR(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"), GlobalVariables.Input.get("Process_DCR2"), GlobalVariables.Input.get("Process_DCR3"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(SiView.getInstance().getEquipmentInhibitStatus(GlobalVariables.Input.get("EquipmentID1")),"Verified the Equipment inhibit status in Siview");
		Assert.assertTrue(SiView.getInstance().cancelEquipmentInhibit(GlobalVariables.Input.get("EquipmentID1")),"Cancelled/released the Equipment inhibit from siview ");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	}
	@Test
	public void GF_SIL_CARP010__verify_new_CAs_EquipmentAndPDAndProdGrpInhibi_functionality_withInvalid_equipment_passed_in_DCR()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,CA's With RespPD's");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_MultiProcesDCR(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"), GlobalVariables.Input.get("Process_DCR2"), GlobalVariables.Input.get("Process_DCR3"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	}
	@Test
	public void GF_SIL_CARP011__verify_new_CAs_AutoEquipmentAndPDInhibit_functionality_with_CAattribute_ResponsiblePD1()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,CA's With RespPD's");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_MultiProcesDCR(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"), GlobalVariables.Input.get("Process_DCR2"), GlobalVariables.Input.get("Process_DCR3"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(SiView.getInstance().getPDandEquipmentStatus( GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1"), GlobalVariables.Input.get("Status_Flag")),"Verified the PD and equipment inhibit");
		Assert.assertTrue(SiView.getInstance().cancelPDAndEquipment( GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1")),"Cancelled PD and equipment inhibit");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	}
	@Test
	public void GF_SIL_CARP012__verify_new_CAs_AutoEquipmentAndPDInhibit_functionality_without_CAattribute_ResponsiblePD1()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,CA's With RespPD's");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Resetting CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_MultiProcesDCR(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"), GlobalVariables.Input.get("Process_DCR2"), GlobalVariables.Input.get("Process_DCR3"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(SiView.getInstance().getPDandEquipmentStatus( GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1"), GlobalVariables.Input.get("Status_Flag")),"Verified the PD and equipment inhibit");
		Assert.assertTrue(SiView.getInstance().cancelPDAndEquipment( GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1")),"Cancelled PD and equipment inhibit");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	}
	@Test
	public void GF_SIL_CARP013__verify_new_CAs_AutoEquipmentAndPDInhibit_functionality_with_CAattribute_ResponsiblePD1_2()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,CA's With RespPD's");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_MultiProcesDCR(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"), GlobalVariables.Input.get("Process_DCR2"), GlobalVariables.Input.get("Process_DCR3"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(SiView.getInstance().getPDandEquipmentStatus( GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1"), GlobalVariables.Input.get("Status_Flag")),"Verified the PD and equipment inhibit");
		Assert.assertTrue(SiView.getInstance().cancelPDAndEquipment( GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1")),"Cancelled PD and equipment inhibit");
		Assert.assertTrue(SiView.getInstance().getPDandEquipmentStatus( GlobalVariables.Input.get("ProductID2"), GlobalVariables.Input.get("EquipmentID2"), GlobalVariables.Input.get("Status_Flag")),"Verified the PD and equipment inhibit");
		Assert.assertTrue(SiView.getInstance().cancelPDAndEquipment( GlobalVariables.Input.get("ProductID2"), GlobalVariables.Input.get("EquipmentID2")),"Cancelled PD and equipment inhibit");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	}
	@Test
	public void GF_SIL_CARP014__verify_new_CAs_AutoEquipmentAndPDInhibit_functionality_with_CAattribute_ResponsiblePD0()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,CA's With RespPD's");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_MultiProcesDCR(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"), GlobalVariables.Input.get("Process_DCR2"), GlobalVariables.Input.get("Process_DCR3"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(SiView.getInstance().getEquipmentInhibitStatus(GlobalVariables.Input.get("EquipmentID1")),"Verified the Equipment inhibit status in Siview");
		Assert.assertTrue(SiView.getInstance().cancelEquipmentInhibit(GlobalVariables.Input.get("EquipmentID1")),"Cancelled/released the Equipment inhibit from siview ");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	}
	@Test
	public void GF_SIL_CARP015__verify_new_CAs_AutoEquipmentAndPDInhibit_functionality_withInvalid_equipment_passed_in_DCR()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,CA's With RespPD's");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_MultiProcesDCR(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"), GlobalVariables.Input.get("Process_DCR2"), GlobalVariables.Input.get("Process_DCR3"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	}
	
	@Test
	public void GF_SIL_CARP016__verify_new_CAs_EquipmentAndPDInhibit_functionality_with_CAattribute_ResponsiblePD1()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,CA's With RespPD's");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_MultiProcesDCR(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"), GlobalVariables.Input.get("Process_DCR2"), GlobalVariables.Input.get("Process_DCR3"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(SiView.getInstance().getPDandEquipmentStatus( GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1"), GlobalVariables.Input.get("Status_Flag")),"Verified the PD and equipment inhibit");
		Assert.assertTrue(SiView.getInstance().cancelPDAndEquipment( GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1")),"Cancelled PD and equipment inhibit");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	}
	@Test
	public void GF_SIL_CARP017__verify_new_CAs_EquipmentAndPDInhibit_functionality_without_CAattribute_ResponsiblePD1()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,CA's With RespPD's");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().ResetCAAttributes(GlobalVariables.Input.get("CA1")),"Resetting CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_MultiProcesDCR(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"), GlobalVariables.Input.get("Process_DCR2"), GlobalVariables.Input.get("Process_DCR3"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(SiView.getInstance().getPDandEquipmentStatus( GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1"), GlobalVariables.Input.get("Status_Flag")),"Verified the PD and equipment inhibit");
		Assert.assertTrue(SiView.getInstance().cancelPDAndEquipment( GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1")),"Cancelled PD and equipment inhibit");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	}
	@Test
	public void GF_SIL_CARP018__verify_new_CAs_EquipmentAndPDInhibit_functionality_with_CAattribute_ResponsiblePD1_2()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,CA's With RespPD's");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_MultiProcesDCR(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"), GlobalVariables.Input.get("Process_DCR2"), GlobalVariables.Input.get("Process_DCR3"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(SiView.getInstance().getPDandEquipmentStatus( GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1"), GlobalVariables.Input.get("Status_Flag")),"Verified the PD and equipment inhibit");
		Assert.assertTrue(SiView.getInstance().cancelPDAndEquipment( GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1")),"Cancelled PD and equipment inhibit");
		Assert.assertTrue(SiView.getInstance().getPDandEquipmentStatus( GlobalVariables.Input.get("ProductID2"), GlobalVariables.Input.get("EquipmentID2"), GlobalVariables.Input.get("Status_Flag")),"Verified the PD and equipment inhibit");
		Assert.assertTrue(SiView.getInstance().cancelPDAndEquipment( GlobalVariables.Input.get("ProductID2"), GlobalVariables.Input.get("EquipmentID2")),"Cancelled PD and equipment inhibit");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	}
	@Test
	public void GF_SIL_CARP019__verify_new_CAs_EquipmentAndPDInhibit_functionality_with_CAattribute_ResponsiblePD0()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,CA's With RespPD's");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_MultiProcesDCR(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"), GlobalVariables.Input.get("Process_DCR2"), GlobalVariables.Input.get("Process_DCR3"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(SiView.getInstance().getEquipmentInhibitStatus(GlobalVariables.Input.get("EquipmentID1")),"Verified the Equipment inhibit status in Siview");
		Assert.assertTrue(SiView.getInstance().cancelEquipmentInhibit(GlobalVariables.Input.get("EquipmentID1")),"Cancelled/released the Equipment inhibit from siview ");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	}
	@Test
	public void GF_SIL_CARP020__verify_new_CAs_EquipmentAndPDInhibit_functionality_withInvalid_equipment_passed_in_DCR()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,CA's With RespPD's");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_MultiProcesDCR(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"), GlobalVariables.Input.get("Process_DCR2"), GlobalVariables.Input.get("Process_DCR3"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().AssignCorrectiveAction(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"), GlobalVariables.Input.get("ManualCA1"),GlobalVariables.Input.get("ViolationComment1")),"Manual CA is assigned to the Sample");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	}
	@Test
	public void GF_SIL_CARP021__verifyverify_newCAs_AutoEquipmentAndPDInhibit_functionality_withAll_Inhibit_codes_exhausted()
	{
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,CA's With RespPD's");
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		Assert.assertTrue(Space.getInstance().assignCAAttributes(GlobalVariables.Input.get("CA1"),GlobalVariables.Input.get("CAAttributes")),"Assigning CA Attributes");
		Assert.assertTrue(commonScripts.common.CreateBaseChannel_MultiProcesDCR(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"), GlobalVariables.Input.get("Process_DCR2"), GlobalVariables.Input.get("Process_DCR3"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base channel is created");
		Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
		Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
		Assert.assertTrue(Space.getInstance().VerifyAttributes(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("Startdate1"),GlobalVariables.Input.get("Enddate1"),GlobalVariables.Input.get("SampleIndex1"),GlobalVariables.Input.get("Attributes1")),"Attributes details are verified");
		Assert.assertTrue(SiView.getInstance().getPDandEquipmentStatus( GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1"), GlobalVariables.Input.get("Status_Flag")),"Verified the PD and equipment inhibit");
		Assert.assertTrue(SiView.getInstance().cancelPDAndEquipment( GlobalVariables.Input.get("ProductID1"), GlobalVariables.Input.get("EquipmentID1")),"Cancelled PD and equipment inhibit");
		Assert.assertTrue(SiView.getInstance().getEquipmentInhibitStatus(GlobalVariables.Input.get("EquipmentID1")),"Verified the Equipment inhibit status in Siview");
		Assert.assertTrue(SiView.getInstance().cancelEquipmentInhibit(GlobalVariables.Input.get("EquipmentID1")),"Cancelled/released the Equipment inhibit from siview ");
		Assert.assertTrue(Space.getInstance().RestoreCAAttributes(),"restoring older CA Attributes");
		Assert.assertAll();
		
	}
	
	
}
