package regressionSuite;


import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.gf.app.SiView;
import com.gf.app.Space;
import com.gf.util.GlobalVariables;
import com.gf.util.Helper;
import com.gf.util.InitaiteTestdata;

public class ChartCheck_Failure {

	@BeforeClass
	public void initaiteData()
	{
			InitaiteTestdata.getInstance();
			Helper.getInstance().clearSILLogs();
	}
	@BeforeSuite
	public void Pre_Execution()
	{
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Pre and Post Execution");
		Helper.getInstance().clearSILLogs();
		Helper.getInstance().backupSILConfigPorperty();
		Helper.getInstance().VerfiyServerConnection();
		commonScripts.common.ReportEnd();
	}
	@BeforeMethod
	public void VerifyServerConnection()
	{
		Helper.getInstance().VerfiyServerConnection();
	}
	
	@AfterMethod
	public void DeleteChannel()
	{
		//Assert.assertTrue(Space.getInstance().DeleteChannelByFolder(GlobalVariables.LDSName, GlobalVariables.LDSFolderName, GlobalVariables.SPCChannelPathName));
		Space.getInstance().DeleteChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"));
		commonScripts.common.ReportEnd();
	}
	
	@Test
	public void GF_SIL_CCF01__InlineLDS_ChartCheckFailure()
	{	
			SoftAssert Assert= new SoftAssert();
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Chart_Check_Failur");
			GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Helper.getInstance().SetConfigPropertyValue(GlobalVariables.Input.get("UpdateConfigFiles"),GlobalVariables.Input.get("UpdateConfigPropValues")),"SIL config prop are updated");
			Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1")),"Non WIP channel is created");
			Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
			Assert.assertTrue(Helper.getInstance().SearchSilLog(GlobalVariables.Input.get("SIL_Logs")));
			Assert.assertTrue(Helper.getInstance().PropConfigFileReplace(GlobalVariables.Input.get("UpdateConfigFiles")),"Replace the modified Config SIL property file");
			Assert.assertAll();
	
	}
	
	@Test
	public void GF_SIL_CCF02__Simple_ChartCheckFailure_Active_in_InlineLDS()
	{	
			SoftAssert Assert= new SoftAssert();
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Chart_Check_Failur");
			GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created");
			Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
			Assert.assertTrue(Space.getInstance().AddValuationEvents(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("CA1")),"CA's are added to the valuations");
			Assert.assertTrue(Helper.getInstance().SetConfigPropertyValue(GlobalVariables.Input.get("UpdateConfigFiles"),GlobalVariables.Input.get("UpdateConfigPropValues")),"SIL config prop are updated");
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(commonScripts.common.WaitInSeconds(GlobalVariables.Input.get("Wait1")),"waiting for few seconds");
			Assert.assertTrue(Helper.getInstance().SearchSilLog(GlobalVariables.Input.get("SIL_Logs")));
			Assert.assertTrue(Helper.getInstance().PropConfigFileReplace(GlobalVariables.Input.get("UpdateConfigFiles")),"Replace the modified Config SIL property file");
			Assert.assertTrue(SiView.getInstance().getFutureLotStatus(GlobalVariables.Input.get("LotID1")));
			Assert.assertTrue(SiView.getInstance().releaseFutureLotHold(GlobalVariables.Input.get("LotID1")));
			Assert.assertAll();
	
	}
	@Test
	public void GF_SIL_CCF03__Complex_ChartCheckFailure_Active_in_InlineLDS()
	{	
			SoftAssert Assert= new SoftAssert();
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Chart_Check_Failur");
			GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(commonScripts.common.CreateBaseChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1"),GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.LotID),"Base chanel is created");
			Assert.assertTrue(Space.getInstance().AddValuationts(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1"),GlobalVariables.Input.get("ValuationName1")),"Valuations are added to the Channel");
			Assert.assertTrue(Helper.getInstance().SetConfigPropertyValue(GlobalVariables.Input.get("UpdateConfigFiles"),GlobalVariables.Input.get("UpdateConfigPropValues")),"SIL config prop are updated");
			Assert.assertTrue(commonScripts.common.ReuploadDCR(GlobalVariables.Input.get("Metrology_DCR1")),"Reuploaded the metrology DCR");
			Assert.assertTrue(commonScripts.common.WaitInSeconds(GlobalVariables.Input.get("Wait1")),"waiting for few seconds");
			Assert.assertTrue(Helper.getInstance().SearchSilLog(GlobalVariables.Input.get("SIL_Logs")));
			Assert.assertTrue(Helper.getInstance().PropConfigFileReplace(GlobalVariables.Input.get("UpdateConfigFiles")),"Replace the modified Config SIL property file");
			Assert.assertAll();
	
	}

	@Test
	public void GF_SIL_CCF04__InlineLDS_ChartCheckFailure_withCriticality_setBackground()
	{	
			SoftAssert Assert= new SoftAssert();
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Chart_Check_Failur");
			GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Helper.getInstance().SetConfigPropertyValue(GlobalVariables.Input.get("UpdateConfigFiles"),GlobalVariables.Input.get("UpdateConfigPropValues")),"SIL config prop are updated");
			Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1")),"Non WIP channel is created");
			Assert.assertTrue(Space.getInstance().VerifyChannel(GlobalVariables.Input.get("LDSName1"),GlobalVariables.Input.get("LDSFolderName1"),GlobalVariables.Input.get("SPCChannelPathName1"),GlobalVariables.Input.get("ChannelName1")),"Channel is verified in space");
			Assert.assertTrue(Helper.getInstance().SearchNoSilLog(GlobalVariables.Input.get("SIL_Logs")));
			Assert.assertTrue(Helper.getInstance().PropConfigFileReplace(GlobalVariables.Input.get("UpdateConfigFiles")),"Replace the modified Config SIL property file");
			Assert.assertAll();
	
	}
	@Test
	public void GF_SIL_CCF05__InlineLDS_ChartCheckFailure_withSpaceScenario_NoHoldCC()
	{	
			SoftAssert Assert= new SoftAssert();
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Chart_Check_Failur");
			GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			Assert.assertTrue(Helper.getInstance().SetConfigPropertyValue(GlobalVariables.Input.get("UpdateConfigFiles"),GlobalVariables.Input.get("UpdateConfigPropValues")),"SIL config prop are updated");
			Assert.assertTrue(commonScripts.common.MonitoringSpecUpload(GlobalVariables.Input.get("MonitoringSpecFile1"), GlobalVariables.Input.get("PDSetupFile1")),"MOnitoring spec file uploaded");
			Assert.assertTrue(commonScripts.common.CreateChannelExt(GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1")),"Non WIP channel is created");
			Assert.assertTrue(commonScripts.common.WaitInSeconds(GlobalVariables.Input.get("Wait1")),"waiting for few seconds");
			Assert.assertTrue(Helper.getInstance().SearchSilLog(GlobalVariables.Input.get("SIL_Logs")),"verifeid in SIL logs");
			Assert.assertTrue(SiView.getInstance().VerifyFutureLotAttributes(GlobalVariables.Input.get("LotID1"),GlobalVariables.Input.get("Siview_Attribute1"),GlobalVariables.Input.get("Status_Flag")),"Verifying Lot status");
			Assert.assertTrue(SiView.getInstance().releaseFutureLotHold(GlobalVariables.Input.get("LotID1")),"Verifying Lot status");
			Assert.assertTrue(Helper.getInstance().PropConfigFileReplace(GlobalVariables.Input.get("UpdateConfigFiles")),"Replace the modified Config SIL property file");
			Assert.assertAll();
	
	}
	@Test
	public void GF_SIL_CCF06__InlineLDS_ChartCheckFailure_withSpaceScenario_NoCC()
	{	
			SoftAssert Assert= new SoftAssert();
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Chart_Check_Failur");
			Assert.assertTrue(Helper.getInstance().SetConfigPropertyValue(GlobalVariables.Input.get("UpdateConfigFiles"),GlobalVariables.Input.get("UpdateConfigPropValues")),"SIL config prop are updated");
			Assert.assertTrue(commonScripts.common.MonitoringSpecUpload(GlobalVariables.Input.get("MonitoringSpecFile1"), GlobalVariables.Input.get("PDSetupFile1")),"MOnitoring spec file uploaded");
			Assert.assertTrue(commonScripts.common.CreateChannelExt(GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1")),"Non WIP channel is created");
			Assert.assertTrue(commonScripts.common.WaitInSeconds(GlobalVariables.Input.get("Wait1")),"waiting for few seconds");
			Assert.assertTrue(Helper.getInstance().SearchNoSilLog(GlobalVariables.Input.get("SIL_Logs")),"verifeid in SIL logs");
			Assert.assertTrue(SiView.getInstance().VerifyFutureLotAttributes(GlobalVariables.Input.get("LotID1"),GlobalVariables.Input.get("Siview_Attribute1"),GlobalVariables.Input.get("Status_Flag")),"Verifying Lot status");
			Assert.assertTrue(SiView.getInstance().releaseFutureLotHold(GlobalVariables.Input.get("LotID1")),"Verifying Lot status");
			Assert.assertTrue(Helper.getInstance().PropConfigFileReplace(GlobalVariables.Input.get("UpdateConfigFiles")),"Replace the modified Config SIL property file");
			Assert.assertAll();
	
	}
	@Test
	public void GF_SIL_CCF07and09__InlineLDSMissing_ParameterCheckFailure()
	{	
			SoftAssert Assert= new SoftAssert();
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Chart_Check_Failur");
			Assert.assertTrue(Helper.getInstance().SetConfigPropertyValue(GlobalVariables.Input.get("UpdateConfigFiles"),GlobalVariables.Input.get("UpdateConfigPropValues")),"SIL config prop are updated");
			Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1")),"Non WIP channel is created");
			Assert.assertTrue(Helper.getInstance().SearchSilLog(GlobalVariables.Input.get("SIL_Logs")),"verifeid in SIL logs");
			Assert.assertTrue(SiView.getInstance().getFutureLotStatus(GlobalVariables.Input.get("LotID1"),GlobalVariables.Input.get("Status_Flag")),"Verifying Lot status");
			Assert.assertTrue(SiView.getInstance().VerifyFutureLotAttributes(GlobalVariables.Input.get("LotID1"),GlobalVariables.Input.get("Siview_Attribute1"),GlobalVariables.Input.get("Status_Flag")),"Verifying Lot attribute status");
			Assert.assertTrue(SiView.getInstance().releaseFutureLotHold(GlobalVariables.Input.get("LotID1")),"Verifying Lot status");
			Assert.assertTrue(Helper.getInstance().PropConfigFileReplace(GlobalVariables.Input.get("UpdateConfigFiles")),"Replace the modified Config SIL property file");
			Assert.assertAll();
	
	}
	
	@Test
	public void GF_SIL_CCF08and10__SetupLDSMissing_ParameterCheckFailure()
	{	
			SoftAssert Assert= new SoftAssert();
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Chart_Check_Failur");
			Assert.assertTrue(Helper.getInstance().SetConfigPropertyValue(GlobalVariables.Input.get("UpdateConfigFiles"),GlobalVariables.Input.get("UpdateConfigPropValues")),"SIL config prop are updated");
			Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1")),"Non WIP channel is created");
			Assert.assertTrue(Helper.getInstance().SearchSilLog(GlobalVariables.Input.get("SIL_Logs")),"verifeid in SIL logs");
			Assert.assertTrue(SiView.getInstance().getFutureLotStatus(GlobalVariables.Input.get("LotID1"),GlobalVariables.Input.get("Status_Flag")),"Verifying Lot status");
			Assert.assertTrue(SiView.getInstance().VerifyFutureLotAttributes(GlobalVariables.Input.get("LotID1"),GlobalVariables.Input.get("Siview_Attribute1"),GlobalVariables.Input.get("Status_Flag")),"Verifying Lot attribute status");
			Assert.assertTrue(SiView.getInstance().releaseFutureLotHold(GlobalVariables.Input.get("LotID1")),"Verifying Lot status");
			Assert.assertTrue(Helper.getInstance().PropConfigFileReplace(GlobalVariables.Input.get("UpdateConfigFiles")),"Replace the modified Config SIL property file");
			Assert.assertAll();
	
	}
	
	@Test
	public void GF_SIL_CCF11__AutochartingFailure_in_Inline_LDS()
	{	
			SoftAssert Assert= new SoftAssert();
			GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
			GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
			commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Chart_Check_Failur");
			Assert.assertTrue(Helper.getInstance().SetConfigPropertyValue(GlobalVariables.Input.get("UpdateConfigFiles"),GlobalVariables.Input.get("UpdateConfigPropValues")),"SIL config prop are updated");
			Assert.assertTrue(Space.getInstance().CreateTemplate(GlobalVariables.Input.get("LDSName1"), GlobalVariables.Input.get("TemplateFolder1"), GlobalVariables.Input.get("TemplateName1"), GlobalVariables.Input.get("ParameterName1"), GlobalVariables.Input.get("ParameterName1")),"Template is created");
			Assert.assertTrue(Space.getInstance().CreateTemplateWithSameChannel(GlobalVariables.Input.get("LDSName1"), GlobalVariables.Input.get("TemplateFolder1"), GlobalVariables.Input.get("ParameterName1"), GlobalVariables.Input.get("ParameterName2")),"Paste same template ");
			Assert.assertTrue(commonScripts.common.WaitInSeconds(GlobalVariables.Input.get("Wait1")),"waiting for few seconds");
			Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1")),"Non WIP channel is created");
			Assert.assertTrue(commonScripts.common.WaitInSeconds(GlobalVariables.Input.get("Wait1")),"waiting for few seconds");
			Assert.assertTrue(Helper.getInstance().SearchSilLog(GlobalVariables.Input.get("SIL_Logs")),"verifeid in SIL logs");
			Assert.assertTrue(Space.getInstance().DeleteChannel(GlobalVariables.Input.get("LDSName1"), GlobalVariables.Input.get("TemplateFolder1"), GlobalVariables.Input.get("ParameterName1")),"Deleting the template");
			Assert.assertTrue(Helper.getInstance().PropConfigFileReplace(GlobalVariables.Input.get("UpdateConfigFiles")),"Replace the modified Config SIL property file");
			Assert.assertAll();
	
	}

	@Test
	public void GF_SIL_CCF12__AutochartingFailure_in_Setup_LDS()
	{	
		SoftAssert Assert= new SoftAssert();
		GlobalVariables.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		GlobalVariables.Input=commonScripts.common.Testdata(GlobalVariables.TestTitle);
		commonScripts.common.ReportStart(GlobalVariables.TestTitle,"Regression Test,Chart_Check_Failur");
		Assert.assertTrue(Helper.getInstance().SetConfigPropertyValue(GlobalVariables.Input.get("UpdateConfigFiles"),GlobalVariables.Input.get("UpdateConfigPropValues")),"SIL config prop are updated");
		Assert.assertTrue(Space.getInstance().CreateTemplate(GlobalVariables.Input.get("LDSName1"), GlobalVariables.Input.get("TemplateFolder1"), GlobalVariables.Input.get("TemplateName1"), GlobalVariables.Input.get("ParameterName1"), GlobalVariables.Input.get("ParameterName1")),"Template is created");
		Assert.assertTrue(Space.getInstance().CreateTemplateWithSameChannel(GlobalVariables.Input.get("LDSName1"), GlobalVariables.Input.get("TemplateFolder1"), GlobalVariables.Input.get("ParameterName1"), GlobalVariables.Input.get("ParameterName2")),"Paste same template ");
		Assert.assertTrue(commonScripts.common.WaitInSeconds(GlobalVariables.Input.get("Wait1")),"waiting for few seconds");
		Assert.assertTrue(commonScripts.common.CreateChannel(GlobalVariables.Input.get("SpecLimitFile1"), GlobalVariables.Input.get("PDSetupFile1"), GlobalVariables.Input.get("Metrology_DCR1"), GlobalVariables.Input.get("Process_DCR1")),"Non WIP channel is created");
		Assert.assertTrue(commonScripts.common.WaitInSeconds(GlobalVariables.Input.get("Wait1")),"waiting for few seconds");
		Assert.assertTrue(Helper.getInstance().SearchSilLog(GlobalVariables.Input.get("SIL_Logs")),"verifeid in SIL logs");
		Assert.assertTrue(Space.getInstance().DeleteChannel(GlobalVariables.Input.get("LDSName1"), GlobalVariables.Input.get("TemplateFolder1"), GlobalVariables.Input.get("ParameterName1")),"Deleting the template");
		Assert.assertTrue(Helper.getInstance().PropConfigFileReplace(GlobalVariables.Input.get("UpdateConfigFiles")),"Replace the modified Config SIL property file");
		Assert.assertAll();
	
	}
	
	
}
