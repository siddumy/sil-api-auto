package com.gf.util;

import java.io.File;
import java.io.FileNotFoundException;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ibm.mq.jms.MQQueueConnectionFactory;
import com.aventstack.extentreports.Status;


public class MessageQueue {

	static Logger logger=  LogManager.getLogger(MessageQueue.class);
	private static MessageQueue messagequeue=null;
	   private static File file = null;
	   static byte[] arrayOfByte= null;
	   protected static String hostname=null;;
	   protected static int port=0;
	   protected static String qmrname=null;
	   protected static  String channel=null;
	   protected static String qname=null;
	   private static String replyTo=null;
	   protected static  int numberOfThreads=0;
	   private static QueueConnectionFactory factory;
	  
	   public MessageQueue()
	   {
		   try {
		   GetServerValues();
		   }
		   catch(Exception e)
		   {
			   logger.info("Error occured while getting Message queue server details" + e.getMessage());
		   }
	   }
	   public static MessageQueue getInstance()
	   {
		   if(messagequeue==null)
			   messagequeue= new MessageQueue();
		   return messagequeue;
	   }
	   
	public void GetServerValues()
	{
		hostname=ReadPropertyFile.getInstance().getProperty("MSG_hostname").trim();
		port=Integer.parseInt(ReadPropertyFile.getInstance().getProperty("MSG_port").trim());
		qmrname=ReadPropertyFile.getInstance().getProperty("MSG_qmrname").trim();
		channel=ReadPropertyFile.getInstance().getProperty("MSG_channel").trim();
		qname=ReadPropertyFile.getInstance().getProperty("MSG_qname").trim();
		replyTo=ReadPropertyFile.getInstance().getProperty("MSG_replyTo").trim();
		numberOfThreads=Integer.parseInt(ReadPropertyFile.getInstance().getProperty("MSG_numberOfThreads").trim());
	}

	   private static String LoadMQ(String DCRfile) 
	   {
		   MessageQueue jms_Intance= new MessageQueue();
		   
		return jms_Intance.start(DCRfile);
		   
	   }
	   public boolean Load_ProcessDCR(String DCRFile)
	   {
		  if( LoadMQ(DCRFile).equalsIgnoreCase("Uploaded succefully"))
		  {
		   TestReport.GetInstance().log(Status.PASS,"Process DCR uploaded succefully" );
		  return true;
		  }
		  else 
			TestReport.GetInstance().log(Status.FAIL,"Process DCR is Failed upload!:"+ LoadMQ(DCRFile));  
		   return false;
	   }
	   public boolean Load_MetrologyDCR(String DCRFile)
	   {
		   if( LoadMQ(DCRFile).equalsIgnoreCase("Uploaded succefully"))
			  {
			   TestReport.GetInstance().log(Status.PASS,"Metrology DCR uploaded succefully" );
			  return true;
			  }
			  else 
				TestReport.GetInstance().log(Status.FAIL,"Metrology DCR is Failed upload!:"+ LoadMQ(DCRFile));  
			   return false;
	   }
	   
	   protected QueueConnectionFactory getFactory() 
	   {
		   try 
		   {
		       if (factory == null) 
		       {
		          MQQueueConnectionFactory mqFactory = new MQQueueConnectionFactory();
		          mqFactory.setTransportType(1);
		          mqFactory.setHostName(hostname);
		          mqFactory.setPort(port);
		          mqFactory.setQueueManager(qmrname);
		          mqFactory.setChannel(channel);
		          factory = mqFactory;
		       }
		   }
		   catch(JMSException var0)
		   {
			   logger.error("Error occurede while getting MQfactory details " + var0 );
		   }
		   return factory; 
	   }
		    
		    private String start(String Param)   
		    {
		    	
		           try
		           {
		        	file = new File(Param);
				    byte[] arrayOfByte = FileUtils.readFileToByteArray(file);
				    QueueConnection connection = getFactory().createQueueConnection();
		            connection.start();
		            QueueSession session = connection.createQueueSession(false, 1);
		            Queue queue = session.createQueue(qname);
		            QueueSender sender = session.createSender(queue);
		            BytesMessage msg = session.createBytesMessage();
		            msg.writeBytes(arrayOfByte);
		            msg.setJMSReplyTo(session.createQueue(replyTo));
		            sender.send(msg);
		             logger.debug(Param + "is Uploaded succefully");
		             return "Uploaded succefully";
		            
		           }
		           catch (JMSException var1) 
		           {
		              logger.error("Error occured while sending message " + var1);
		              return var1.getMessage();
		           } 
		          
		           catch(FileNotFoundException var2)
		           {
		        	   logger.error("Error occured while getting file " + var2);
		        	   return var2.getMessage();
		           }
		           catch (Exception var3) 
		           {
		        	   logger.error("Error occured while sending message " + var3);
		        	   return var3.getMessage();
		           }
		           
		         
		           
		  
		    }
		    
		    
		
		    
}
