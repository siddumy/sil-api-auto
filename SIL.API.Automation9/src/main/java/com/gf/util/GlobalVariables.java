package com.gf.util;

import java.util.Map;

public class GlobalVariables {
	//Common Space related Global variables
	public static String SP_userName=null;
	public static String SP_Password=null;
	public static String SpacePopertyFile=null;
	public static String LDSName=null;
	public static String LDSFolderName=null;
	public static String SPCChannelPathName=null;
	public static String ChannelName=null;
	public static String Startdate="EMPTY";
	public static String Enddate="EMPTY";
	public static String SampleIndex= "1";
	public static String LotID=null;
	public static String EquipmentID=null;
	public static String EventFolder="SIL";
	public static String ChamberId=null;
	public static String ProductID=null;
	public static String ProductGroup=null;
	public static String RouteId=null;
	public static String ReticleId=null;
	public static String RecipeId=null;
	public static String ReasonCode="All";
	//common utility related Global varaibles
	public static String PDSetupFile= null;
	public static String SpecLimitFile= null;
	public static String Process_DCR= null;
	public static String Metrology_DCR= null;
	public static String ViolationComment=null;
	public static String CA=null;
	public static String Attributes=null;
	public static String TestTitle=null;
//	public static Map<String,String> Input=null;
	
	//Config Variables
	public static String SIL_LogFileName= null;
	public static String SIL_LogFilePath= null;
	public static String SIL_ConfigPropPath= null;
	public static String SIL_DateFormat= null;
	public static String SIL_TimeZone= null;
	public static String ExceStartTime=null;
	public static String ExceEndTime=null;
	public static String SIL_ConfigPropBackupPath=null;
	public static String SetProperty_SIL_Text=null;
//	public static String SILLogs_SearchStartTime=null;
	

	//Space Class temp Variable
	public static String[] Space_OldCAattrib=null;
	public static Map<String, String> Input=null;
	public static long WaitInMilSec=10000;
	public static boolean Reverify_Flag=false;   
}