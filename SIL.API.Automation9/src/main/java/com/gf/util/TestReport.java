package com.gf.util;


import org.apache.poi.openxml4j.exceptions.InvalidOperationException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Protocol;
import com.aventstack.extentreports.reporter.configuration.Theme;

	
	public class TestReport {
	public static ExtentTest Test=null;
	public static ExtentReports _instance=null;
	

	public static ExtentReports Instance()
	{ 
	
		if(_instance==null)
	{
			
			
			ExtentHtmlReporter	htmlreporter =new ExtentHtmlReporter("./ExtentReport/Report.html");
			
			htmlreporter.config().setAutoCreateRelativePathMedia(true);
			htmlreporter.config().setCSS("css-string");
			htmlreporter.config().setDocumentTitle("SIl Automation Report");
			htmlreporter.config().setEncoding("utf-8");
			htmlreporter.config().setJS("js-string");
			htmlreporter.config().setProtocol(Protocol.HTTPS);
			htmlreporter.config().setReportName("Automation Report");
			htmlreporter.config().setTheme(Theme.DARK);
			htmlreporter.config().setTimeStampFormat("MMM dd, yyyy HH:mm:ss a");
			htmlreporter.config().enableTimeline(true);
			htmlreporter.config().getConfigMap();
			htmlreporter.config().getCSS();
			
			
			_instance=new ExtentReports();
			String nameOS = "os.name";
		    String architectureOS = "os.arch";
		    String UserName = "user.name";

		    nameOS = System.getProperty(nameOS);
		    architectureOS = System.getProperty(architectureOS);
		    UserName = System.getProperty(UserName);

			_instance.setSystemInfo("OS : ", nameOS);
			_instance.setSystemInfo("OS Architecture : ", architectureOS);
			_instance.setSystemInfo("User Name : ", UserName);
		
			_instance.attachReporter(htmlreporter);
			
	return _instance;
	}
	return _instance;
	}
	
	
	public static ExtentTest GetInstance(String Arg1,String Arg2,String Arg3)
	{
		String[] ArrayOfCat=null;
		if(Arg3!=null&&!Arg3.isEmpty())
		{ ArrayOfCat=Arg3.split(",");}
		
		if(Test==null)
			{
				Test=TestReport.Instance().createTest(Arg1, Arg2);
				if(ArrayOfCat!=null&&ArrayOfCat.length==1)
				Test.assignCategory(ArrayOfCat[0]);
				else if(ArrayOfCat!=null&&ArrayOfCat.length==2)
					Test.assignCategory(ArrayOfCat[0],ArrayOfCat[1]);
				else if(ArrayOfCat!=null&&ArrayOfCat.length==3)
					Test.assignCategory(ArrayOfCat[0],ArrayOfCat[1],ArrayOfCat[2]);

				return Test;
			}
			return Test;
	}
	

	public static ExtentTest GetInstance()
	{
		if(Test==null)
			{
			throw new InvalidOperationException("Singleton not created - use GetInstance(arg1, arg2)");
			}
			return Test;
	}
	
	public static void RemoveInstance()
	{
	if(Test!=null)
	Test=null;
	}
	
	public static void Endtest()
	{
		//TestReport.Instance().removeTest(TestReport.GetInstance());;
		TestReport.Instance().flush();
	}
	
	}


