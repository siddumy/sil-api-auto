package com.gf.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.util.Hashtable;

import java.util.Properties;
import java.util.TimeZone;
import java.util.Vector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.aventstack.extentreports.Status;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;


public class Helper {
	public static JSch _jschInstance;
	private static Helper helper=null;
	 public static	Session session=null;
	public static Channel channel=null;
	 public static	int retry_Session=0;
	 public static	int retry_Channel=0;
	 public static	Properties prop_File;
	 public static	InputStream inputStream=null;
	 public static boolean PropUpdated_Flag=false;
	 static Logger logger=  LogManager.getLogger(Helper.class);
	
	public static void main(String[] args) 
	{
		
	}

	private Helper()
	{
		InitaiteTestdata.getInstance();
		OpenConnection();
	}

	 public static Helper getInstance()
	  {
		  if(helper==null)
			  helper=new Helper();
		  return helper;
	  }
	
	
	private JSch jschgetInstance()
	{
		if(_jschInstance==null)
			_jschInstance=new JSch();
		return _jschInstance;
	}
	
	public boolean VerfiyServerConnection()
	{
		GlobalVariables.ExceStartTime=StartDateTime();
		return OpenConnection().isConnected();
	}
	public boolean CloseServerConnection()
	{
		if(session!=null||session.isConnected())
		{
			session.disconnect();
		return !session.isConnected();
		}
		else 
			return true;
	}
	private Session OpenConnection()  
	{
		try 
		{
			
		if(session==null)
		{
			Hashtable<String,String> prop=new Hashtable<String,String>();
			 prop.put("StrictHostKeyChecking", "no");
			 prop.put("PreferredAuthentications", 
					    "publickey,keyboard-interactive,password");
			 jschgetInstance();
			session=_jschInstance.getSession(ReadPropertyFile.getInstance().getProperty("UserName"),ReadPropertyFile.getInstance().getProperty("Host_Server"),Integer.parseInt(ReadPropertyFile.getInstance().getProperty("Port")));
			session.setConfig(prop);
			session.setPassword(ReadPropertyFile.getInstance().getProperty("Password"));
	        session.connect();
	        logger.debug("Session is connected successfully");
	        
	        if(!session.isConnected())
	        {
	        		while(retry_Session<10)
	        		OpenConnection();retry_Session++;	
	        }
			
		}
		}
		catch(JSchException e)
		{
			  logger.error("Error occured while creating a session:"+e);
		}
		return session;
	}
	private ChannelSftp CreateSftpChannel()
	{
		try {
		Session session= OpenConnection();
		if(channel!=null&&channel.isConnected())
			channel.disconnect();
		channel = session.openChannel("sftp");
       channel.connect();
       if(channel.isConnected())
       {
       	 ChannelSftp sftpChannel = (ChannelSftp) channel;
         logger.debug("SFTPChannel is created successfully");
    		return sftpChannel;
       }
       while(retry_Channel<10)
       	CreateSftpChannel();retry_Channel++;
       	logger.debug("Unable to create a SFTP Channel");
		}
		catch(JSchException e)
		{
			logger.info("Error occured while creating a SFTPChannel :"+e);
			
		}
		return null;
	}
	
	private Channel CreateExecChannel()
	{
		try 
		{
			Session session= OpenConnection();
			Channel ExecChannel=session.openChannel("exec");
			logger.debug("SFTPChannel is created successfully");
			return ExecChannel;
			
		}
		catch(Exception e)
		{
			return null;
		}
	}

	public boolean ExecCMD(String cmd) 
	{
		try {
		Channel channel=CreateExecChannel();
		 ((ChannelExec)channel).setCommand(cmd);
		 channel.connect();
		 logger.debug(cmd+ ": command is executed successfuly");
		}
		
		catch(JSchException e)
		{
			logger.error(e+ "Error occured while executing the command "+ cmd);
			
		}
		return true;
	}
	public boolean ExecFile(String path,String fileName)
	{
		try 
		{
		String setcmd="cd "+path+"\n"+"./" +fileName;
		Channel channel=CreateExecChannel();
		 ((ChannelExec)channel).setCommand(setcmd);
		 channel.connect();
		 logger.debug(fileName+ ": file is executed successfuly");
		}
		 catch(JSchException e)
			{
			 logger.debug("Failed to execute file"+e);
				
			}
		return true;
	}
	private String CurrentDateTime(String DateFormat, String Timezone) 
	{
		
		try {
			if(DateFormat!=null&&!DateFormat.isEmpty()&&Timezone!=null&&!Timezone.isEmpty())
			{	
			SimpleDateFormat dateTimeInGMT = new SimpleDateFormat(DateFormat);
			dateTimeInGMT.setTimeZone(TimeZone.getTimeZone(Timezone));
			String Sdate=dateTimeInGMT.format(new Date());
			return Sdate;
			}
			else
			{
				logger.debug("Unable to fetch date for Dateformat: "+ DateFormat + Timezone +" Hence returning system Current dateTime");
			return new SimpleDateFormat().format(new Date());
			}
		}
		catch(Exception e)
		{
			logger.debug("Unable to fetch date for Dateformat: "+ DateFormat + Timezone +" Hence returning system Current dateTime");
			return new SimpleDateFormat().format(new Date());
		}
	}
	
	private String StartDateTime()
	{
			return CurrentDateTime(GlobalVariables.SIL_DateFormat,GlobalVariables.SIL_TimeZone);
	}
	private String EndDateTime()
	{
			return CurrentDateTime(GlobalVariables.SIL_DateFormat,GlobalVariables.SIL_TimeZone);
	}
	public boolean SILlogs_StartDateTime()
	{
		GlobalVariables.ExceStartTime = CurrentDateTime(GlobalVariables.SIL_DateFormat,GlobalVariables.SIL_TimeZone);
		return true;
	}
	
	private InputStream GetServerFileStreamReader(String FilePath,String FileName) 
	{
		try 
		{
			String sourceFile=FilePath+FileName;
			ChannelSftp Channel=CreateSftpChannel();
			InputStream Input_Stream=Channel.get(sourceFile);
			logger.debug(FileName+": File Input stream is created ");
			return Input_Stream;
		}
		catch(SftpException e)
		{
			logger.error(FileName+": File Input stream is Not created "+e);
			return null;
		}
	}
	private OutputStream GetServerFileStreamWriter(String FilePath,String FileName) 
	{
		try 
		{
		String sourceFile=FilePath+FileName;
		ChannelSftp Channel=CreateSftpChannel();
		OutputStream output_Stream=Channel.put(sourceFile);
		logger.debug(FileName+": File output stream is created ");
		return output_Stream;
		}
		catch(SftpException e)
		{
			logger.error("Failed to create output stream for file"+ FileName +":-"+ e);
			return null;
		}
	}
	
	private	InputStream GetSILLogFileStreamReader()
	{
		return GetServerFileStreamReader(GlobalVariables.SIL_LogFilePath,GlobalVariables.SIL_LogFileName);
	}
		
	private String SearchSilLog_String(String StartTime, String EndTime, String text) {
		try {

			String strLine;
			InputStream Stream = GetSILLogFileStreamReader();
			BufferedReader br = new BufferedReader(new InputStreamReader(Stream));
			while ((strLine = br.readLine()) != null) {
				if (strLine.contains("INFO  [") || strLine.contains("DEBUG [") || strLine.contains("WARN  [")
						|| strLine.contains("ERROR  [")) {
					String str = strLine.split("INFO|DEBUG|WARN|ERROR")[0].split(",")[0];
					Date date = ConvertDate(str);
					if (date != null && date.after(ConvertDate(StartTime))) {
						while ((strLine = br.readLine()) != null) {
							if (strLine.contains("INFO  [") || strLine.contains("DEBUG [")
									|| strLine.contains("WARN  [") || strLine.contains("ERROR  [")) {
								str = strLine.split("INFO|DEBUG|WARN|ERROR")[0].split(",")[0];
								date = ConvertDate(str);
								if (date.after(ConvertDate(EndTime)))
									break;
							}
							if (strLine.toUpperCase().contains(text.toUpperCase())) {
								Stream.close();
								br.close();
								logger.debug(text + ": verified in SIL Server logs:--" + strLine);

								return strLine;

							}

						}

					}

				}
			}
			br.close();
			Stream.close();
			logger.debug(text + ": Failed to verify in SIL Server logs");
			return "[ " + text + " ] is not found in SILlogs";
		} catch (IOException e) {
			TestReport.GetInstance().log(Status.FAIL, "Error occured while searching the SIL logs" + e);
			logger.error("Error occured while searching the SIL logs" + e);
			return e.getMessage().replace("\n", "") + "not found or Error";
		}
	}

	public boolean SearchNoSilLog(String Text)
	{
		GlobalVariables.ExceEndTime=EndDateTime();
		String SILlog_Str=SearchSilLog_String(GlobalVariables.ExceStartTime,GlobalVariables.ExceEndTime,Text);
		if(SILlog_Str.contains("not found")&&!SILlog_Str.contains("not found or Error"))
		{
			 TestReport.GetInstance().log(Status.PASS,"[ "+ Text+" ]" +" is not present in SIL server logs"); 
				return true;
		}
			else
			{	
				TestReport.GetInstance().log(Status.FAIL,"[ "+ Text+" ]" + " May found in Logs or failed to find due to :- "+ SILlog_Str);  
				return false;
			}
	}
	public boolean SearchSilLog(String Text)
	{
		GlobalVariables.ExceEndTime=EndDateTime();
		String SILlog_Str=SearchSilLog_String(GlobalVariables.ExceStartTime,GlobalVariables.ExceEndTime,Text);
		if(SILlog_Str.contains("not found"))
		{
			TestReport.GetInstance().log(Status.FAIL,"Failed to verify in SIL Server logs due to:- "+SILlog_Str); 
		 	return false;
			
		}
			else
			{
				TestReport.GetInstance().log(Status.PASS,"[ "+ Text+" ]" +": verified in SIL Server logs \n\r " +SILlog_Str); 
				return true;
			}
	}
	
	
	
	private Date ConvertDate(String SDate)
	{
		try 
		{
			return new SimpleDateFormat(GlobalVariables.SIL_DateFormat).parse(SDate.trim());
		}
		catch(Exception e)
		{
			return null;
		}
	}
	public void clearSILLogs()
	{
		try {
		OutputStream writeFile= GetServerFileStreamWriter(GlobalVariables.SIL_LogFilePath,GlobalVariables.SIL_LogFileName);
		BufferedWriter bw= new BufferedWriter(new OutputStreamWriter(writeFile));
		bw.write("");
		bw.flush();
		bw.close();
		writeFile.close();
		logger.debug("SIL Server Logs are cleared");
		//TestReport.GetInstance().log(Status.PASS,"SIL Server Logs are cleared"); 
		}
		catch(Exception e)
		{
		//	 TestReport.GetInstance().log(Status.FAIL,"Error occured while clearing the SIL Servre Logs"+ e); 
			logger.error("Error occured while clearing the SIL Servre Logs"+ e);
		}
	}
	
	private boolean BackupFolderfiles(String Src, String Dstn,String FileType)
	{
		try
		{
			ChannelSftp sftpchannel=CreateSftpChannel();
			sftpchannel.cd(Src);
			@SuppressWarnings("unchecked")
			Vector<ChannelSftp.LsEntry> ArrayofProplist = sftpchannel.ls("*."+FileType);
			for(ChannelSftp.LsEntry PropFileEntry : ArrayofProplist)
			{
				sftpchannel.get(PropFileEntry.getFilename(), Dstn);
			}
			TestReport.GetInstance().log(Status.PASS,FileType+": Files backup is taken successully in :"+Dstn);
			logger.info(FileType+": Files backup is taken successully in :"+Dstn);
		return true;
		}
		catch(SftpException e)
		{
			logger.error("Error occured while taking the backup for Files: "+ e);
			return false;
			
		}
		
	}

	public boolean backupSILConfigPorperty()
	{
		return BackupFolderfiles(GlobalVariables.SIL_ConfigPropPath,GlobalVariables.SIL_ConfigPropBackupPath,"properties");
	}
	public boolean RestoreSILConfigPorperty()
	{
		return BackupFolderfiles(GlobalVariables.SIL_ConfigPropBackupPath,GlobalVariables.SIL_ConfigPropPath,"properties");
	}
	public Properties LoadPorpertyFile(String PropFilePath,String PropFileName)
	{
		prop_File= new Properties();
		try 
		{
		InputStream	stream=GetServerFileStreamReader(PropFilePath,PropFileName);
		prop_File.load(stream);
		logger.info(PropFileName+ " is loaded for properties");
		stream.close();
		return prop_File;
		}
		catch(Exception e)
		{
			logger.error("Failed to load property file for "+PropFileName+":- "+ e);
			return prop_File;	
		}
		
	}

	public Properties LoadPorpertyFile(String PropFileName)
	{
		return LoadPorpertyFile(GlobalVariables.SIL_ConfigPropPath,PropFileName);
	
	}

	public String Getproperty(String FileName,String PropKey)
	{
		Properties prop=LoadPorpertyFile(FileName);
		if(!prop.isEmpty())
		{	
			TestReport.GetInstance().log(Status.PASS,"[ "+PropKey+ " ] Property value is [ "+prop.getProperty(PropKey)+" ]"); 
			return prop.getProperty(PropKey);
			}
		else
		{
			logger.error(PropKey+" is Invalid Key or not found in file: "+ FileName);
			 TestReport.GetInstance().log(Status.FAIL,PropKey+" is Invalid Key or not found in file: "+ FileName); 
			return PropKey+" is Invalid Key or not found in file: "+ FileName;
		}
		}
	public boolean Setproperty(String Filepath,String FileName,String PropKey,String SetValue)
	{
		try
		{
		Properties prop=LoadPorpertyFile(FileName);
		OutputStream Ostream=GetServerFileStreamWriter(Filepath, FileName);
		if(!prop.isEmpty())
		{ 
			prop.setProperty(PropKey,SetValue);
			prop.store(Ostream,"Property Value is updated by Auto Script");
			logger.debug(PropKey+" value is updated with "+ SetValue +" in file "+FileName);
			TestReport.GetInstance().log(Status.PASS,"[ "+PropKey+ " ] Property value is updated to [ "+SetValue+" ]");
			return true;
		}
		else
			{
			logger.error("Failed to load property file or Empty file"+FileName);
			return false;
			}
		}
		catch(IOException e)
		{
			logger.error(" Error occured while setting property key [ " + PropKey +" ] Value [ " + SetValue+ " ]"+e);
			
			return false;
		}
		}
	public boolean Setproperty(String FileName,String PropKey,String SetValue)
	{
		return Setproperty(GlobalVariables.SIL_ConfigPropPath,FileName,PropKey,SetValue);
	}
	
	public boolean SetPropertyValue(String FileNames,String PropertyDetails)
	{
		String[] ArrayofFileNames=FileNames.split("~#~");
		PropUpdated_Flag=false;
		try
		{
		String[] ArrayofFilePropertyDetails=PropertyDetails.split("~#~");
		if(ArrayofFileNames.length!=0)
		{
			for(int i=0;i<ArrayofFileNames.length;i++)
			{
				if(!ArrayofFileNames[i].trim().isEmpty()&& ArrayofFilePropertyDetails[i]!=null&& !ArrayofFilePropertyDetails[i].isEmpty())
				{
					String[] ArrayofPropertyDetails=ArrayofFilePropertyDetails[i].split("~~");
					for(String PropertyKeyValue:ArrayofPropertyDetails )
					{
						if(!Getproperty(ArrayofFileNames[i].trim(), PropertyKeyValue.split("=")[0].trim()).equalsIgnoreCase(PropertyKeyValue.split("=")[1].trim()))
						{
							Setproperty(ArrayofFileNames[i].trim(),PropertyKeyValue.split("=")[0].trim(),PropertyKeyValue.split("=")[1].trim());
							PropUpdated_Flag=true; //Flag will be used for waiting until SIL prop get update if changes done
						}
					}	 
				}
			
			}
			logger.debug("Property details are updated for files:- "+FileNames);	
		return true;
		}
		logger.debug("Please provide file Name and Property values ");
		return false;
	}
		catch(Exception e)
		{
			logger.error("Error occuered while updating property values for:- "+ PropertyDetails+":- "+e);
			return false;
		}
	}
	
	public boolean SetConfigPropertyValue(String FileNames,String PropertyDetails)  
	{
		try {
		GlobalVariables.ExceStartTime=StartDateTime();
		//String str1=GlobalVariables.ExceStartTime.split(":")[0]+GlobalVariables.ExceStartTime.split(":")[1];
			double startMin=Double.parseDouble(StartDateTime().split(":")[1]);
			if(SetPropertyValue(FileNames,PropertyDetails))
		{
			//GlobalVariables.ExceEndTime=EndDateTime();
			double EndMin=Double.parseDouble(StartDateTime().split(":")[1]);
			logger.info("Waiting for updating the SIL property files in 5 Min of interval Time");
			if(PropUpdated_Flag) //If property changed then wait for 5 min of interval
				{
					//while(str1.equals((GlobalVariables.ExceEndTime.split(":")[0]+GlobalVariables.ExceEndTime.split(":")[1]))||(Integer.parseInt(GlobalVariables.ExceEndTime.split(":")[1])%5!=0))
					while(EndMin-startMin<=1||EndMin%5!=0)
					{
						//GlobalVariables.ExceEndTime=EndDateTime();
						 EndMin=Double.parseDouble(StartDateTime().split(":")[1]);
						continue;
					}
					Thread.sleep(10000);
					if(SearchSilLog(GlobalVariables.SetProperty_SIL_Text))
					{
						String str="[ "+ PropertyDetails+" ]"+" Properties are updated in"+ "[ "+ FileNames+" ]";
						 TestReport.GetInstance().log(Status.PASS,str); 
						 return true;
					}
					else 
					{
						 TestReport.GetInstance().log(Status.WARNING,"Properties are updated but not found in SIL Logs"); 
						return true;
					}		
		
				}
			else
			{
				String str="[ "+ PropertyDetails+" ]"+" Properties are already updated in"+ "[ "+ FileNames+" ]";
				 TestReport.GetInstance().log(Status.PASS,str); 
				 return true;
			}
		}
		return false;
		}
		catch(InterruptedException e)
		{
		 TestReport.GetInstance().log(Status.FAIL,"Failed to updated property details"+ PropertyDetails ); 
		return false;
		}
		
	}
	public boolean FileReplace(String Src,String Dstn,String FileName)  
	{
		try 
		{
		ChannelSftp sftpchannel=CreateSftpChannel();
		sftpchannel.cd(Dstn);
		String SrcFileName=Src+FileName;
		File file=new File(SrcFileName);
		sftpchannel.put(new FileInputStream(file),FileName,ChannelSftp.OVERWRITE);
		logger.info(FileName+ " file is replaced in folder"+ Dstn);
		 TestReport.GetInstance().log(Status.PASS,FileName+ " file is replaced in folder"+ Dstn);
		return true;
		}
		catch(SftpException  e)
		{
			TestReport.GetInstance().log(Status.FAIL,e+ "Error occured while replacing file:- "+ FileName );
			logger.error(e+ "Error occured while replacing file:- "+ FileName );
			return false;
		}
		catch( FileNotFoundException e)
		{
			TestReport.GetInstance().log(Status.FAIL,e+ "Error occured while replacing file:- "+ FileName );
			logger.error(e+ "Error occured while replacing file:- "+ FileName );
			return false;
		}
	}
	public boolean FileReplace(String FileName)
	{
		return FileReplace(GlobalVariables.SIL_ConfigPropBackupPath,GlobalVariables.SIL_ConfigPropPath,FileName);
	}
	public boolean PropConfigFileReplace(String FileNames)
	{
		if(PropUpdated_Flag)
		{
			GlobalVariables.ExceStartTime=StartDateTime();
		//	String str1=GlobalVariables.ExceStartTime.split(":")[0]+GlobalVariables.ExceStartTime.split(":")[1];
			boolean temp=false;
			String[] ArrayofFileNames=FileNames.split("~#~");
			for(String Str:ArrayofFileNames)
			{
				temp=FileReplace(Str);
			}
	/*		GlobalVariables.ExceEndTime=EndDateTime();
			while(str1.equals((GlobalVariables.ExceEndTime.split(":")[0]+GlobalVariables.ExceEndTime.split(":")[1]))||(Integer.parseInt(GlobalVariables.ExceEndTime.split(":")[1])%5!=0))
			{
				GlobalVariables.ExceEndTime=EndDateTime();
				continue;
			}
		*/	return  temp;
		}
		else
		{
			TestReport.GetInstance().log(Status.PASS,"Skipping Prop file Replace due to No changes on SIL config properties" );
			logger.info("Skipping Prop file Replace due to No changes on SIL config properties" );
			return true;
		}
		
	}
	public void ClearBackup_CAattributeFile()
	{
		try {
			String appConfigfile=ReadPropertyFile.getInstance().getProperty("AppConfigPropertiespath").trim()+"//"+ReadPropertyFile.getInstance().getProperty("AppConfigPropertiesName").trim();
			String BackupFileName=ReadPropertyFile.getInstance(appConfigfile).getProperty("SIL_CAAttributeBackupFile").trim();
			Properties Pr=ReadPropertyFile.getInstance(BackupFileName).PropertyFile();
			Pr.clear();
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
	}

	@SuppressWarnings("deprecation")
	public void Backup_CAAttributes(String KeyName, String[] ArrayofValues) {

		try {

			String str = "#";
			if (ArrayofValues != null && !(ArrayofValues.length == 0)) {
				for (String st : ArrayofValues)
					str = str + "~#~" + st.trim();
				str=str.replaceAll("#~#~", "").trim();
			} else
				str = "";
			
			String appConfigfile=ReadPropertyFile.getInstance().getProperty("AppConfigPropertiespath").trim()+"//"+ReadPropertyFile.getInstance().getProperty("AppConfigPropertiesName").trim();
			String BackupFileName=ReadPropertyFile.getInstance(appConfigfile).getProperty("SIL_CAAttributeBackupFile").trim();
			Properties Pr=ReadPropertyFile.getInstance(BackupFileName).PropertyFile();
			FileOutputStream outputStrem = new FileOutputStream(BackupFileName);
			Pr.setProperty(KeyName, str);
			Pr.save(outputStrem, "Updating with Latest SIL CA attributes values");
			outputStrem.close();
			
			
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	public boolean CloseSession()
	{
		if(session!=null||session.isConnected())
			session.disconnect();
		return !session.isConnected();
	}
}
