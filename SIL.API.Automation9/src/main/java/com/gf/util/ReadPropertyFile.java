package com.gf.util;

import java.io.FileInputStream;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ReadPropertyFile
{
	static Logger logger=  LogManager.getLogger(ReadPropertyFile.class);
	private static ReadPropertyFile readPropFiles = null;
	private static FileInputStream fileInputStrm = null;
	private Properties SILProp = new Properties();

	static String  gf_configurationFile=null;
	
	private ReadPropertyFile()
	{
		try {
				loadPropertyFile();
			}
		catch (Exception exception) 
		{
			logger.error("Unable to Load the ");
		}
	}
	private ReadPropertyFile(String FileName)
	{
		try {
				loadPropertyFile(FileName);
			}
		catch (Exception exception) 
		{
			logger.error("Unable to Load the ");
		}
	}

	public void loadPropertyFile() 
	{

		//Load the gf.properties file only
		
		try
		{
			 gf_configurationFile = ".//Resources//configFile//gf_configuration.properties";
			fileInputStrm = new FileInputStream(gf_configurationFile);
			SILProp.load(fileInputStrm);
			logger.info(gf_configurationFile +" property file is loaded");

		} 
		catch (Exception e) 
		{
		logger.error("Error occured while loading the property file " + e );
		}

		finally
		{
			try 
			{
				fileInputStrm.close();
			}
			catch(Exception e)
			{
				logger.error("Error occured while loading the property file "+ e);
			}
		}
	}
	public void loadPropertyFile(String FileName) 
	{

		//Load the given file name
		
		try
		{
		
		if(FileName.endsWith(".properties")) {
			String str	 = FileName;
			fileInputStrm = new FileInputStream(str);
			SILProp.load(fileInputStrm);
			
		}
			

		} 
		catch (Exception e) 
		{
		logger.error("Error occured while loading the property file " + e );
		}
		finally
		{
			try 
			{
				fileInputStrm.close();
			}
			catch(Exception e)
			{
				logger.error("Error occured while loading the property file "+ e);
			}
		}

	}
	public static ReadPropertyFile getInstance() 
	{
		//if (readPropFiles == null)
			readPropFiles = new ReadPropertyFile();
		return readPropFiles;
	}
	public static ReadPropertyFile getInstance(String FileName) 
	{
		//if (readPropFiles == null)
			readPropFiles = new ReadPropertyFile(FileName);
		return readPropFiles;
	}

	public String getProperty(String KeyParam) {
		return SILProp.getProperty(KeyParam).trim();
	}

	public void setProperty(String paramString1, String paramString2) {
		SILProp.setProperty(paramString1, paramString2);
	}
	public Properties PropertyFile()
	{
		return SILProp;
	}


}
