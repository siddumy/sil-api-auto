package com.gf.util;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

public class InitaiteTestdata {
	 static Logger logger=  LogManager.getLogger(InitaiteTestdata.class);
	private static InitaiteTestdata readtestdata=null;
	public InitaiteTestdata()
	{
		AssignDataToGlobalData();
	}
	public static InitaiteTestdata getInstance()
	{
			readtestdata=new InitaiteTestdata();
		return readtestdata;
	}
	@SuppressWarnings("unused")
	private static Map<String,String> readtestdata(String TestData_FolderName)
	{
		String appConfigfile=ReadPropertyFile.getInstance().getProperty("AppConfigPropertiespath").trim()+"//"+ReadPropertyFile.getInstance().getProperty("AppConfigPropertiesName").trim();
		String TestDataFolderPath=ReadPropertyFile.getInstance(appConfigfile).getProperty("Test_DataPath").trim();
		String TestDataPropFile=ReadPropertyFile.getInstance(appConfigfile).getProperty("TestDataFile").trim();
		String FolderPath= TestDataFolderPath+"//"+TestData_FolderName+"//";
		Map<String,String> Testdata=new HashMap<String,String>();

		Set<Object> keys =	ReadPropertyFile.getInstance(appConfigfile).PropertyFile().keySet();
		for(Object k:keys){
            String key = (String)k;
          String  Keyvalue=ReadPropertyFile.getInstance(appConfigfile).getProperty(key);
            if(Keyvalue.endsWith("xml")||Keyvalue.endsWith("xlsx")||Keyvalue.endsWith("xls"))
            Testdata.put(key,FolderPath+Keyvalue.trim());
            else
            Testdata.put(key, Keyvalue.trim());
            
        }
		Set<Object> keys1 =	ReadPropertyFile.getInstance(FolderPath+TestData_FolderName+"_"+TestDataPropFile).PropertyFile().keySet();
		for(Object k:keys1){
            String key = (String)k;
            String  Keyvalue=ReadPropertyFile.getInstance(FolderPath+TestData_FolderName+"_"+TestDataPropFile).getProperty(key);
            if(Keyvalue.endsWith("xml")||Keyvalue.endsWith("xlsx")||Keyvalue.endsWith("xls"))
            Testdata.put(key,FolderPath+Keyvalue.trim());
            else
            Testdata.put(key,Keyvalue.trim());    	
	}
		return Testdata;
	}
	
	private static Map<String,String> readExceldata(String TestCaseID)
	{
		try {
		String appConfigfile=ReadPropertyFile.getInstance().getProperty("AppConfigPropertiespath").trim()+"//"+ReadPropertyFile.getInstance().getProperty("AppConfigPropertiesName").trim();
		String TestDataFolderPath=ReadPropertyFile.getInstance(appConfigfile).getProperty("Test_DataPath").trim();
		String TestDataExcelFile=ReadPropertyFile.getInstance(appConfigfile).getProperty("Excel_TestDataFile").trim();
		String FolderPath= TestDataFolderPath+"//"+TestCaseID+"//";
		Map<String,String> Testdata=new LinkedHashMap<String,String>();
		Sheet testdataSheet=getExcelSheet(TestDataFolderPath+"//"+TestDataExcelFile,"SIL");
		int temp;
		
	Cell[] First_columnValues=testdataSheet.getColumn(0);
	for(Cell cell:First_columnValues)
	if(cell.getContents().trim().equalsIgnoreCase(TestCaseID))
	{	temp=cell.getRow();
	Cell[] HeaderRow= testdataSheet.getRow(0);
	Cell[] TestdataRow=testdataSheet.getRow(temp);
	int t1=0;
	for(Cell cell1:TestdataRow)
	{
		if(cell1.getContents().endsWith("xlsx")||cell1.getContents().endsWith("xls")||cell1.getContents().endsWith("xml"))
		Testdata.put(HeaderRow[t1].getContents().trim(), FolderPath+cell1.getContents().trim());
		else
		Testdata.put(HeaderRow[t1].getContents().trim(), cell1.getContents().trim());
		t1++;
	}
	return Testdata;
	
	}
	logger.warn("[ "+TestCaseID+" ]  is not found in Test data file");
		}
		catch(Exception e)
		{
			logger.error("Failed to read the test data for : "+TestCaseID+" due to :-"+e);
		}
	return null;
	}
	
	private static Sheet getExcelSheet(String FileName,String SheetName)
	{
		try
		{
			Workbook workbook= Workbook.getWorkbook(new File(FileName));
			Sheet sheet=workbook.getSheet(SheetName);
			return sheet;
		}
		catch(Exception e)
		{
			logger.error("Failed while readign the excel sheet"+ e);
		}
		return null;
	}
	
	public void AssignDataToGlobalData()
	{	
		String appConfigfile=ReadPropertyFile.getInstance().getProperty("AppConfigPropertiespath").trim()+"//"+ReadPropertyFile.getInstance().getProperty("AppConfigPropertiesName").trim();
	
	
		Map<String,String> TestData1=new HashMap<String,String>();

		Set<Object> keys =	ReadPropertyFile.getInstance(appConfigfile).PropertyFile().keySet();
		for(Object k:keys){
            String key = (String)k;
          String  Keyvalue=ReadPropertyFile.getInstance(appConfigfile).getProperty(key);
            	TestData1.put(key, Keyvalue.trim());
		}
		GlobalVariables.LDSName=TestData1.get("LDSName");
		GlobalVariables.LDSFolderName=TestData1.get("LDSFolderName");
		GlobalVariables.SPCChannelPathName=TestData1.get("SPCChannelPathName");
		GlobalVariables.ChannelName=TestData1.get("ChannelName");
		GlobalVariables.Startdate=TestData1.get("Startdate");
		GlobalVariables.Enddate=TestData1.get("Enddate");
		GlobalVariables.SampleIndex=TestData1.get("SampleIndex");
		GlobalVariables.Attributes=TestData1.get("Attributes");
		GlobalVariables.ViolationComment=TestData1.get("ViolationComment");
		GlobalVariables.CA=TestData1.get("CA");
		GlobalVariables.LotID=TestData1.get("LotID");
		GlobalVariables.ReasonCode=TestData1.get("ReasonCode");
		GlobalVariables.EquipmentID=TestData1.get("EquipmentID");
		GlobalVariables.ChamberId=TestData1.get("ChamberId");
		GlobalVariables.RouteId=TestData1.get("RouteId");
		GlobalVariables.RecipeId=TestData1.get("RecipeId");
		GlobalVariables.ReticleId=TestData1.get("ReticleId");
	
		GlobalVariables.SIL_LogFileName=TestData1.get("SIL_LogFileName");
		GlobalVariables.SIL_LogFilePath=TestData1.get("SIL_LogFilePath");
		GlobalVariables.SIL_ConfigPropPath=TestData1.get("SIL_ConfigPropPath");
		GlobalVariables.SIL_DateFormat=TestData1.get("SIL_DateFormat");
		GlobalVariables.SIL_TimeZone=TestData1.get("SIL_TimeZone");
		GlobalVariables.SIL_ConfigPropBackupPath=TestData1.get("SIL_ConfigPropBackupPath");
		GlobalVariables.SetProperty_SIL_Text=TestData1.get("SetProperty_SIL_Text");
		
	}

	public static Map<String,String> Readtestdata(String TestData_FolderName)
	{
		return readExceldata(TestData_FolderName);
		//return readtestdata(TestData_FolderName);
	}
}
