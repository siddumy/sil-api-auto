package com.gf.util;

import java.io.File;
import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.aventstack.extentreports.Status;


public class SpecUpload {
	private static SpecUpload specupload=null;
	  static Logger logger=  LogManager.getLogger(SpecUpload.class);
	  private String pcpURL = ReadPropertyFile.getInstance().getProperty("pcpDoUpload").trim();
	  
	  private String pdURL = ReadPropertyFile.getInstance().getProperty("pdDoUpload").trim();
	  
	  private String chamberFilter = ReadPropertyFile.getInstance().getProperty("chamberFilterDoUpload").trim();
	  
	  private String futureHold = ReadPropertyFile.getInstance().getProperty("futureHoldDoUpload").trim();
	  
	  private String autoCharting = ReadPropertyFile.getInstance().getProperty("autoChartDoUpload").trim();
	  
	  private String genericKey = ReadPropertyFile.getInstance().getProperty("genericKeyDoUpload").trim();
	  
	  private String monitoringSpec = ReadPropertyFile.getInstance().getProperty("monitoringSpecDoUpload").trim();
	  
	  private String terminateSpecId = ReadPropertyFile.getInstance().getProperty("doTerminateSpecId").trim();
	 
	  public String getPCPURL() {
		    return pcpURL;
		  }
		  
		  public String getPDURL() {
		    return pdURL;
		  }
		  
		  public String getChamberFilter() {
		    return chamberFilter;
		  }
		  
		  public String getFutureHold() {
		    return futureHold;
		  }
		  
		  public String getAutoCharting() {
		    return autoCharting;
		  }
		  
		  public String getGenericKey() {
		    return genericKey;
		  }
		  
		  public String getMonitoringSpec() {
		    return monitoringSpec;
		  }
		  
		  public String getTerminateSpecID() {
		    return terminateSpecId;
		  }
		  
		  public static SpecUpload getInstance()
		  {
			  if(specupload==null)
				  specupload=new SpecUpload();
			  return specupload;
		  }
		  
	  
		private boolean Uploadfiles(String URL,String fileUpload)
	{
			String str = "";
			String FileName=fileUpload.split("//")[fileUpload.split("//").length-1];
		    CloseableHttpClient closeableHttpClient = HttpClients.createDefault();
		    try 
		    {
		    	
		      HttpPost httpPost = new HttpPost(URL);
		      FileBody fileBody = new FileBody(new File(fileUpload));
		      HttpEntity httpEntity1 = MultipartEntityBuilder.create().addPart("upload", (ContentBody)fileBody).addPart("caption", (ContentBody)new StringBody("autoTest", ContentType.TEXT_PLAIN)).build();
		      httpPost.setEntity(httpEntity1);
		      CloseableHttpResponse closeableHttpResponse = closeableHttpClient.execute((HttpUriRequest)httpPost);
		      HttpEntity httpEntity2 = closeableHttpResponse.getEntity();
		      str = EntityUtils.toString(httpEntity2);
		      closeableHttpResponse.close();
		      if(str.contains("Upload Success"))
		      {
		    	  TestReport.GetInstance().log(Status.PASS,FileName+ " :- File is uploaded successfully" );
		    	  logger.debug(FileName+" File Uploaded successfully" );
		    	  return true;
		      }
		      else
		      {
		    	  TestReport.GetInstance().log(Status.PASS,FileName+ " :-is not able to upload or invalid file :"+ str );
		    	  logger.error(FileName+ " is not able to upload or invalid file :"+ str );
		    	  return false;
		      }
		    } 
		    catch (Exception exception)
		    {
		    	logger.error("Error occured while uploading file "+ exception);
		    	  TestReport.GetInstance().log(Status.FAIL," Unable to upload the file "+ fileUpload);
		    } 
		    finally
		    {
		      try
		      {
		        closeableHttpClient.close();
		      } 
		      catch (IOException iOException) 
		      {
		      logger.error("Error occured while uploading file" + iOException.getMessage());
		      } 
		    } 
		return true;
	}

public boolean Uploadspeclimit(String param)
{
	return Uploadfiles(getPCPURL(),param);
}
public boolean UploadResponsiblePD(String param)
{
	return Uploadfiles(getPDURL(),param);
}
public boolean UploadChamberLifter(String param)
{
	return Uploadfiles(getChamberFilter(),param);
}
public boolean uploadGenericKeys(String param)
{
    return Uploadfiles(getGenericKey(), param);
}
public boolean uploadAutoCharting(String param) {
    return Uploadfiles(getAutoCharting(), param);
  }
  
  public boolean uploadFutureHold(String param) {
    return Uploadfiles(getFutureHold(), param);
  }
  
  public boolean uploadMonitorSpec(String param) {
    return Uploadfiles(getMonitoringSpec(), param);
  }

}
