package com.gf.app;

import com.amd.jcs.siview.SiViewHelper;
import com.amd.jcs.siview.code.CS_PPTServiceManager;
import com.amd.jcs.siview.code.CS_PPTServiceManagerHelper;
import com.amd.jcs.siview.code.objectIdentifier_struct;
import com.amd.jcs.siview.code.pptAllAvailableEqpInqResult_struct;
import com.amd.jcs.siview.code.pptBaseResult_struct;
import com.amd.jcs.siview.code.pptCS_UserPrivilegesInqResult_struct;
import com.amd.jcs.siview.code.pptCandidateChamberStatusInqResult_struct;
import com.amd.jcs.siview.code.pptCandidateEqpStatusInqResult_struct;
import com.amd.jcs.siview.code.pptChamberStatusChangeReqResult_struct;
import com.amd.jcs.siview.code.pptEntityIdentifier_struct;
import com.amd.jcs.siview.code.pptEntityInhibitAttributes_struct;
import com.amd.jcs.siview.code.pptEntityInhibitDetailAttributes_struct;
import com.amd.jcs.siview.code.pptEntityInhibitDetailInfo_struct;
import com.amd.jcs.siview.code.pptEntityInhibitInfo_struct;
import com.amd.jcs.siview.code.pptEntityInhibitListInqResult__101_struct;
import com.amd.jcs.siview.code.pptEntityInhibitListInqResult_struct;
import com.amd.jcs.siview.code.pptEntityInhibitReasonDetailInfo_struct;
import com.amd.jcs.siview.code.pptEntityInhibitReqResult_struct;
import com.amd.jcs.siview.code.pptEqpBrInfo_struct;
import com.amd.jcs.siview.code.pptEqpChamberInfo_struct;
import com.amd.jcs.siview.code.pptEqpChamberStatusInfo_struct;
import com.amd.jcs.siview.code.pptEqpChamberStatus_struct;
import com.amd.jcs.siview.code.pptEqpInfoInqResult_struct;
import com.amd.jcs.siview.code.pptEqpPortInfo_struct;
import com.amd.jcs.siview.code.pptEqpPortStatus_struct;
import com.amd.jcs.siview.code.pptEqpStatusChangeRptResult_struct;
import com.amd.jcs.siview.code.pptEqpStatusInfo_struct;
import com.amd.jcs.siview.code.pptExperimentalLotListInqResult_struct;
import com.amd.jcs.siview.code.pptExperimentalLot_struct;
import com.amd.jcs.siview.code.pptFutureHoldListAttributes_struct;
import com.amd.jcs.siview.code.pptFutureHoldListByKeyInqResult_struct;
import com.amd.jcs.siview.code.pptFutureHoldSearchKey_struct;
import com.amd.jcs.siview.code.pptHoldReq_struct;
import com.amd.jcs.siview.code.pptLotHoldListInqResult_struct;
import com.amd.jcs.siview.code.pptLotInCassette_struct;
import com.amd.jcs.siview.code.pptLotInfoInqResult_struct;
import com.amd.jcs.siview.code.pptLotNoteInfoInqResult_struct;
import com.amd.jcs.siview.code.pptLotNoteInfo_struct;
import com.amd.jcs.siview.code.pptLotOperationListFromHistoryInqResult_struct;
import com.amd.jcs.siview.code.pptLotOperationListInqResult_struct;
import com.amd.jcs.siview.code.pptOperationHisInfo_struct;
import com.amd.jcs.siview.code.pptOperationHistoryInqResult_struct;
import com.amd.jcs.siview.code.pptOperationMode_struct;
import com.amd.jcs.siview.code.pptOperationNameAttributes_struct;
import com.amd.jcs.siview.code.pptPortOperationMode_struct;
import com.amd.jcs.siview.code.pptProcHoldListAttributes_struct;
import com.amd.jcs.siview.code.pptProcessHoldCancelReqResult_struct;
import com.amd.jcs.siview.code.pptProcessHoldListInqResult_struct;
import com.amd.jcs.siview.code.pptProcessHoldReqResult_struct;
import com.amd.jcs.siview.code.pptProcessHoldSearchKey_struct;
import com.amd.jcs.siview.code.pptRouteOperationListInqResult_struct;
import com.amd.jcs.siview.code.pptStartCassette_struct;
import com.amd.jcs.siview.code.pptStartRecipe_struct;
import com.amd.jcs.siview.code.pptUserParameterValueInqResult_struct;
import com.amd.jcs.siview.code.pptUserParameterValue_struct;
import com.amd.jcs.siview.code.pptUser_struct;
import com.aventstack.extentreports.Status;
import com.gf.util.ReadPropertyFile;
import com.gf.util.TestReport;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.omg.CORBA.Any;
import org.omg.CORBA.ORB;
import org.omg.CORBA.Object;

public class SiView {
	protected static Logger log = LogManager.getLogger(SiView.class);
	private static SiView siview = null;

	private static String IORFILE = "";

	private static pptUser_struct user = null;

	private static CS_PPTServiceManager siViewConn = null;

	private static String siViewUser = "";

	private static String siViewPassword = "";

	private static Any siInfo = null;

	public SiView() throws Exception {
		getPropertyValues();
		siViewConn();
		siViewUser();
		siInfo = getSiInfo();
	}

	public static SiView getInstance() {
		try {
			if (siview == null)
				siview = new SiView();
		} catch (Exception e) {
			log.error("Error Occured while creating Siview instance");
		}
		return siview;
	}

	private Any getSiInfo() {
		return ORB.init().create_any();
	}

	public boolean getPropertyValues() {
		IORFILE = ReadPropertyFile.getInstance().getProperty("IORFile");
		siViewUser = ReadPropertyFile.getInstance().getProperty("siUserName");
		siViewPassword = ReadPropertyFile.getInstance().getProperty("siPassword");
		return true;
	}

	public CS_PPTServiceManager siViewConn() throws Exception {
		if (siViewConn == null)
			try {
				URL uRL = new URL(IORFILE);
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(uRL.openStream()));
				String str = bufferedReader.readLine();
				bufferedReader.close();
				ORB oRB = ORB.init(new String[0], (Properties) null);
				Object object = oRB.string_to_object(str);
				siViewConn = CS_PPTServiceManagerHelper.narrow(object);
				return siViewConn;
			} catch (Exception exception) {
				log.warn("cannot create CS_PPTServiceManager, customized methods are not available");
				throw exception;
			}
		return siViewConn;
	}

	public pptUser_struct siViewUser() {
		if (user == null)
			user = SiViewHelper.buildUser(siViewUser, siViewPassword);
		return user;
	}

	public boolean getEquipmentInhibitStatus(String EquipmentID) {
		log.debug("Getting equipment inhibit status");
		return getEntityInhibitStatus("Equipment", EquipmentID, true);
	}
	public boolean VerifyEquipmentInhibitAttribute(String EquipmentID,String Attributes) {
		log.debug("Getting Reticle Inhibit Status");
		return VerifyEntityInhibitAttributes("Equipment", EquipmentID, Attributes);
	}
	public boolean getEquipmentInhibitStatus(String EquipmentID, String Status_Flag) {
		boolean flag = true;
		if (Status_Flag != null && !Status_Flag.isEmpty() && Status_Flag.equalsIgnoreCase("false"))
			flag = false;
		log.debug("Getting equipment inhibit status");
		return getEquipmentEntityInhibitStatus(EquipmentID, "Equipment", "", flag);
	}

	public boolean getReticleInhibitStatus(String ReticleID) {
		log.debug("Getting Reticle Inhibit Status");
		return getEntityInhibitStatus("Reticle", ReticleID, true);
	}
	public boolean VerifyReticleInhibitAttributes(String ReticleID,String Attributes) {
		log.debug("Getting Reticle Inhibit Status");
		return VerifyEntityInhibitAttributes("Reticle", ReticleID, Attributes);
	}

	public boolean getReticleInhibitStatus(String ReticleID, String Status_Flag) {
		boolean flag = true;
		if (Status_Flag != null && !Status_Flag.isEmpty() && Status_Flag.equalsIgnoreCase("false"))
			flag = false;
		log.debug("Getting Reticle Inhibit Status");
		return getEntityInhibitStatus("Reticle", ReticleID, flag);
	}

	public boolean getRouteInhibitStatus(String RouteID) {
		log.debug("Get the route inhibit status " + RouteID);
		return getEntityInhibitStatus("Route", RouteID, true);

	}

	public boolean getRouteInhibitStatus(String RouteID, String Status_Flag) {
		boolean flag = true;
		if (Status_Flag != null && !Status_Flag.isEmpty() && Status_Flag.equalsIgnoreCase("false"))
			flag = false;
		log.debug("Get the route inhibit status " + RouteID);
		return getEntityInhibitStatus("Route", RouteID, flag);

	}

	public boolean getRecipeInhibitStatus(String RecipeID) {
		log.debug("Getting Recipe Inhibit Status");
		return getEntityInhibitStatus("Machine Recipe", RecipeID, true);
	}
	public boolean VerifyRecipeInhibitAttributes(String RecipeID,String Attributes) {
		log.debug("Getting Recipe Inhibit Attributes");
		return VerifyEntityInhibitAttributes("Machine Recipe", RecipeID, Attributes);
	}

	public boolean getRecipeInhibitStatus(String RecipeID, String Status_Flag) {
		boolean flag = true;
		if (Status_Flag != null && !Status_Flag.isEmpty() && Status_Flag.equalsIgnoreCase("false"))
			flag = false;
		log.debug("Getting Recipe Inhibit Status");
		return getEntityInhibitStatus("Machine Recipe", RecipeID, flag);
	}

	public boolean getEquipmentAndRecipeInhibitStatus(String EquipmentID, String RecipeID) {
		log.debug("Getting EquimentAnd Recipe Inhibit status");
		return getEntityInhibitStatus("Equipment + Machine Recipe", EquipmentID + " + " + RecipeID, true);
	}
	public boolean VerifyEquipmentAndRecipeInhibitAttributes(String EquipmentID, String RecipeID,String Attributes) {
		log.debug("Getting EquimentAnd Recipe Inhibit attributes");
		return VerifyEntityInhibitAttributes("Equipment + Machine Recipe", EquipmentID + " + " + RecipeID, Attributes);
	}
	public boolean getEquipmentAndRecipeInhibitStatus(String EquipmentID, String RecipeID, String Status_Flag) {
		boolean flag = true;
		if (Status_Flag != null && !Status_Flag.isEmpty() && Status_Flag.equalsIgnoreCase("false"))
			flag = false;
		log.debug("Getting EquimentAnd Recipe Inhibit status");
		return getEntityInhibitStatus("Equipment + Machine Recipe", EquipmentID + " + " + RecipeID, flag);
	}

	public boolean getChamberAndRecipeInhibitStatus(String EquipmentID, String RecipeID) {
		log.debug("Getting EquimentAnd Recipe Inhibit status");
		return getEntityInhibitStatus("Chamber + Machine Recipe", EquipmentID + "+" + RecipeID, true);
	}
	
	public boolean VerifyChamberAndRecipeInhibitAttributes(String EquipmentID, String RecipeID,String Attributes) {
		log.debug("Getting Equiment/chamber And Recipe Inhibit Attributes");
		return VerifyEntityInhibitAttributes("Chamber + Machine Recipe", EquipmentID + "+" + RecipeID, Attributes);
	}
	
	public boolean getChamberAndRecipeInhibitStatus(String EquipmentID, String RecipeID, String Status_Flag) {
		boolean flag = true;
		if (Status_Flag != null && !Status_Flag.isEmpty() && Status_Flag.equalsIgnoreCase("false"))
			flag = false;
		log.debug("Getting EquimentAnd Recipe Inhibit status");
		return getEntityInhibitStatus("Chamber + Machine Recipe", EquipmentID + "+" + RecipeID, flag);
	}
	public boolean getProductGrpPDandEquipmentStatus(String ProductGroup,String PD,String EquipmentID, String Status_Flag) {
		boolean flag = true;
		if (Status_Flag != null && !Status_Flag.isEmpty() && Status_Flag.equalsIgnoreCase("false"))
			flag = false;
		log.debug("Getting Product group and PD and Equipment Inhibit status");
		return getEntityInhibitStatus_101("Product Group + Process Definition + Equipment",ProductGroup+"+"+PD+"+"+ EquipmentID , flag);
	}
	
	public boolean getPDandEquipmentStatus(String PD,String EquipmentID, String Status_Flag) {
		boolean flag = true;
		if (Status_Flag != null && !Status_Flag.isEmpty() && Status_Flag.equalsIgnoreCase("false"))
			flag = false;
		log.debug("Getting PD and Equipment Inhibit status");
		return getEntityInhibitStatus_101("Process Definition + Equipment",PD+"+"+ EquipmentID , flag);
	}


	public boolean getChamberInhibitStatus(String EquipmentID, String ChamberID) {
		log.debug("Getting Chamber Inhibit Status");
		return getEquipmentEntityInhibitStatus(EquipmentID, "Chamber", ChamberID, true);
	}
	public boolean VerifyChamberInhibitAttributes(String EquipmentID, String Attributes) {
		log.debug("Getting Chamber Inhibit Attributes");
		return VerifyEntityInhibitAttributes("Chamber", EquipmentID, Attributes);
	}
	public boolean getChamberInhibitStatus(String EquipmentID, String ChamberID, String Status_Flag) {
		boolean flag = true;
		if (Status_Flag != null && !Status_Flag.isEmpty() && Status_Flag.equalsIgnoreCase("false"))
			flag = false;
		log.debug("Getting Chamber Inhibit Status");
		return getEquipmentEntityInhibitStatus(EquipmentID, "Chamber", ChamberID, flag);
	}

	public String getEquipmentInhibitParam(String paramString1, String paramString2) {
		log.debug("Getting Equipment Inhibit Param");
		return getEquipmentInhibitEntityParams(paramString1, "Equipment", "", paramString2);
	}

	public String getChamberInhibitParam(String paramString1, String paramString2, String paramString3) {
		log.debug("Getting Chamber Inhibit Param");
		return getEquipmentInhibitEntityParams(paramString1, "Chamber", paramString2, paramString3);
	}

	public pptEntityInhibitAttributes_struct getInhibitReticle(String ReticleID) {
		log.debug("Getting Inhibit Reticle");
		pptEntityInhibitListInqResult_struct pptEntityInhibitListInqResult_struct = getEntityInhibitList("Reticle",
				ReticleID);
		pptEntityInhibitInfo_struct[] arrayOfPptEntityInhibitInfo_struct = pptEntityInhibitListInqResult_struct.strEntityInhibitions;
		pptEntityInhibitInfo_struct pptEntityInhibitInfo_struct = arrayOfPptEntityInhibitInfo_struct[0];
		return pptEntityInhibitInfo_struct.entityInhibitAttributes;
	}

	public boolean getEquipmentEntityInhibitStatus(String EquipmentID, String EntityClassName, String ClassID,
			boolean Flag) {
		if (ClassID != null && !ClassID.isEmpty())
			ClassID = ClassID.split(",")[0];
		pptEqpInfoInqResult_struct pptEqpInfoInqResult_struct = siViewConn.TxEqpInfoInq(user, id(EquipmentID), true,
				true, true, true, true, true, true, true);
		pptEntityInhibitAttributes_struct[] arrayOfPptEntityInhibitAttributes_struct = pptEqpInfoInqResult_struct.entityInhibitions;
		if (arrayOfPptEntityInhibitAttributes_struct.length > 0) {
			for (pptEntityInhibitAttributes_struct pptEntityInhibitAttributes_struct : arrayOfPptEntityInhibitAttributes_struct) {
				pptEntityIdentifier_struct[] arrayOfPptEntityIdentifier_struct = pptEntityInhibitAttributes_struct.entities;
				if ((arrayOfPptEntityIdentifier_struct[0]).className.toString().equalsIgnoreCase(EntityClassName)
						&& (arrayOfPptEntityIdentifier_struct[0]).attrib.equalsIgnoreCase(ClassID) && Flag) {
					log.debug("[ " + EquipmentID + " , " + ClassID + " ] is in" + EntityClassName + "inhibit state");
					TestReport.GetInstance().log(Status.PASS,
							"[ " + EquipmentID + " , " + ClassID + " ] is in" + EntityClassName + "inhibit state");
					return true;
				} else if ((arrayOfPptEntityIdentifier_struct[0]).className.toString().equalsIgnoreCase(EntityClassName)
						&& (arrayOfPptEntityIdentifier_struct[0]).attrib.equalsIgnoreCase(ClassID) && !Flag) {
					log.debug("[ " + EquipmentID + " , " + ClassID + " ] is in" + EntityClassName + "inhibit state");
					TestReport.GetInstance().log(Status.FAIL,
							"[ " + EquipmentID + " , " + ClassID + " ] is in" + EntityClassName + "inhibit state");
					return false;
				} else if (!(arrayOfPptEntityIdentifier_struct[0]).className.toString()
						.equalsIgnoreCase(EntityClassName)
						&& !(arrayOfPptEntityIdentifier_struct[0]).attrib.equalsIgnoreCase(ClassID) && !Flag) {
					log.debug("[ " + EquipmentID + " , " + ClassID + " ] is in" + EntityClassName + "inhibit state");
					TestReport.GetInstance().log(Status.PASS,
							"[ " + EquipmentID + " , " + ClassID + " ] is in" + EntityClassName + "inhibit state");
					return true;
				}

			}

			log.debug("[ " + EquipmentID + " , " + ClassID + " ] is not in " + EntityClassName + "inhibit state");
			TestReport.GetInstance().log(Status.FAIL,
					"[ " + EquipmentID + " , " + ClassID + " ] is not in " + EntityClassName + "inhibit state");
			return false;
		} else if (!Flag && arrayOfPptEntityInhibitAttributes_struct.length == 0) {
			log.debug(EquipmentID + " is not in inhibit state");
			TestReport.GetInstance().log(Status.PASS,
					"[ " + EquipmentID + " , " + ClassID + " ] is not in " + EntityClassName + "inhibit state");
			return true;
		}
		log.error(EquipmentID + " is not in inhibit state");
		TestReport.GetInstance().log(Status.FAIL, "[ " + EquipmentID + " ] is not in " + EntityClassName
				+ "inhibit state, please enter valid Equipment ID");
		return false;
	}

	public pptEntityInhibitListInqResult_struct getEntityInhibitList_old(String paramString1, String paramString2) {
		pptCS_UserPrivilegesInqResult_struct pptCS_UserPrivilegesInqResult_struct = siViewConn
				.CS_TxUserPrivilegesInq(user, id(""), "", "");
		siInfo = pptCS_UserPrivilegesInqResult_struct.siInfo;
		pptEntityIdentifier_struct[] arrayOfPptEntityIdentifier_struct = {
				new pptEntityIdentifier_struct("Equipment", id("UTC001"), "", siInfo),
				new pptEntityIdentifier_struct("Machine Recipe", id("EI-TEST.PSX100-1.01"), "", siInfo) };
		String[] arrayOfString = new String[0];
		pptEntityInhibitAttributes_struct pptEntityInhibitAttributes_struct = new pptEntityInhibitAttributes_struct(
				arrayOfPptEntityIdentifier_struct, arrayOfString, "", "", "", "", "", id(""), "", siInfo);
		log.debug("Getting Entity Inhibit List for " + paramString2);
		return siViewConn.TxEntityInhibitListInq(user, pptEntityInhibitAttributes_struct);
	}

	public pptEntityInhibitListInqResult_struct getEntityInhibitList(String paramString1, String paramString2) {
		pptCS_UserPrivilegesInqResult_struct pptCS_UserPrivilegesInqResult_struct = siViewConn
				.CS_TxUserPrivilegesInq(user, id(""), "", "");
		siInfo = pptCS_UserPrivilegesInqResult_struct.siInfo;
		String[] arrayOfString1 = paramString1.split("\\+");
		String[] arrayOfString2 = paramString2.split("\\+");
		if(arrayOfString1.length!=arrayOfString2.length)
			return null;
		pptEntityIdentifier_struct[] arrayOfPptEntityIdentifier_struct = new pptEntityIdentifier_struct[arrayOfString1.length];
		for (byte b = 0; b < arrayOfString1.length; b++)
			arrayOfPptEntityIdentifier_struct[b] = new pptEntityIdentifier_struct(arrayOfString1[b].trim(),
					id(arrayOfString2[b].trim()), "", siInfo);
		String[] arrayOfString3 = new String[0];
		pptEntityInhibitAttributes_struct pptEntityInhibitAttributes_struct = new pptEntityInhibitAttributes_struct(
				arrayOfPptEntityIdentifier_struct, arrayOfString3, "", "", "", "", "", id(""), "", siInfo);
		log.debug("Getting Entity Inhibit List for " + paramString2);
		return siViewConn.TxEntityInhibitListInq(user, pptEntityInhibitAttributes_struct);
	}

	public pptEntityInhibitListInqResult__101_struct getEntityInhibitList_101(String paramString1, String paramString2) {
		pptCS_UserPrivilegesInqResult_struct pptCS_UserPrivilegesInqResult_struct = siViewConn
				.CS_TxUserPrivilegesInq(user, id(""), "", "");
		siInfo = pptCS_UserPrivilegesInqResult_struct.siInfo;
		String[] arrayOfString1 = paramString1.split("\\+");
		String[] arrayOfString2 = paramString2.split("\\+");
		if(arrayOfString1.length!=arrayOfString2.length)
			return null;
		pptEntityIdentifier_struct[] arrayOfPptEntityIdentifier_struct = new pptEntityIdentifier_struct[arrayOfString1.length];
		for (byte b = 0; b < arrayOfString1.length; b++)
			arrayOfPptEntityIdentifier_struct[b] = new pptEntityIdentifier_struct(arrayOfString1[b].trim(),
					id(arrayOfString2[b].trim()), "", siInfo);
		String[] arrayOfString3 = new String[0];
		pptEntityInhibitReasonDetailInfo_struct[] strEntityInhibitReasonDetailInfos= new pptEntityInhibitReasonDetailInfo_struct[0];
		pptEntityInhibitDetailAttributes_struct pptEntityInhibitAttributes_struct = new pptEntityInhibitDetailAttributes_struct(
				arrayOfPptEntityIdentifier_struct, arrayOfString3, "", "", "", "", "", id(""), "", strEntityInhibitReasonDetailInfos, siInfo);
		log.debug("Getting Entity Inhibit List for " + paramString2);
		return siViewConn.TxEntityInhibitListInq__101(user, pptEntityInhibitAttributes_struct,true);
	}
	public boolean getEntityInhibitStatus(String paramString1, String paramString2, boolean Flag) {
		log.info("Paramaters are: " + paramString1 + " & " + paramString2);
		pptEntityInhibitListInqResult_struct pptEntityInhibitListInqResult_struct = getEntityInhibitList(paramString1,
				paramString2);
		if (Flag && pptEntityInhibitListInqResult_struct.strEntityInhibitions.length > 0) {
			log.debug(paramString2 + " is in inhibit state");
			TestReport.GetInstance().log(Status.PASS,
					"[ " + paramString1 + ":-" + paramString2 + " ]" + " is in inhibit state");
			return true;
		} else if (!Flag && pptEntityInhibitListInqResult_struct.strEntityInhibitions.length > 0) {
			log.debug(paramString2 + " is in inhibit state");
			TestReport.GetInstance().log(Status.FAIL,
					"[ " + paramString1 + ":-" + paramString2 + " ]" + " is in inhibit state");
			return false;
		} else if (!Flag && pptEntityInhibitListInqResult_struct.strEntityInhibitions.length == 0) {
			log.info(paramString2 + " is not in inhibit state");
			TestReport.GetInstance().log(Status.PASS,
					"[ " + paramString1 + ":-" + paramString2 + " ]" + " is not in inhibit state");
			return true;
		}

		log.debug(paramString2 + " is not in inhibit state");
		TestReport.GetInstance().log(Status.FAIL,
				"[ " + paramString1 + ":-" + paramString2 + " ]" + " is not in inhibit state");
		return false;
	}
	public boolean getEntityInhibitStatus_101(String paramString1, String paramString2, boolean Flag) {
		log.info("Paramaters are: " + paramString1 + " & " + paramString2);
		pptEntityInhibitListInqResult__101_struct pptEntityInhibitListInqResult_struct = getEntityInhibitList_101(paramString1,
				paramString2);
		if (Flag && pptEntityInhibitListInqResult_struct.strEntityInhibitions.length > 0) {
			log.debug(paramString2 + " is in inhibit state");
			TestReport.GetInstance().log(Status.PASS,
					"[ " + paramString1 + ":-" + paramString2 + " ]" + " is in inhibit state");
			return true;
		} else if (!Flag && pptEntityInhibitListInqResult_struct.strEntityInhibitions.length > 0) {
			log.debug(paramString2 + " is in inhibit state");
			TestReport.GetInstance().log(Status.FAIL,
					"[ " + paramString1 + ":-" + paramString2 + " ]" + " is in inhibit state");
			return false;
		} else if (!Flag && pptEntityInhibitListInqResult_struct.strEntityInhibitions.length == 0) {
			log.info(paramString2 + " is not in inhibit state");
			TestReport.GetInstance().log(Status.PASS,
					"[ " + paramString1 + ":-" + paramString2 + " ]" + " is not in inhibit state");
			return true;
		}

		log.debug(paramString2 + " is not in inhibit state");
		TestReport.GetInstance().log(Status.FAIL,
				"[ " + paramString1 + ":-" + paramString2 + " ]" + " is not in inhibit state");
		return false;
	}

	public boolean inhibitRoute(String paramString1, String paramString2) {
		log.debug(" Inhibit Route" + paramString1);
		return inhibitEntity("Route", paramString1, paramString2);
	}

	public boolean inhibitRecipe(String paramString1, String paramString2, String paramString3) {
		log.debug(" Inhibit Recipe" + paramString2);
		return inhibitEntity("Route", paramString2, paramString3);
	}

	public boolean cancelRouteInhibit(String RouteID, String CancelResonCode) {
		log.debug("Canceling Route Inhibit " + RouteID);
		return cancelEntityInhibit("Route", RouteID, CancelResonCode);
	}

	public boolean cancelRouteInhibit(String RouteID) {
		log.debug("Canceling Route Inhibit " + RouteID);
		return cancelEntityInhibit("Route", RouteID, "ALL");
	}

	public boolean cancelRecipeInhibit(String paramString1, String paramString2, String paramString3) {
		log.debug("Canceling Recipe Inhibit " + paramString2);
		return cancelEntityInhibit(paramString1, paramString2, paramString3);
	}

	public boolean cancelEquipmentAndRecipeInhibit(String EquipmentID, String recipeID) {
		log.debug("Canceling Equiment and Machine recipe Inhibit " + "[ " + EquipmentID + " ," + recipeID + " ]");

		return cancelEntityInhibit("Equipment + Machine Recipe", EquipmentID + " + " + recipeID, "ALL");
	}

	public boolean cancelChamberAndRecipeInhibit(String EquipmentID, String recipeID) {
		log.debug("Canceling Equiment and Machine recipe Inhibit " + "[ " + EquipmentID + " ," + recipeID + " ]");

		return cancelEntityInhibit("Chamber + Machine Recipe", EquipmentID + " + " + recipeID, "ALL");
	}
	public boolean cancelProductGrpAndPDAndEquipment(String ProductGroup ,String PD,String EquipmentID) {
		log.debug("Canceling ProductGroup and PD and Equipment Inhibit " + "[ " + ProductGroup + " ," + PD + " ," +EquipmentID +" ]");

		return cancelEntityInhibit("Product Group + Process Definition + Equipment",ProductGroup+"+"+PD+"+"+ EquipmentID, "ALL");
	}
	

public boolean cancelPDAndEquipment(String PD,String EquipmentID) {
		log.debug("Canceling  PD and Equipment Inhibit " + "[ " + PD + " ," +EquipmentID +" ]");

		return cancelEntityInhibit("Process Definition + Equipment",PD+"+"+ EquipmentID, "ALL");
	}


	public boolean cancelRecipeInhibit(String paramString1, String paramString2) {
		log.debug("Canceling Recipe Inhibit " + paramString2);
		return cancelEntityInhibit(paramString1, paramString2, "ALL");
	}

	public String getRouteInhibitAttributes(String paramString1, String paramString2) {
		log.debug("Getting Route Inhibit Attributes " + paramString1);
		return getInhibitAttributes("Route", paramString1, paramString2);
	}

	public String getRecipeInhibitAttributes(String paramString1, String paramString2, String paramString3) {
		log.debug("Getting Route Inhibit Attributes " + paramString3);
		return getInhibitAttributes(paramString1, paramString2, paramString3);
	}

	public boolean VerifyEntityInhibitAttributes(String ClassID, String ClassID_Details, String Attributes) {
		String[] ArrayofAttributes = Attributes.split("~#~");
		boolean flag = false;
		try {
			if (Attributes != null && !Attributes.isEmpty() &&!Attributes.equalsIgnoreCase("EMPTY"))
				for (String Attrib : ArrayofAttributes) {
					String str = getInhibitAttributes(ClassID, ClassID_Details, Attrib.split("=")[0]);
					if (Attrib.split("=").length!= 2) {
						log.warn("Please add attributes in the proper format--- AttributeName=AttributeValue");
						TestReport.GetInstance().log(Status.WARNING,
								"Please add verifying attributes in the proper format--- AttributeName=AttributeValue");
						continue;
					}
					if (str.equalsIgnoreCase(Attrib.split("=")[1])) {
						log.info("[ " + Attrib.split("=")[0] + "  ] value is [ " + str + " ]");
						TestReport.GetInstance().log(Status.PASS,
								"[ " + Attrib.split("=")[0] + "  ] value is [ " + str + " ]");
						flag = true;
					} else {
						log.warn("[ " + Attrib.split("=")[0] + "  ] Expected value is [ " + Attrib.split("=")[1]
								+ " ] but Actaul Value is [ " + str + " ]");
						TestReport.GetInstance().log(Status.FAIL,
								"[ " + Attrib.split("=")[0] + "  ] Expected value is [ " + Attrib.split("=")[1]
										+ " ] but Actaul Value is [ " + str + " ]");
						flag = false;
					}
				}
			else
				log.warn("Entity Inhibits are null or empty and Please Add Attributes!!");
			TestReport.GetInstance().log(Status.FAIL, "Entity Inhibits are null or empty and Please Add Attributes!!");
			return flag;
		} catch (Exception e) {
			log.warn("Failed while verifying entity inhibit attributes! due to " + e);
			TestReport.GetInstance().log(Status.FAIL,
					"Failed while verifying entity inhibit attributes! due to:- " + e);
			return false;
		}
	}
	private String getInhibitAttributes(String paramString1, String paramString2, String paramString3) {
		String str = "";
		pptEntityInhibitListInqResult__101_struct pptEntityInhibitListInqResult_struct = getEntityInhibitList_101(paramString1,
				paramString2);
		pptEntityInhibitDetailInfo_struct[] arrayOfPptEntityInhibitInfo_struct = pptEntityInhibitListInqResult_struct.strEntityInhibitions;
		pptEntityInhibitDetailInfo_struct pptEntityInhibitInfo_struct = arrayOfPptEntityInhibitInfo_struct[0];
		 pptEntityInhibitDetailAttributes_struct pptEntityInhibitAttributes_struct = pptEntityInhibitInfo_struct.entityInhibitAttributes;
		try {
			SIVIEW siview;
			try {
				siview = SIVIEW.valueOf(paramString3.replaceAll(" ", "").toUpperCase().trim());
			} catch (Exception exception) {
				log.warn("Getting Inhibit Attribute " + SIVIEW.DEFAULT.toString());
				siview = SIVIEW.DEFAULT;
			}
			switch (siview) {
			
			case SUBLOTTYPE:
				str = pptEntityInhibitAttributes_struct.subLotTypes[0].toString();
				log.debug("Attribute value is:" + str);
				return str;
			case STARTTIME:
				str = pptEntityInhibitAttributes_struct.startTimeStamp;
				log.debug("Attribute value is:" + str);
				return str;
			case ENDTIME:
				str = pptEntityInhibitAttributes_struct.endTimeStamp;
				log.debug("Attribute value is:" + str);
				return str;
			case USERID:
			case OWNER:
				str = pptEntityInhibitAttributes_struct.ownerID.identifier;
				log.debug("Attribute value is:" + str);
				return str;
			case REGISTRATIONTIME:
				str = pptEntityInhibitAttributes_struct.claimedTimeStamp;
				log.debug("Attribute value is:" + str);
				return str;
			case REASONCODE:
				str = pptEntityInhibitAttributes_struct.reasonCode;
				log.debug("Attribute value is:" + str);
				return str;
			case COMMENT:
				str = pptEntityInhibitAttributes_struct.reasonDesc;
				log.debug("Attribute value is:" + str);
				return str;
			case RELATEDLOT:
				str = pptEntityInhibitAttributes_struct.strEntityInhibitReasonDetailInfos[0].relatedLotID;
				log.debug("Attribute value is:" + str);
				return str;
			case RELATEDOPERNO:
				str = pptEntityInhibitAttributes_struct.strEntityInhibitReasonDetailInfos[0].relatedOperationNumber;
				log.debug("Attribute value is:" + str);
				return str;
			case RELATEDOPERID:
				str = pptEntityInhibitAttributes_struct.strEntityInhibitReasonDetailInfos[0].relatedProcessDefinitionID;
				log.debug("Attribute value is:" + str);
				return str;
			case RELATEDFAB:
				str = pptEntityInhibitAttributes_struct.strEntityInhibitReasonDetailInfos[0].relatedFabID;
				log.debug("Attribute value is:" + str);
				return str;
			case ROUTEID:
			case RELATEDROUTEID:
				str = pptEntityInhibitAttributes_struct.strEntityInhibitReasonDetailInfos[0].relatedRouteID;
				log.debug("Attribute value is:" + str);
				return str;
			default:
				str = "Empty";
				break;
			}
			
		} catch (Exception exception) {
			if (str != null) {
				log.warn("Getting Inhibit Attribute null");
				str = exception.getMessage();
			}
		}
		log.debug("Attribute value is:" + str);
		return str;
	}

	public boolean cancelEntityInhibit(String ClassID, String paramString2, String paramString3) {
		pptEntityInhibitListInqResult_struct pptEntityInhibitListInqResult_struct = getEntityInhibitList(ClassID,
				paramString2);
		if (pptEntityInhibitListInqResult_struct!=null&&pptEntityInhibitListInqResult_struct.strEntityInhibitions.length != 0) {
			pptBaseResult_struct pptBaseResult_struct = siViewConn.TxEntityInhibitCancelReq(user,
					pptEntityInhibitListInqResult_struct.strEntityInhibitions, id(paramString3), "MTQA Auto cancel");
			if (pptBaseResult_struct.strResult.messageText.trim() != "" && !pptBaseResult_struct.strResult.messageText
					.toUpperCase().contains(("0000000I:Normal end.").toUpperCase())) {
				log.error("Cancel Entity Inhibits: Entity type-" + ClassID + " Entity Name-" + paramString2
						+ " failure reason-" + pptBaseResult_struct.strResult.messageText);

				TestReport.GetInstance().log(Status.FAIL,
						"Cancel Entity Inhibits: Entity type-" + ClassID + " Entity Name-" + paramString2
								+ " failure reason-" + pptBaseResult_struct.strResult.messageText);
				return false;
			}
			log.info("Successfully cancelled Entity Inhibit: Entity type-" + ClassID + " Entity Name-" + paramString2
					+ " failure reason-" + pptBaseResult_struct.strResult.messageText);
			TestReport.GetInstance().log(Status.PASS, "Successfully cancelled Entity Inhibit: Entity type-" + ClassID
					+ " Entity Name-" + paramString2 + " failure reason-" + pptBaseResult_struct.strResult.messageText);
			return true;
		} else
			log.error("Cancel Entity Inhibits: Entity type-[ " + ClassID + " ] Entity Name- [ " + paramString2
					+ " ] failed due to empty/null pptEntityInhibitListInqResult_struct ");
		TestReport.GetInstance().log(Status.FAIL, "Cancel Entity Inhibits: Entity type-" + ClassID + " Entity Name-"
				+ paramString2 + " failed due to empty/null pptEntityInhibitListInqResult_struct ");
		return false;

	}

	public boolean cancelEntityInhibit(String paramString1, String paramString2) {
		log.debug("Cancel Entity inhibit list " + paramString2);
		return cancelEntityInhibit(paramString1, paramString2, "ALL");
	}

	public boolean inhibitEntity(String paramString1, String paramString2, String paramString3) {
		pptCS_UserPrivilegesInqResult_struct pptCS_UserPrivilegesInqResult_struct = siViewConn
				.CS_TxUserPrivilegesInq(user, id(""), "", "");
		siInfo = pptCS_UserPrivilegesInqResult_struct.siInfo;
		pptEntityIdentifier_struct[] arrayOfPptEntityIdentifier_struct = {
				new pptEntityIdentifier_struct(paramString1, id(paramString2), "", siInfo) };
		String[] arrayOfString = new String[0];
		pptEntityInhibitAttributes_struct pptEntityInhibitAttributes_struct = new pptEntityInhibitAttributes_struct(
				arrayOfPptEntityIdentifier_struct, arrayOfString, getFutureDate("yyyy-MM-dd-HH.mm.ss.SSSSSS", "0"),
				getFutureDate("yyyy-MM-dd-HH.mm.ss.SSSSSS", "3"), paramString3, "", "", id(siViewUser), "", siInfo);
		pptEntityInhibitReqResult_struct pptEntityInhibitReqResult_struct = siViewConn.TxEntityInhibitReq(user,
				pptEntityInhibitAttributes_struct, "");
		if (pptEntityInhibitReqResult_struct.strResult.messageText.trim() != ""
				&& !pptEntityInhibitReqResult_struct.strResult.messageText.trim().equals("0000000I:Normal end.")) {
			log.error("Unable to inhibit the entities: Entity type-" + paramString1 + " Entity Name-" + paramString2
					+ " failure reason-" + pptEntityInhibitReqResult_struct.strResult.messageText);
			return false;
		}
		log.info("Successfully inhibited the entities: Entity type-" + paramString1 + " Entity Name-" + paramString2
				+ " failure reason-" + pptEntityInhibitReqResult_struct.strResult.messageText);
		return true;
	}

	public boolean cancelEquipmentInhibit(String paramString1, String paramString2) {
		log.debug("Cancel Equipment Inhibit " + paramString1);
		return cancelEntityInhibit( "Equipment",paramString1, paramString2);
	}

			/*					public boolean cancelEquipmentChamberRecipe() {
									log.debug("Cancel Equipment Inhibit cancelEntityInhibit");
									getEquipmentEntityInhibitStruct("CVD2601", "", "");
									return cancelEntityInhibit("Equipment+Chamber+Recipe", "CVD2601+CHB+F-P-CVD260X.F-P-FTEOS5K5.01", "ALL");
								}
			 */
			
	public boolean cancelReticleInhibit(String paramString1, String paramString2) {
		log.debug("Cancel Reticle Inhibit " + paramString1);
		return cancelEntityInhibit("Reticle", paramString1, paramString2);
	}

	public boolean cancelReticleInhibit(String ReticleID) {
		log.debug("Cancel Reticle Inhibit with reason code ALL");
		return cancelEntityInhibit("Reticle", ReticleID, "ALL");
	}

	public boolean cancelEquipmentInhibit(String EquipmentID) {
		log.debug("Cancel Equipment Inhibit using the reason code ALL");
		return cancelEquipmentInhibit( EquipmentID,"ALL");
	}

	public boolean cancelChamberInhibit(String paramString1, String paramString2, String paramString3) {
		log.debug("Cancel Chamber Inhibit " + paramString2);
		return cancelEquipmentEntityInhibits(paramString1, "Chamber", paramString2, paramString3);
	}

	public boolean cancelChamberInhibit(String EquipmentID, String ChamberID) {
		log.debug("Cancel Chamber Inhibit using the reason code ALL " + ChamberID);
		return cancelEquipmentEntityInhibits(EquipmentID, "Chamber", ChamberID, "ALL");
	}

	public boolean cancelRecipeInhibit(String RecipeID) {
		log.debug("Canceling Recipe Inhibit " + RecipeID);
		return cancelEntityInhibit("Machine Recipe", RecipeID, "ALL");
	}

	public boolean cancelEquipmentEntityInhibits(String EntityID, String ClassID, String paramString3,
			String EntityAttributes) {
		try {
			pptEntityInhibitListInqResult_struct pptEntityInhibitListInqResult_struct = siViewConn
					.TxEntityInhibitListInq(user, getEquipmentEntityInhibitStruct(EntityID, ClassID, paramString3));
			pptEntityInhibitInfo_struct[] arrayOfPptEntityInhibitInfo_struct = pptEntityInhibitListInqResult_struct.strEntityInhibitions;
			ArrayList<pptEntityInhibitInfo_struct> arrayList = new ArrayList<pptEntityInhibitInfo_struct>();
			for (String Str : paramString3.split(",")) {
				for (pptEntityInhibitInfo_struct pptEntityInhibitInfo_struct : arrayOfPptEntityInhibitInfo_struct) {
					if ((pptEntityInhibitInfo_struct.entityInhibitAttributes.entities[0]).attrib.toString()
							.equalsIgnoreCase(Str))
						arrayList.add(pptEntityInhibitInfo_struct);
				}
			}
			pptBaseResult_struct pptBaseResult_struct = siViewConn.TxEntityInhibitCancelReq(user,
					arrayList.<pptEntityInhibitInfo_struct>toArray(new pptEntityInhibitInfo_struct[arrayList.size()]),
					id(EntityAttributes), "MTQA Auto close");
			if (pptBaseResult_struct.strResult.messageText.trim().isEmpty()
					&& !pptBaseResult_struct.strResult.messageText.trim().equals("0000000I:Normal end.")) {
				log.error("Cancel Equipment Entity Inhibits: Entity type-" + ClassID + " Entity Name-" + paramString3
						+ " failure reason-" + pptBaseResult_struct.strResult.messageText);
				TestReport.GetInstance().log(Status.FAIL,
						"Cancel Equipment Entity Inhibits: Entity type-" + ClassID + " Entity Name-" + paramString3
								+ " failure reason-" + pptBaseResult_struct.strResult.messageText);
				return false;
			}
			log.info("Successfully cancelled Equipment Entity Inhibit: Entity type-" + ClassID + " Entity Name-"
					+ paramString3 + " failure reason-" + pptBaseResult_struct.strResult.messageText);
			TestReport.GetInstance().log(Status.PASS,
					"Successfully cancelled Equipment Entity Inhibit: Entity type-" + ClassID + " Entity Name-"
							+ paramString3 + " failure reason-" + pptBaseResult_struct.strResult.messageText);
			return true;
		} catch (Exception exception) {
			log.warn(EntityID + ": is not inhibit state.");
			TestReport.GetInstance().log(Status.WARNING, EntityID + ": is not inhibit state." + exception);

			return false;
		}
	}

	public String getEquipmentInhibitEntityParams(String paramString1, String paramString2, String paramString3,
			String paramString4) {
		pptEntityInhibitAttributes_struct pptEntityInhibitAttributes_struct = getEquipmentEntityInhibitStruct(
				paramString1, paramString2, paramString3);
		if (pptEntityInhibitAttributes_struct != null) {
			SIVIEW sIVIEW;
			String str;
			try {
				sIVIEW = SIVIEW.valueOf(paramString4.replaceAll(" ", "").toUpperCase().trim());
			} catch (Exception exception) {
				sIVIEW = SIVIEW.DEFAULT;
			}
			switch (sIVIEW) {
			case REASONDESCRIPTION:
				return pptEntityInhibitAttributes_struct.reasonDesc;
			case REASONDESC:
				return pptEntityInhibitAttributes_struct.reasonDesc;
			case REASONCODE:
				return pptEntityInhibitAttributes_struct.reasonCode;
			case MEMO:
				return pptEntityInhibitAttributes_struct.memo;
			case OWNERID:
				return pptEntityInhibitAttributes_struct.ownerID.toString();
			case CHAMBER:
				str = pptEntityInhibitAttributes_struct.subLotTypes[0];
				if (null == str)
					str = "*";
				break;
			}
			log.error("Inhibit entity param not found: Param-" + paramString4);
			return "";
		}
		log.error("Inhibit entity not found: Entity-" + paramString1);
		return "";
	}

	public pptEntityInhibitAttributes_struct getEquipmentEntityInhibitStruct(String EntityID, String ClassName,
			String AttributeName) {
		int AttributeNameCnt = 0;
		int temp = 0;
		pptEqpInfoInqResult_struct pptEqpInfoInqResult_struct = siViewConn.TxEqpInfoInq(user, id(EntityID), true, true,
				true, true, true, true, true, true);
		siInfo = pptEqpInfoInqResult_struct.siInfo;
		pptEntityInhibitAttributes_struct[] arrayOfPptEntityInhibitAttributes_struct = pptEqpInfoInqResult_struct.entityInhibitions;
		if (arrayOfPptEntityInhibitAttributes_struct.length > 0) {
			Attribute: for (String Str : AttributeName.split(",")) {
				AttributeNameCnt++;
				int EIAtrCnt = 0;
				for (pptEntityInhibitAttributes_struct pptEntityInhibitAttributes_struct : arrayOfPptEntityInhibitAttributes_struct) {
					EIAtrCnt++;
					pptEntityIdentifier_struct[] arrayOfPptEntityIdentifier_struct = pptEntityInhibitAttributes_struct.entities;
					if ((arrayOfPptEntityIdentifier_struct[0]).className.toString().equalsIgnoreCase(ClassName)
							&& (arrayOfPptEntityIdentifier_struct[0]).attrib.equalsIgnoreCase(Str)) {
						pptEntityInhibitAttributes_struct.reasonCode = "";
						pptEntityInhibitAttributes_struct.reasonDesc = "";
						log.debug("Getting Equipment Entity" + pptEntityInhibitAttributes_struct);
						temp++;
						if (AttributeName.split(",").length > 1 && AttributeName.split(",").length > AttributeNameCnt) {
							continue Attribute;
						}
						if (temp > 0 && AttributeNameCnt == AttributeName.split(",").length)
							return pptEntityInhibitAttributes_struct;

					}
					if (temp > 0 && AttributeNameCnt == AttributeName.split(",").length
							&& EIAtrCnt == arrayOfPptEntityInhibitAttributes_struct.length)
						return pptEntityInhibitAttributes_struct;
				}

			}

			log.warn("Not able to get entity " + AttributeName);
			return null;
		}
		log.warn("Not able to get entity " + AttributeName);
		return null;
	}

	public String verifyChamber(String paramString) {
		ArrayList<String> arrayList = new ArrayList();
		pptAllAvailableEqpInqResult_struct pptAllAvailableEqpInqResult_struct = siViewConn.TxAllAvailableEqpInq(user);
		siViewConn.TxEqpInfoInq(user, id("UTC001"), true, true, true, true, true, true, true, true);
		objectIdentifier_struct[] arrayOfObjectIdentifier_struct = pptAllAvailableEqpInqResult_struct.equipmentIDs;
		for (objectIdentifier_struct objectIdentifier_struct : arrayOfObjectIdentifier_struct) {
			arrayList.add(objectIdentifier_struct.identifier);
			pptEntityIdentifier_struct pptEntityIdentifier_struct = new pptEntityIdentifier_struct();
			pptEntityIdentifier_struct[] arrayOfPptEntityIdentifier_struct = { pptEntityIdentifier_struct };
			(arrayOfPptEntityIdentifier_struct[0]).objectID = objectIdentifier_struct;
			pptEntityInhibitAttributes_struct pptEntityInhibitAttributes_struct = new pptEntityInhibitAttributes_struct();
			pptEntityInhibitAttributes_struct.entities = arrayOfPptEntityIdentifier_struct;
			pptEntityInhibitListInqResult_struct pptEntityInhibitListInqResult_struct = siViewConn
					.TxEntityInhibitListInq(user, pptEntityInhibitAttributes_struct);
			log.debug(pptEntityInhibitListInqResult_struct.toString());
			siViewConn.AMD_TxAllEquipmentStatesInq(user);
			pptAllAvailableEqpInqResult_struct pptAllAvailableEqpInqResult_struct1 = siViewConn
					.TxAllAvailableEqpInq(user);
			log.debug(pptAllAvailableEqpInqResult_struct1.toString());
		}
		return arrayList.toString();
	}

	private objectIdentifier_struct id(String paramString) {
		log.debug("Getting object identifier structure " + paramString);
		return new objectIdentifier_struct(paramString, "");
	}

	public pptLotInfoInqResult_struct getLotInfo(String paramString) {
		objectIdentifier_struct[] arrayOfObjectIdentifier_struct = { id(paramString) };
		pptLotInfoInqResult_struct pptLotInfoInqResult_struct = siViewConn.TxLotInfoInq(user,
				arrayOfObjectIdentifier_struct, true, false, false, false, true, false, false, false, false, false,
				false, true, false, false, false);
		if (pptLotInfoInqResult_struct.strResult.returnCode.equalsIgnoreCase("0")) {
			log.debug("successfully retrieved lot information for LOT: " + paramString);
		} else {
			log.error("unable to retrieve lot information for LOT: " + paramString + " Reason code: "
					+ pptLotInfoInqResult_struct.strResult.returnCode);
		}
		return pptLotInfoInqResult_struct;
	}

	public String getLotDetails(String paramString1, String paramString2) {
		SIVIEW sIVIEW;
		String str = "";
		log.info("getLotDetails() parameter is: " + paramString1 + " Detail " + paramString2.toString());
		objectIdentifier_struct[] arrayOfObjectIdentifier_struct = { id(paramString1) };
		pptLotInfoInqResult_struct pptLotInfoInqResult_struct = siViewConn.TxLotInfoInq(user,
				arrayOfObjectIdentifier_struct, true, false, false, false, true, false, false, false, false, false,
				false, true, false, false, false);
		try {
			sIVIEW = SIVIEW.valueOf(paramString2.replaceAll(" ", "").toUpperCase().trim());
		} catch (Exception exception) {
			log.warn("Not a valid SIVIEW constant " + SIVIEW.DEFAULT.toString());
			sIVIEW = SIVIEW.DEFAULT;
		}
		switch (sIVIEW) {
		case PRIORITYCLASS:
			str = (pptLotInfoInqResult_struct.strLotInfo[0]).strLotBasicInfo.priorityClass;
			log.debug(paramString1 + "Lot Details " + paramString2 + " : " + str);
			return str;
		case LOTTYPE:
			str = (pptLotInfoInqResult_struct.strLotInfo[0]).strLotBasicInfo.lotType;
			log.debug(paramString1 + "Lot Details " + paramString2 + " : " + str);
			return str;
		case INTERNALPRIORITY:
			str = (pptLotInfoInqResult_struct.strLotInfo[0]).strLotBasicInfo.internalPriority;
			log.debug(paramString1 + "Lot Details " + paramString2 + " : " + str);
			return str;
		case EXTERNALPRIORITY:
			str = (pptLotInfoInqResult_struct.strLotInfo[0]).strLotBasicInfo.externalPriority;
			log.debug(paramString1 + "Lot Details " + paramString2 + " : " + str);
			return str;
		case TOTALWAFER:
			str = Integer.toString((pptLotInfoInqResult_struct.strLotInfo[0]).strLotBasicInfo.totalWaferCount);
			log.debug(paramString1 + "Lot Details " + paramString2 + " : " + str);
			return str;
		case PRODUCTWAFER:
			str = Integer.toString((pptLotInfoInqResult_struct.strLotInfo[0]).strLotBasicInfo.productWaferCount);
			log.debug(paramString1 + "Lot Details " + paramString2 + " : " + str);
			return str;
		case CONTROLWAFER:
			str = Integer.toString((pptLotInfoInqResult_struct.strLotInfo[0]).strLotBasicInfo.controlWaferCount);
			log.debug(paramString1 + "Lot Details " + paramString2 + " : " + str);
			return str;
		case SUBLOTTYPE:
			str = (pptLotInfoInqResult_struct.strLotInfo[0]).strLotBasicInfo.subLotType;
			log.debug(paramString1 + "Lot Details " + paramString2 + " : " + str);
			return str;
		case OPERATIONID:
			str = (pptLotInfoInqResult_struct.strLotInfo[0]).strLotOperationInfo.operationID.identifier;
			log.debug(paramString1 + "Lot Details " + paramString2 + " : " + str);
			return str;
		case ROUTEID:
			str = (pptLotInfoInqResult_struct.strLotInfo[0]).strLotOperationInfo.routeID.identifier;
			log.debug(paramString1 + "Lot Details " + paramString2 + " : " + str);
			return str;
		case BANKID:
			str = (pptLotInfoInqResult_struct.strLotInfo[0]).strLotBasicInfo.bankID.identifier;
			log.debug(paramString1 + "Lot Details " + paramString2 + " : " + str);
			return str;
		}
		return "EMPTY";
	}

	public String getLotProductInfo(String paramString1, String paramString2) {
		SIVIEW sIVIEW;
		String str = "";
		log.info("getLotDetails() parameter is: " + paramString1 + " Detail: " + paramString2.toString());
		objectIdentifier_struct[] arrayOfObjectIdentifier_struct = { id(paramString1) };
		pptLotInfoInqResult_struct pptLotInfoInqResult_struct = siViewConn.TxLotInfoInq(user,
				arrayOfObjectIdentifier_struct, true, false, false, false, true, false, false, true, false, false,
				false, true, false, false, false);
		try {
			sIVIEW = SIVIEW.valueOf(paramString2.replaceAll(" ", "").toUpperCase().trim());
		} catch (Exception exception) {
			log.warn("Not a valid SIVIEW constant " + SIVIEW.DEFAULT.toString());
			sIVIEW = SIVIEW.DEFAULT;
		}
		switch (sIVIEW) {
		case PRODUCTID:
			str = (pptLotInfoInqResult_struct.strLotInfo[0]).strLotProductInfo.productID.identifier;
			log.debug(paramString1 + "Lot Product " + paramString2 + " : " + str);
			return str;
		case PRODUCTGROUP:
			str = (pptLotInfoInqResult_struct.strLotInfo[0]).strLotProductInfo.productGroupID.identifier;
			log.debug(paramString1 + "Lot Product " + paramString2 + " : " + str);
			return str;
		case MFGLAYER:
			str = (pptLotInfoInqResult_struct.strLotInfo[0]).strLotProductInfo.manufacturingLayer;
			log.debug(paramString1 + "Lot Product " + paramString2 + " : " + str);
			return str;
		case TECHCODE:
			str = (pptLotInfoInqResult_struct.strLotInfo[0]).strLotProductInfo.technologyCode;
			log.debug(paramString1 + "Lot Product " + paramString2 + " : " + str);
			return str;
		case RETICLESET:
			str = (pptLotInfoInqResult_struct.strLotInfo[0]).strLotProductInfo.reticleSetID;
			log.debug(paramString1 + "Lot Product " + paramString2 + " : " + str);
			return str;
		}
		return "EMPTY";
	}

	public String getLotStatus(String paramString) {
		String str = ((getLotInfo(paramString)).strLotInfo[0]).strLotBasicInfo.lotStatus;
		log.info("Get lot status for LOT-" + paramString + ":" + str);
		return str;
	}

	public String getOperationName(String paramString) {
		String str = ((getLotInfo(paramString)).strLotInfo[0]).strLotOperationInfo.operationName;
		log.info("Get lot operation name for LOT-" + paramString + ":" + str);
		return str;
	}

	public String getOperationID(String paramString) {
		String str = ((getLotInfo(paramString)).strLotInfo[0]).strLotOperationInfo.operationID.identifier;
		log.info("Get lot operation id for LOT-" + paramString + ":" + str);
		return str;
	}

	public String getOperationNumber(String paramString) {
		String str = ((getLotInfo(paramString)).strLotInfo[0]).strLotOperationInfo.operationNumber;
		log.info("Get lot operation number for LOT-" + paramString + ":" + str);
		return str;
	}

	public String getReasonCode(String paramString) {
		String str = (getLotInfo(paramString)).strResult.returnCode;
		log.info("Get lot reason code for LOT-" + paramString + ":" + str);
		return str;
	}

	public String getLotHoldAttributes(String paramString1, String paramString2) {
		SIVIEW sIVIEW;
		pptLotHoldListInqResult_struct pptLotHoldListInqResult_struct = lotHoldList(paramString1);
		String str = "";
		int i = pptLotHoldListInqResult_struct.strLotHoldListAttributes.length;
		try {
			sIVIEW = SIVIEW.valueOf(paramString2.replaceAll(" ", "").toUpperCase().trim());
		} catch (Exception exception) {
			log.warn("Not a valid SIVIEW constant " + SIVIEW.DEFAULT.toString());
			sIVIEW = SIVIEW.DEFAULT;
		}
		switch (sIVIEW) {
		case HOLDTYPE:
			str = (pptLotHoldListInqResult_struct.strLotHoldListAttributes[i - 1]).holdType;
			log.debug(paramString1 + " lot Hold Attributes " + str);
			return str;
		case REASONCODEID:
			str = (pptLotHoldListInqResult_struct.strLotHoldListAttributes[i - 1]).reasonCodeID.toString();
			log.debug(paramString1 + " lot Hold Attributes " + str);
			return str;
		case REASONCODE:
			str = (pptLotHoldListInqResult_struct.strLotHoldListAttributes[i - 1]).reasonCodeID.identifier;
			log.debug(paramString1 + " lot Hold Attributes " + str);
			return str;
		case USERID:
			str = (pptLotHoldListInqResult_struct.strLotHoldListAttributes[i - 1]).userID.toString();
			log.debug(paramString1 + " lot Hold Attributes " + str);
			return str;
		case USERNAME:
			str = (pptLotHoldListInqResult_struct.strLotHoldListAttributes[i - 1]).userName;
			log.debug(paramString1 + " lot Hold Attributes " + str);
			return str;
		case RESPONSIBLEOPERATIONMARK:
			str = (pptLotHoldListInqResult_struct.strLotHoldListAttributes[i - 1]).responsibleOperationMark;
			log.debug(paramString1 + " lot Hold Attributes " + str);
			return str;
		case RESPONSIBLEROUTEID:
			str = (pptLotHoldListInqResult_struct.strLotHoldListAttributes[i - 1]).responsibleRouteID.toString();
			log.debug(paramString1 + " lot Hold Attributes " + str);
			return str;
		case RESPONSIBLEOPERATIONNUMBER:
			str = (pptLotHoldListInqResult_struct.strLotHoldListAttributes[i - 1]).responsibleOperationNumber;
			log.debug(paramString1 + " lot Hold Attributes " + str);
			return str;
		case RELATEDLOTID:
			str = (pptLotHoldListInqResult_struct.strLotHoldListAttributes[i - 1]).relatedLotID.toString();
			log.debug(paramString1 + " lot Hold Attributes " + str);
			return str;
		case CLAIMMEMO:
			str = (pptLotHoldListInqResult_struct.strLotHoldListAttributes[i - 1]).claimMemo;
			log.debug(paramString1 + " lot Hold Attributes " + str);
			return str;
		case SIINFO:
			str = (pptLotHoldListInqResult_struct.strLotHoldListAttributes[i - 1]).siInfo.toString();
			log.debug(paramString1 + " lot Hold Attributes " + str);
			return str;
		}
		str = null;
		log.debug(paramString1 + " lot Hold Attributes " + str);
		return str;
	}

	private boolean VerifyFutureLotAttributes(String LotID, String Attribute_NameValues, boolean status_falg) {
		try {
			if (LotID != null && Attribute_NameValues != null && !LotID.isEmpty() && !Attribute_NameValues.isEmpty()) {

				String[] ArrayofAttrName = Attribute_NameValues.split("\n|~#~");

				for (String Attributes : ArrayofAttrName) {
					String[] Attribute = Attributes.split("=");
					String str = getFutureLotHoldAttributes(LotID, Attribute[0]);
					if (status_falg && str.toUpperCase().contains(Attribute[1].toUpperCase())) {
						TestReport.GetInstance().log(Status.PASS, "[ " + Attribute[0] + " ] value is [ " + str + " ]");

					} else if (status_falg && !str.toUpperCase().contains(Attribute[1].toUpperCase())) {
						TestReport.GetInstance().log(Status.FAIL, "[ " + Attribute[0] + " ] Expected value is [ "
								+ Attribute[1] + " ] But Actual value is [ " + str + " ]");
						return false;
					}
					else if (!status_falg && str.toUpperCase().contains(Attribute[1].toUpperCase())) {
						TestReport.GetInstance().log(Status.FAIL, "[ " + Attribute[0] + " ] Not Expected value is [ "
								+ Attribute[1] + " ] But Actual value is [ " + str + " ]");
						return false;

					} else if (!status_falg && !str.toUpperCase().contains(Attribute[1].toUpperCase())) {
						TestReport.GetInstance().log(Status.PASS, "[ " + Attribute[0] + " ] Not Expected value is [ "
								+ Attribute[1] + " ] and Actual value is [ " + str + " ]");

					}

					else
						{TestReport.GetInstance().log(Status.WARNING,
								"Attributes Name and Attributes values lenght are not matching!!");
						}
				}
				return true;
			}
			TestReport.GetInstance().log(Status.WARNING, "LotID/Attribute Name or value is null or empty!!");

		} catch (Exception e) {
			TestReport.GetInstance().log(Status.FAIL, "Error occured while verifying the future lot attributes:- " + e);
		}
		return false;

	}
	
	public boolean VerifyFutureLotAttributes(String LotID, String Siview_Attributes,String status_flag)
	{
		boolean flag=true;
		if(status_flag!=null&&!status_flag.isEmpty()&&status_flag.equalsIgnoreCase("false"))
		flag=false;
		return VerifyFutureLotAttributes(LotID,Siview_Attributes,flag);
	}
	public String getFutureLotHoldAttributes(String paramString1, String paramString2) {
		SIVIEW sIVIEW;
		pptFutureHoldListAttributes_struct pptFutureHoldListAttributes_struct = getFutureLotHoldList(paramString1)[0];
		String str = "";
		try {
			sIVIEW = SIVIEW.valueOf(paramString2.replaceAll(" ", "").toUpperCase().trim());
		} catch (Exception exception) {
			log.warn("Not a valid SIVIEW constant " + SIVIEW.DEFAULT.toString());
			sIVIEW = SIVIEW.DEFAULT;
		}
		switch (sIVIEW) {
		case LOTID:
			str = pptFutureHoldListAttributes_struct.lotID.identifier;
			log.debug(" Future Lot Hold Attributes " + str);
			return str;
		case REPORTTIMESTAMP:
			str = pptFutureHoldListAttributes_struct.reportTimeStamp;
			log.debug(" Future Lot Hold Attributes " + str);
			return str;
		case REASONCODE:
			str = pptFutureHoldListAttributes_struct.reasonCodeID.identifier;
			log.debug(" Future Lot Hold Attributes " + str);
			return str;
		case REASONDESCRIPTION:
			str = pptFutureHoldListAttributes_struct.reasonCodeDescription;
			log.debug(" Future Lot Hold Attributes " + str);
			return str;
		case HOLDTYPE:
			str = pptFutureHoldListAttributes_struct.holdType;
			log.debug(" Future Lot Hold Attributes " + str);
			return str;
		case HOLDTIMING:
			if (pptFutureHoldListAttributes_struct.postFlag) {
				str = "Post";
			} else {
				str = "Previous";
			}
			log.debug(" Future Lot Hold Attributes " + str);
			return str;
		case CANCELABLE:
			str = pptFutureHoldListAttributes_struct.cancelableFlag + "";
			log.debug(" Future Lot Hold Attributes " + str);
			return str;
		case ROUTEID:
			str = pptFutureHoldListAttributes_struct.routeID.identifier;
			log.debug(" Future Lot Hold Attributes " + str);
			return str;
		case OPERATIONNUMBER:
			str = pptFutureHoldListAttributes_struct.operationNumber;
			log.debug(" Future Lot Hold Attributes " + str);
			return str;
		case OPERATIONNAME:
			str = pptFutureHoldListAttributes_struct.operationName;
			log.debug(" Future Lot Hold Attributes " + str);
			return str;
		case USERID:
			str = pptFutureHoldListAttributes_struct.userID.identifier;
			log.debug(" Future Lot Hold Attributes " + str);
			return str;
		case USERNAME:
			str = pptFutureHoldListAttributes_struct.userName;
			log.debug(" Future Lot Hold Attributes " + str);
			return str;
		case RELATEDLOT:
			str = pptFutureHoldListAttributes_struct.relatedLotID.identifier;
			log.debug(" Future Lot Hold Attributes " + str);
			return str;
		case CLAIMMEMO:
			str = pptFutureHoldListAttributes_struct.claimMemo;
			log.debug(" Future Lot Hold Attributes " + str);
			return str;
		}
		str = null;
		log.debug(" Future Lot Hold Attributes " + str);
		return str;
	}

	public String getEquipmentInhibitAttributes(String paramString1, String paramString2) {
		pptEntityInhibitAttributes_struct pptEntityInhibitAttributes_struct = getInhibitEquipment(paramString1);
		if (pptEntityInhibitAttributes_struct.entities.length == 0) {
			log.error(paramString1 + ": is not in inhibit state");
			return null;
		}
		String str = "";
		try {
			SIVIEW sIVIEW;
			try {
				sIVIEW = SIVIEW.valueOf(paramString2.replaceAll(" ", "").toUpperCase().trim());
			} catch (Exception exception) {
				log.warn("Not a valid SIVIEW constant " + SIVIEW.DEFAULT.toString());
				sIVIEW = SIVIEW.DEFAULT;
			}
			switch (sIVIEW) {
			case ENTITY:
				str = (pptEntityInhibitAttributes_struct.entities[0]).objectID.identifier;
				log.debug(" Equipment Inhibit Attributes " + str);
				return str;
			case SUBLOTTYPE:
				str = pptEntityInhibitAttributes_struct.subLotTypes[0].toString();
				log.debug(" Equipment Inhibit Attributes " + str);
				return str;
			case STARTTIME:
				str = pptEntityInhibitAttributes_struct.startTimeStamp;
				log.debug(" Equipment Inhibit Attributes " + str);
				return str;
			case ENDTIME:
				str = pptEntityInhibitAttributes_struct.endTimeStamp;
				log.debug(" Equipment Inhibit Attributes " + str);
				return str;
			case OWNER:
				str = pptEntityInhibitAttributes_struct.ownerID.identifier;
				log.debug(" Equipment Inhibit Attributes " + str);
				return str;
			case REGISTRATIONTIME:
				str = pptEntityInhibitAttributes_struct.claimedTimeStamp;
				log.debug(" Equipment Inhibit Attributes " + str);
				return str;
			case REASONCODE:
				str = pptEntityInhibitAttributes_struct.reasonCode;
				log.debug(" Equipment Inhibit Attributes " + str);
				return str;
			case COMMENT:
				str = pptEntityInhibitAttributes_struct.reasonDesc;
				log.debug(" Equipment Inhibit Attributes " + str);
				return str;
			}
			str = null;
		} catch (Exception exception) {
			if (str != null)
				str = "*";
		}
		log.debug(" Equipment Inhibit Attributes " + str);
		return str;
	}

	public String getReticleInhibitAttributes(String paramString1, String paramString2) {
		String str = "";
		try {
			pptEntityInhibitAttributes_struct pptEntityInhibitAttributes_struct = getInhibitReticle(paramString1);
			if (pptEntityInhibitAttributes_struct.entities.length == 0) {
				log.error(paramString1 + ": is not in inhibit state");
				return null;
			}
			try {
				SIVIEW sIVIEW;
				try {
					sIVIEW = SIVIEW.valueOf(paramString2.replaceAll(" ", "").toUpperCase().trim());
				} catch (Exception exception) {
					log.warn("Not a valid SIVIEW constant " + SIVIEW.DEFAULT.toString());
					sIVIEW = SIVIEW.DEFAULT;
				}
				switch (sIVIEW) {
				case RETICLE:
					str = (pptEntityInhibitAttributes_struct.entities[0]).objectID.identifier;
					break;
				case SUBLOTTYPE:
					str = pptEntityInhibitAttributes_struct.subLotTypes[0];
					break;
				case STARTTIME:
					str = pptEntityInhibitAttributes_struct.startTimeStamp;
					break;
				case ENDTIME:
					str = pptEntityInhibitAttributes_struct.endTimeStamp;
					break;
				case OWNER:
					str = pptEntityInhibitAttributes_struct.ownerID.identifier;
					break;
				case REGISTRATIONTIME:
					str = pptEntityInhibitAttributes_struct.claimedTimeStamp;
					break;
				case REASONCODE:
					str = pptEntityInhibitAttributes_struct.reasonCode;
					break;
				case COMMENT:
					str = pptEntityInhibitAttributes_struct.memo;
					break;
				}
			} catch (Exception exception) {
				if (str != null)
					str = "*";
			}
		} catch (Exception exception) {
			log.error(paramString1 + ": is not in inhibit state. Error message is:" + exception.getMessage());
		}
		log.debug(" Reticle Inhibit Attributes " + str);
		return str;
	}

	public boolean syncLotStatus(String paramString1, String paramString2, int paramInt) throws Exception {
		Date date = new Date();
		while (date.getTime() - (new Date()).getTime() > paramInt) {
			if (paramString2.equalsIgnoreCase(getLotStatus(paramString1)))
				return true;
			Thread.sleep(3000L);
		}
		log.warn("timeout occurred waiting for lot status change.  Lot: " + paramString1 + " Expected Status: "
				+ paramString2 + " Timeout in Milli Sec: " + paramInt);
		return false;
	}

	public pptLotHoldListInqResult_struct lotHoldList(String paramString) {
		return siViewConn.TxLotHoldListInq(user, id(paramString));
	}

	private pptFutureHoldListAttributes_struct[] getFutureLotHoldList(String paramString) {
		pptLotInfoInqResult_struct pptLotInfoInqResult_struct = getLotInfo(paramString);
		pptFutureHoldListByKeyInqResult_struct pptFutureHoldListByKeyInqResult_struct = siViewConn
				.TxFutureHoldListByKeyInq(user, new pptFutureHoldSearchKey_struct(id(paramString), "", id(""), id(""),
						id(""), "", id(""), "", "", pptLotInfoInqResult_struct.siInfo), 100);
		return pptFutureHoldListByKeyInqResult_struct.strFutureHoldListAttributes;
	}

	private pptEntityInhibitAttributes_struct getInhibitEquipment(String paramString) {
		pptEqpInfoInqResult_struct pptEqpInfoInqResult_struct = siViewConn.TxEqpInfoInq(user, id(paramString), true,
				true, true, true, true, true, true, true);
		pptEntityInhibitAttributes_struct[] arrayOfPptEntityInhibitAttributes_struct = pptEqpInfoInqResult_struct.entityInhibitions;
		log.debug("Getting inhibit equipement " + arrayOfPptEntityInhibitAttributes_struct[0]);
		return arrayOfPptEntityInhibitAttributes_struct[0];
	}

	public boolean isEqpChamberExists(String paramString1, String paramString2) {
		log.info("Verifyt wether chamber '" + paramString2 + "' exists in the equipment '" + paramString1 + "'");
		pptCandidateChamberStatusInqResult_struct pptCandidateChamberStatusInqResult_struct = siViewConn
				.TxCandidateChamberStatusInq(user, id(paramString1), "test");
		int i = pptCandidateChamberStatusInqResult_struct.strCandidateChamberStatusInfo.length;
		for (byte b = 0; b < i; b++) {
			if ((pptCandidateChamberStatusInqResult_struct.strCandidateChamberStatusInfo[b]).chamberID.identifier
					.toString().equalsIgnoreCase(paramString2)) {
				log.info("Chamber '" + paramString2 + "' exists in the equipment '" + paramString1 + "'");
				return true;
			}
		}
		log.warn("Chamber '" + paramString2 + "' does not exists in the equipment '" + paramString1 + "'");
		return false;
	}

	private pptEqpChamberStatusInfo_struct getEquipmentChamberDetails(String paramString1, String paramString2) {
		pptEqpInfoInqResult_struct pptEqpInfoInqResult_struct = siViewConn.TxEqpInfoInq(user, id(paramString1), true,
				true, true, true, true, true, true, true);
		pptEqpChamberInfo_struct pptEqpChamberInfo_struct = pptEqpInfoInqResult_struct.equipmentChamberInfo;
		pptEqpChamberStatusInfo_struct[] arrayOfPptEqpChamberStatusInfo_struct = pptEqpChamberInfo_struct.strEqpChamberStatus;
		for (pptEqpChamberStatusInfo_struct pptEqpChamberStatusInfo_struct : arrayOfPptEqpChamberStatusInfo_struct) {
			if (pptEqpChamberStatusInfo_struct.chamberID.identifier.equalsIgnoreCase(paramString2)) {
				log.debug("Chamber status is:" + pptEqpChamberStatusInfo_struct);
				return pptEqpChamberStatusInfo_struct;
			}
		}
		log.warn("Chamber status is:" + null);
		return null;
	}

	public String getEquipmentChamberAttributes(String paramString1, String paramString2, String paramString3) {
		pptEqpChamberStatusInfo_struct pptEqpChamberStatusInfo_struct = getEquipmentChamberDetails(paramString1,
				paramString2);
		String str = "None";
		if (pptEqpChamberStatusInfo_struct != null) {
			SIVIEW sIVIEW;
			try {
				sIVIEW = SIVIEW.valueOf(paramString3.replaceAll(" ", "").toUpperCase().trim());
			} catch (Exception exception) {
				log.warn("Not a valid SIVIEW constant " + SIVIEW.DEFAULT.toString());
				sIVIEW = SIVIEW.DEFAULT;
			}
			switch (sIVIEW) {
			case CHAMBERID:
				return pptEqpChamberStatusInfo_struct.chamberID.identifier;
			case STATUS:
				return pptEqpChamberStatusInfo_struct.actualE10Status.identifier;
			case SUBSTATUS:
				return pptEqpChamberStatusInfo_struct.chamberStatusCode.identifier;
			case STATUSDESCRIPTION:
				return pptEqpChamberStatusInfo_struct.chamberStatusDescription;
			case INHIBIT:
				if (pptEqpChamberStatusInfo_struct.chamberAvailableFlag) {
					str = "no";
				} else {
					str = "yes";
				}
				return str;
			case STATUSNAME:
				return pptEqpChamberStatusInfo_struct.chamberStatusName;
			}
			str = null;
		} else {
			log.error("The required " + paramString1 + "/" + paramString2 + " is not available");
		}
		return str;
	}

	private pptEqpInfoInqResult_struct getEquipmentInfo(String paramString) {
		log.debug("Getting equipment infomation " + paramString);
		return siViewConn.TxEqpInfoInq(user, id(paramString), true, true, true, true, true, true, true, true);
	}

	public String getEquipmentGeneralDetails(String paramString1, String paramString2) {
		pptEqpInfoInqResult_struct pptEqpInfoInqResult_struct = getEquipmentInfo(paramString1);
		pptEntityInhibitAttributes_struct[] arrayOfPptEntityInhibitAttributes_struct = pptEqpInfoInqResult_struct.entityInhibitions;
		pptEqpStatusInfo_struct pptEqpStatusInfo_struct = pptEqpInfoInqResult_struct.equipmentStatusInfo;
		pptEqpBrInfo_struct pptEqpBrInfo_struct = pptEqpInfoInqResult_struct.equipmentBRInfo;
		String str = "None";
		if (paramString2 != null) {
			SIVIEW sIVIEW;
			try {
				sIVIEW = SIVIEW.valueOf(paramString2.replaceAll(" ", "").toUpperCase().trim());
			} catch (Exception exception) {
				log.warn("Not a valid SIVIEW constant " + SIVIEW.DEFAULT.toString());
				sIVIEW = SIVIEW.DEFAULT;
			}
			switch (sIVIEW) {
			case STATUS:
				return pptEqpStatusInfo_struct.E10Status + " " + pptEqpStatusInfo_struct.equipmentStatusCode.identifier
						+ " " + (pptEqpStatusInfo_struct.equipmentAvailableFlag ? "Available" : "No");
			case DESCRIPTION:
				return pptEqpStatusInfo_struct.equipmentStatusDescription;
			case LASTRECIPE:
				return pptEqpStatusInfo_struct.lastRecipeID.identifier;
			case LASTCHANGETIME:
				return pptEqpStatusInfo_struct.changeTimeStamp;
			case INHIBIT:
				if (arrayOfPptEntityInhibitAttributes_struct.length > 0) {
					if (((arrayOfPptEntityInhibitAttributes_struct[0]).entities[0]).objectID.identifier
							.equalsIgnoreCase(paramString1)) {
						str = "Yes";
					} else {
						str = "No";
					}
				} else {
					str = "No";
				}
				return str;
			case RESERVEDFLOWBATCH:
				return pptEqpStatusInfo_struct.reservedFlowBatchID.identifier;
			case RESERVEDCONTROLJOB:
				return (pptEqpStatusInfo_struct.reservedControlJobID[0]).identifier;
			case WORKAREA:
				return pptEqpBrInfo_struct.workArea.identifier;
			case EQPOWNER:
				return pptEqpBrInfo_struct.equipmentOwner;
			case TCSRESOURCE:
				return pptEqpBrInfo_struct.TCSResourceName;
			case EQPCATEGORY:
				return pptEqpBrInfo_struct.equipmentCategory;
			case MONITORBANK:
				return pptEqpBrInfo_struct.monitorBank.identifier;
			case DUMMYBANK:
				return pptEqpBrInfo_struct.dummyBank.identifier;
			case SPECIALCONTROL:
				return pptEqpBrInfo_struct.specialControl[0];
			case RETICLEUSE:
				return pptEqpBrInfo_struct.reticleUseFlag ? "Yes" : "No";
			case EQPTOEQP:
				return pptEqpBrInfo_struct.eqpToEqpTransferFlag ? "Yes" : "No";
			case CARRIERCHANGE:
				return pptEqpBrInfo_struct.cassetteChangeFlag ? "Yes" : "No";
			case MONITORCREATION:
				return pptEqpBrInfo_struct.monitorCreationFlag ? "Yes" : "No";
			case TAKEINOUT:
				return pptEqpBrInfo_struct.takeInOutTransferFlag ? "Yes" : "No";
			}
			str = null;
		} else {
			log.error("The required " + paramString2 + " is not available in the equipment: " + paramString1);
		}
		return str;
	}

	public boolean lotHold(String paramString1, String paramString2) {
		log.info("Put LOT-" + paramString1 + " on hold with reason code: " + paramString2);
		paramString1 = paramString1.trim();
		pptLotInfoInqResult_struct pptLotInfoInqResult_struct = getLotInfo(paramString1);
		pptHoldReq_struct[] arrayOfPptHoldReq_struct = {
				new pptHoldReq_struct("LotHold", id(paramString2), new objectIdentifier_struct("X-SIL", ""),
						(pptLotInfoInqResult_struct.strLotInfo[0]).strLotOperationInfo.operationName,
						(pptLotInfoInqResult_struct.strLotInfo[0]).strLotOperationInfo.routeID,
						(pptLotInfoInqResult_struct.strLotInfo[0]).strLotOperationInfo.operationNumber,
						id(paramString1), "MTQA Automation", pptLotInfoInqResult_struct.siInfo) };
		pptBaseResult_struct pptBaseResult_struct = siViewConn.TxHoldLotReq(user, id(paramString1),
				arrayOfPptHoldReq_struct);
		if (pptBaseResult_struct.strResult.reasonText == ""
				|| pptBaseResult_struct.strResult.reasonText.length() == 0) {
			log.info("LOT-" + paramString1 + " is put on hold with reason code: " + paramString2);
			return true;
		}
		log.warn("LOT-" + paramString1 + "; unable to put this lot on hold. Reason code: "
				+ pptBaseResult_struct.strResult.reasonText);
		return false;
	}

	public boolean releaseLotHold(String paramString1, String paramString2) {
		pptLotHoldListInqResult_struct pptLotHoldListInqResult_struct = lotHoldList(paramString1);
		int i = pptLotHoldListInqResult_struct.strLotHoldListAttributes.length;
		pptHoldReq_struct[] arrayOfPptHoldReq_struct = new pptHoldReq_struct[i];
		for (byte b = 0; b < i; b++) {
			arrayOfPptHoldReq_struct[b] = new pptHoldReq_struct();
			(arrayOfPptHoldReq_struct[b]).holdType = (pptLotHoldListInqResult_struct.strLotHoldListAttributes[b]).holdType;
			(arrayOfPptHoldReq_struct[b]).holdReasonCodeID = (pptLotHoldListInqResult_struct.strLotHoldListAttributes[b]).reasonCodeID;
			(arrayOfPptHoldReq_struct[b]).holdUserID = (pptLotHoldListInqResult_struct.strLotHoldListAttributes[b]).userID;
			(arrayOfPptHoldReq_struct[b]).responsibleOperationMark = (pptLotHoldListInqResult_struct.strLotHoldListAttributes[b]).responsibleOperationMark;
			(arrayOfPptHoldReq_struct[b]).routeID = (pptLotHoldListInqResult_struct.strLotHoldListAttributes[b]).responsibleRouteID;
			(arrayOfPptHoldReq_struct[b]).operationNumber = (pptLotHoldListInqResult_struct.strLotHoldListAttributes[b]).responsibleOperationNumber;
			(arrayOfPptHoldReq_struct[b]).relatedLotID = (pptLotHoldListInqResult_struct.strLotHoldListAttributes[b]).relatedLotID;
			(arrayOfPptHoldReq_struct[b]).claimMemo = (pptLotHoldListInqResult_struct.strLotHoldListAttributes[b]).claimMemo;
			(arrayOfPptHoldReq_struct[b]).siInfo = (pptLotHoldListInqResult_struct.strLotHoldListAttributes[b]).siInfo;
		}
		pptBaseResult_struct pptBaseResult_struct = siViewConn.TxHoldLotReleaseReq(user, id(paramString1),
				id(paramString2), arrayOfPptHoldReq_struct);
		if (pptBaseResult_struct.strResult.returnCode.equalsIgnoreCase("0")) {
			log.info(" Successfully released LOT HOLD for LOT-" + paramString1 + " ;" + paramString2);
			TestReport.GetInstance().log(Status.PASS,
					" Successfully released LOT HOLD for LOT-" + paramString1 + " ;" + paramString2);
			return true;
		}
		log.warn("LOT-" + paramString1 + "; unable to release lot hold. Reason code: "
				+ pptBaseResult_struct.strResult.reasonText);
		TestReport.GetInstance().log(Status.FAIL, "LOT-" + paramString1 + "; unable to release lot hold. Reason code: "
				+ pptBaseResult_struct.strResult.reasonText);

		return false;
	}

	public boolean releaseFutureLotHold(String LotID, String ReasonCode) {
		pptLotInfoInqResult_struct pptLotInfoInqResult_struct = getLotInfo(LotID);
		pptFutureHoldListByKeyInqResult_struct pptFutureHoldListByKeyInqResult_struct = siViewConn
				.TxFutureHoldListByKeyInq(user, new pptFutureHoldSearchKey_struct(id(LotID), "", id(""), id(""), id(""),
						"", id(""), "", "", pptLotInfoInqResult_struct.siInfo), 100);
		pptFutureHoldListAttributes_struct[] arrayOfPptFutureHoldListAttributes_struct = pptFutureHoldListByKeyInqResult_struct.strFutureHoldListAttributes;
		pptHoldReq_struct[] arrayOfPptHoldReq_struct = new pptHoldReq_struct[arrayOfPptFutureHoldListAttributes_struct.length];
		if (arrayOfPptFutureHoldListAttributes_struct.length != 0) {

			for (byte b = 0; b < arrayOfPptFutureHoldListAttributes_struct.length; b++)
				arrayOfPptHoldReq_struct[b] = new pptHoldReq_struct(
						(arrayOfPptFutureHoldListAttributes_struct[b]).holdType,
						(arrayOfPptFutureHoldListAttributes_struct[b]).reasonCodeID,
						(arrayOfPptFutureHoldListAttributes_struct[b]).userID,
						(arrayOfPptFutureHoldListAttributes_struct[b]).operationName,
						(arrayOfPptFutureHoldListAttributes_struct[b]).routeID,
						(arrayOfPptFutureHoldListAttributes_struct[b]).operationNumber,
						(arrayOfPptFutureHoldListAttributes_struct[b]).relatedLotID,
						(arrayOfPptFutureHoldListAttributes_struct[b]).claimMemo,
						(arrayOfPptFutureHoldListAttributes_struct[b]).siInfo);
			pptBaseResult_struct pptBaseResult_struct = siViewConn.TxFutureHoldCancelReq(user, id(LotID),
					id(ReasonCode), "cancel", arrayOfPptHoldReq_struct);
			log.info("Future lot hold reslease results is: " + pptBaseResult_struct.strResult.reasonText);
			TestReport.GetInstance().log(Status.PASS,
					"[ " + LotID + " ]" + "Future lot hold reslease is: " + pptBaseResult_struct.strResult.messageText);

		} else
			TestReport.GetInstance().log(Status.WARNING, "Future Lot hold is not present or invalid lotID: " + LotID);

		return getFutureLotStatus(LotID,false);

	}

	public boolean releaseFutureLotHold(String LotID) {
		return releaseFutureLotHold(LotID, "All");
	}

	public boolean getFutureLotStatus(String LotID, boolean Status_flag) {
		pptLotInfoInqResult_struct pptLotInfoInqResult_struct = getLotInfo(LotID);
		pptFutureHoldListByKeyInqResult_struct pptFutureHoldListByKeyInqResult_struct = siViewConn
				.TxFutureHoldListByKeyInq(user, new pptFutureHoldSearchKey_struct(id(LotID), "", id(""), id(""), id(""),
						"", id(""), "", "", pptLotInfoInqResult_struct.siInfo), 100);
		pptFutureHoldListAttributes_struct[] arrayOfPptFutureHoldListAttributes_struct = pptFutureHoldListByKeyInqResult_struct.strFutureHoldListAttributes;
		if (Status_flag && arrayOfPptFutureHoldListAttributes_struct.length != 0) {
			TestReport.GetInstance().log(Status.PASS, "[ " + LotID + " ]" + "Future Lot is on Hold");
			return true;
		} else if (Status_flag && arrayOfPptFutureHoldListAttributes_struct.length == 0) {
			TestReport.GetInstance().log(Status.FAIL, "[ " + LotID + " ]" + "Future Lot is not on Hold");
			return false;
		} else if (!Status_flag && arrayOfPptFutureHoldListAttributes_struct.length != 0) {
			TestReport.GetInstance().log(Status.FAIL, "[ " + LotID + " ]" + "Future Lot is on Hold");
			return false;
		} else
			TestReport.GetInstance().log(Status.PASS, "[ " + LotID + " ]" + "Future Lot is not on Hold");
		return true;
	}
	
	public boolean getFutureLotStatus(String LotID) {
		return getFutureLotStatus(LotID, true);
	}

	public boolean getFutureLotStatus(String LotID, String status_flag) {
		boolean flag = true;
		if (status_flag != null && !status_flag.isEmpty() && status_flag.trim().equalsIgnoreCase("false"))
			flag = false;
		return getFutureLotStatus(LotID, flag);
	}
	
	public boolean moveLotToNextStep(String paramString) {
		log.info("Move LOT-" + paramString + " to next step");
		paramString = paramString.trim();
		pptLotInfoInqResult_struct pptLotInfoInqResult_struct = getLotInfo(paramString);
		pptRouteOperationListInqResult_struct pptRouteOperationListInqResult_struct = siViewConn
				.TxRouteOperationListForLotInq(user, id(paramString));
		byte b = 0;
		for (b = 0; b < pptRouteOperationListInqResult_struct.strOperationNameAttributes.length
				&& !(pptLotInfoInqResult_struct.strLotInfo[0]).strLotOperationInfo.operationName.equalsIgnoreCase(
						(pptRouteOperationListInqResult_struct.strOperationNameAttributes[b]).operationName); b++)
			;
		if (b == pptRouteOperationListInqResult_struct.strOperationNameAttributes.length) {
			log.warn("Lot is at last operation step. Cannot move this to next step");
			return false;
		}
		if (b > pptRouteOperationListInqResult_struct.strOperationNameAttributes.length) {
			log.warn("current operation step could not be found. Cannot move this to next step");
			return false;
		}
		pptBaseResult_struct pptBaseResult_struct = siViewConn.TxOpeLocateReq(user, true, id(paramString),
				(pptLotInfoInqResult_struct.strLotInfo[0]).strLotOperationInfo.routeID,
				(pptLotInfoInqResult_struct.strLotInfo[0]).strLotOperationInfo.operationNumber,
				(pptRouteOperationListInqResult_struct.strOperationNameAttributes[++b]).routeID,
				(pptRouteOperationListInqResult_struct.strOperationNameAttributes[b]).operationID,
				(pptRouteOperationListInqResult_struct.strOperationNameAttributes[b]).operationNumber,
				(pptRouteOperationListInqResult_struct.strOperationNameAttributes[b]).processRef, 0, "Test");
		if (pptBaseResult_struct.strResult.reasonText == "") {
			log.info("LOT-" + paramString + " is moved to next operation step");
			return true;
		}
		log.warn("LOT-" + paramString + "; unable to move lot to next operation step. Reason code: "
				+ pptBaseResult_struct.strResult.reasonText);
		return false;
	}

	public boolean moveLotToPreviousStep(String paramString) {
		log.info("Move LOT-" + paramString + " to previous step");
		paramString = paramString.trim();
		pptLotInfoInqResult_struct pptLotInfoInqResult_struct = getLotInfo(paramString);
		pptRouteOperationListInqResult_struct pptRouteOperationListInqResult_struct = siViewConn
				.TxRouteOperationListForLotInq(user, id(paramString));
		byte b = 0;
		for (b = 0; b < pptRouteOperationListInqResult_struct.strOperationNameAttributes.length
				&& !(pptLotInfoInqResult_struct.strLotInfo[0]).strLotOperationInfo.operationName.equalsIgnoreCase(
						(pptRouteOperationListInqResult_struct.strOperationNameAttributes[b]).operationName); b++)
			;
		if (b == 0) {
			log.warn("Lot is at first operation step. Cannot move this to previous step");
			return false;
		}
		if (b > pptRouteOperationListInqResult_struct.strOperationNameAttributes.length) {
			log.warn("current operation step could not be found. Cannot move this to previous step");
			return false;
		}
		pptBaseResult_struct pptBaseResult_struct = siViewConn.TxOpeLocateReq(user, true, id(paramString),
				(pptLotInfoInqResult_struct.strLotInfo[0]).strLotOperationInfo.routeID,
				(pptLotInfoInqResult_struct.strLotInfo[0]).strLotOperationInfo.operationNumber,
				(pptRouteOperationListInqResult_struct.strOperationNameAttributes[--b]).routeID,
				(pptRouteOperationListInqResult_struct.strOperationNameAttributes[b]).operationID,
				(pptRouteOperationListInqResult_struct.strOperationNameAttributes[b]).operationNumber,
				(pptRouteOperationListInqResult_struct.strOperationNameAttributes[b]).processRef, 0, "Test");
		if (pptBaseResult_struct.strResult.reasonText == "") {
			log.info("LOT-" + paramString + " is moved to previous operation step");
			return true;
		}
		log.warn("LOT-" + paramString + "; unable to move lot to previous operation step. Reason code: "
				+ pptBaseResult_struct.strResult.reasonText);
		return false;
	}

	public boolean moveLotToOperationStep(String paramString1, String paramString2) {
		String str = getLotStatus(paramString1);
		releaseLotHold(paramString1, "ALL");
		releaseFutureLotHold(paramString1, "ALL");
		log.info("Move LOT-" + paramString1 + " to operation step: " + paramString2);
		paramString1 = paramString1.trim();
		pptLotInfoInqResult_struct pptLotInfoInqResult_struct = getLotInfo(paramString1);
		pptRouteOperationListInqResult_struct pptRouteOperationListInqResult_struct = siViewConn
				.TxRouteOperationListForLotInq(user, id(paramString1));
		byte b = 0;
		for (b = 0; b < pptRouteOperationListInqResult_struct.strOperationNameAttributes.length
				&& !paramString2.equalsIgnoreCase(
						(pptRouteOperationListInqResult_struct.strOperationNameAttributes[b]).operationName); b++)
			;
		pptBaseResult_struct pptBaseResult_struct = siViewConn.TxOpeLocateReq(user, true, id(paramString1),
				(pptLotInfoInqResult_struct.strLotInfo[0]).strLotOperationInfo.routeID,
				(pptLotInfoInqResult_struct.strLotInfo[0]).strLotOperationInfo.operationNumber,
				(pptRouteOperationListInqResult_struct.strOperationNameAttributes[b]).routeID,
				(pptRouteOperationListInqResult_struct.strOperationNameAttributes[b]).operationID,
				(pptRouteOperationListInqResult_struct.strOperationNameAttributes[b]).operationNumber,
				(pptRouteOperationListInqResult_struct.strOperationNameAttributes[b]).processRef, 0, "Test");
		if (pptBaseResult_struct.strResult.messageText.contains("Normal end")) {
			log.info("LOT-" + paramString1 + " is moved to operation step: " + paramString2);
			str = getLotStatus(paramString1);
			if (str.equalsIgnoreCase("LotHold"))
				releaseLotHold(paramString1, "ALL");
			return true;
		}
		log.warn("LOT-" + paramString1 + "; unable to move lot to next operation step. Reason code: "
				+ pptBaseResult_struct.strResult.reasonText + " and message text is: "
				+ pptBaseResult_struct.strResult.messageText);
		return false;
	}

	public String getScriptParameter(String ParameterClassName, String ParameterIdentifier, String ParameterName) {
		pptUserParameterValueInqResult_struct pptUserParameterValueInqResult_struct = siViewConn
				.TxUserParameterValueInq(user, ParameterClassName.trim(), ParameterIdentifier.trim());
		if (pptUserParameterValueInqResult_struct.parameters.length > 1) {
			pptUserParameterValue_struct[] arrayOfPptUserParameterValue_struct = pptUserParameterValueInqResult_struct.parameters;
			for (pptUserParameterValue_struct pptUserParameterValue_struct : arrayOfPptUserParameterValue_struct) {
				if (pptUserParameterValue_struct.parameterName.equalsIgnoreCase(ParameterName.trim()))
				{
					TestReport.GetInstance().log(Status.PASS, ParameterName+  " is found in script parameter list "+ ParameterClassName+ ":- "+ ParameterIdentifier);
					log.info(ParameterName+ " is found in script parameter list "+ ParameterClassName+ ":- "+ ParameterIdentifier);
					return pptUserParameterValue_struct.value.isEmpty() ? "EMPTY" : pptUserParameterValue_struct.value;
			
				}
			}
			TestReport.GetInstance().log(Status.FAIL, ParameterName+ " is not found in script parameter list "+ ParameterClassName+ ":- "+ ParameterIdentifier);
			log.debug(ParameterName+ " is not found in script parameter list "+ ParameterClassName+ ":- "+ ParameterIdentifier);
			}
		log.warn("No script parameters are available....");
		
		return "FAILED: No script parameters are available";
	}

	public boolean VerifyScriptParameterValue(String ParameterClassName, String ParameterIdentifier, String ParameterName,String value)
	{
		if(value!=null&&!value.isEmpty()&&!value.equalsIgnoreCase("EMPTY"))
		{
			String str= getScriptParameter(ParameterClassName,ParameterIdentifier,ParameterName);
			if(str.equalsIgnoreCase(value))
			{
				TestReport.GetInstance().log(Status.PASS, ParameterName+ " expected value is [ "+ value + " ]+  actaul value is [ "+ str+" ] ");
				return true;
			}
			else
			{
				TestReport.GetInstance().log(Status.FAIL, ParameterName+ " expected value is [ "+ value + " ]+  actaul value is [ "+ str+" ] ");
				return false;
			}
			
		}
		String str=getScriptParameter(ParameterClassName,ParameterIdentifier,ParameterName);
		if(str.contains("FAILED:"))
		{
			TestReport.GetInstance().log(Status.FAIL, ParameterName+ "is not found due to:- " +str);
			return false;
		}
		TestReport.GetInstance().log(Status.PASS, ParameterName+ " is verified and Parameter value is " +str);
		return true;
	}
	
	
	public boolean removeScriptParameter(String ParameterClassName, String ParameterIdentifier, String ParameterName) {
		pptUserParameterValueInqResult_struct pptUserParameterValueInqResult_struct = siViewConn
				.TxUserParameterValueInq(user, ParameterClassName.trim(), ParameterIdentifier.trim());
		if (pptUserParameterValueInqResult_struct.parameters.length > 1) {
			pptUserParameterValue_struct[] arrayOfPptUserParameterValue_struct = pptUserParameterValueInqResult_struct.parameters;
			for (pptUserParameterValue_struct pptUserParameterValue_struct : arrayOfPptUserParameterValue_struct) {
				if (pptUserParameterValue_struct.parameterName.equalsIgnoreCase(ParameterName.trim()))
					pptUserParameterValue_struct.value = "";
			}
		}
		log.warn("Script parameter is removed...");
		return true;
	}
	

	public boolean changeChamberStatus(String paramString1, String paramString2, String paramString3) {
		pptEqpChamberStatus_struct[] arrayOfPptEqpChamberStatus_struct = {
				new pptEqpChamberStatus_struct(id(paramString2.trim()), id(paramString3.trim()), getSiInfo()) };
		pptChamberStatusChangeReqResult_struct pptChamberStatusChangeReqResult_struct = siViewConn
				.TxChamberStatusChangeReq(user, id(paramString1.trim()), arrayOfPptEqpChamberStatus_struct, "");
		return pptChamberStatusChangeReqResult_struct.strResult.returnCode.toString().equalsIgnoreCase("0");
	}

	public boolean changeEquipmentMode(String paramString1, String paramString2) {
		log.debug("Change Equipment mode for " + paramString1);
		pptEqpInfoInqResult_struct pptEqpInfoInqResult_struct = getEquipmentInfo(paramString1);
		pptEqpPortInfo_struct pptEqpPortInfo_struct = pptEqpInfoInqResult_struct.equipmentPortInfo;
		pptEqpPortStatus_struct[] arrayOfPptEqpPortStatus_struct = pptEqpPortInfo_struct.strEqpPortStatus;
		pptOperationMode_struct pptOperationMode_struct = new pptOperationMode_struct(id(paramString2), "", "", "", "",
				"", "automation", pptEqpInfoInqResult_struct.siInfo);
		pptPortOperationMode_struct[] arrayOfPptPortOperationMode_struct = new pptPortOperationMode_struct[arrayOfPptEqpPortStatus_struct.length];
		for (byte b = 0; b < arrayOfPptEqpPortStatus_struct.length; b++) {
			pptEqpPortStatus_struct pptEqpPortStatus_struct = arrayOfPptEqpPortStatus_struct[b];
			pptPortOperationMode_struct pptPortOperationMode_struct = new pptPortOperationMode_struct();
			pptPortOperationMode_struct.portGroup = pptEqpPortStatus_struct.portGroup;
			pptPortOperationMode_struct.portID = pptEqpPortStatus_struct.portID;
			pptPortOperationMode_struct.portUsage = pptEqpPortStatus_struct.portUsage;
			pptPortOperationMode_struct.strOperationMode = pptOperationMode_struct;
			arrayOfPptPortOperationMode_struct[b] = pptPortOperationMode_struct;
			pptPortOperationMode_struct.siInfo = pptEqpPortStatus_struct.siInfo;
		}
		pptBaseResult_struct pptBaseResult_struct = siViewConn.TxEqpModeChangeReq(user, id(paramString1),
				arrayOfPptPortOperationMode_struct, false, false, "");
		if (pptBaseResult_struct.strResult.messageText.contains("Normal end.")) {
			log.debug("Mode Successfully changed");
			return true;
		}
		log.debug("Mode not changed");
		return false;
	}

	public String getEquipmentPorts(String paramString) {
		String str = "";
		pptEqpInfoInqResult_struct pptEqpInfoInqResult_struct = getEquipmentInfo(paramString);
		pptEqpPortInfo_struct pptEqpPortInfo_struct = pptEqpInfoInqResult_struct.equipmentPortInfo;
		pptEqpPortStatus_struct[] arrayOfPptEqpPortStatus_struct = pptEqpPortInfo_struct.strEqpPortStatus;
		for (pptEqpPortStatus_struct pptEqpPortStatus_struct : arrayOfPptEqpPortStatus_struct)
			str = str + "," + pptEqpPortStatus_struct.portID.identifier;
		log.debug("Getting equipment ports" + paramString);
		return str.replaceFirst(",", "");
	}

	public boolean verifyLotRequeueStatus(String paramString1, String paramString2, String paramString3) {

		try {
			pptLotOperationListFromHistoryInqResult_struct pptLotOperationListFromHistoryInqResult_struct = siViewConn
					.TxLotOperationListFromHistoryInq(user, 300, id(paramString1));
			pptOperationHistoryInqResult_struct pptOperationHistoryInqResult_struct = siViewConn.TxOperationHistoryInq(
					user, id(paramString1),
					(pptLotOperationListFromHistoryInqResult_struct.strOperationNameAttributes[0]).routeID,
					(pptLotOperationListFromHistoryInqResult_struct.strOperationNameAttributes[0]).operationID,
					(pptLotOperationListFromHistoryInqResult_struct.strOperationNameAttributes[0]).operationNumber
							.toString(),
					(pptLotOperationListFromHistoryInqResult_struct.strOperationNameAttributes[0]).operationPass
							.toString(),
					(pptLotOperationListFromHistoryInqResult_struct.strOperationNameAttributes[0]).operationNumber
							.toString(),
					true);
			for (pptOperationHisInfo_struct pptOperationHisInfo_struct : pptOperationHistoryInqResult_struct.strOperationHisInfo) {
				if (pptOperationHisInfo_struct.operationCategory.equalsIgnoreCase("requeue")
						&& verifyDateTime(paramString2, pptOperationHisInfo_struct.reportTimeStamp,
								"yyyy-MM-dd-HH.mm.ss")
						&& pptOperationHisInfo_struct.userID.compareToIgnoreCase(paramString3) == 0) {
					log.info(pptOperationHisInfo_struct.operationCategory + " :"
							+ pptOperationHisInfo_struct.reportTimeStamp + " :" + pptOperationHisInfo_struct.routeID
							+ " :" + pptOperationHisInfo_struct.operationID + " :"
							+ pptOperationHisInfo_struct.operationNumber + " :"
							+ pptOperationHisInfo_struct.operationName + " :" + pptOperationHisInfo_struct.userID + " :"
							+ pptOperationHisInfo_struct.cassetteID);
					return true;
				}
			}
		} catch (Exception exception) {
			log.error("Exception occured while executing verifyLotRequeueStatus: " + exception.getMessage());
		}
		return false;
	}

	public boolean addLotScriptParam(String paramString1, String paramString2, String paramString3, String paramString4)
			throws Exception {
		pptUserParameterValue_struct[] arrayOfPptUserParameterValue_struct = new pptUserParameterValue_struct[1];
		pptUserParameterValue_struct pptUserParameterValue_struct = new pptUserParameterValue_struct();
		pptUserParameterValue_struct.changeType = 1;
		pptUserParameterValue_struct.parameterName = paramString2;
		pptUserParameterValue_struct.dataType = paramString3;
		pptUserParameterValue_struct.keyValue = "";
		pptUserParameterValue_struct.value = paramString4;
		pptUserParameterValue_struct.valueFlag = false;
		pptUserParameterValue_struct.description = "";
		pptUserParameterValue_struct.siInfo = getSiInfo();
		arrayOfPptUserParameterValue_struct[0] = pptUserParameterValue_struct;
		pptBaseResult_struct pptBaseResult_struct = siViewConn.TxUserParameterValueChangeReq(user, "Lot", paramString1,
				arrayOfPptUserParameterValue_struct);
		if (pptBaseResult_struct.strResult.returnCode.equalsIgnoreCase("0")) {
			log.debug("Adding Lot Script Param done");
			return true;
		}
		log.debug("Not able to add script param");
		return false;
	}

	public boolean verifyProcessHoldStatus(String paramString1, String paramString2) {
		pptProcessHoldSearchKey_struct pptProcessHoldSearchKey_struct = new pptProcessHoldSearchKey_struct();
		pptProcessHoldSearchKey_struct.routeID = id(paramString1);
		pptProcessHoldSearchKey_struct.userID = id("");
		pptProcessHoldSearchKey_struct.operationNumber = "";
		pptProcessHoldSearchKey_struct.holdType = "";
		pptProcessHoldSearchKey_struct.productID = id("");
		pptProcessHoldSearchKey_struct.siInfo = getSiInfo();
		pptProcessHoldListInqResult_struct pptProcessHoldListInqResult_struct = siViewConn.TxProcessHoldListInq(user,
				pptProcessHoldSearchKey_struct, 10);
		for (pptProcHoldListAttributes_struct pptProcHoldListAttributes_struct : pptProcessHoldListInqResult_struct.strProcHoldListAttributes) {
			if (pptProcHoldListAttributes_struct.holdType.equalsIgnoreCase("ProcessHold")
					&& pptProcHoldListAttributes_struct.withExecHoldFlag == true
					&& pptProcHoldListAttributes_struct.reasonCodeID.identifier.equals(paramString2)) {
				log.debug("Process is in hold state");
				return true;
			}
		}
		log.debug("Process not found");
		return false;
	}

	public boolean setRouteHold(String paramString1, String paramString2, String paramString3) {
		pptProcessHoldReqResult_struct pptProcessHoldReqResult_struct = siViewConn.TxProcessHoldReq(user, "ProcessHold",
				id(paramString1), paramString3, id(""), id(paramString2), true, "Hold Process");
		if (pptProcessHoldReqResult_struct.strResult.returnCode.equalsIgnoreCase("0")) {
			log.debug(paramString1 + " is hold");
			return true;
		}
		log.debug(paramString1 + " is not hold");
		return false;
	}

	public boolean getRouteHoldRelease(String paramString1, String paramString2) {
		boolean bool = false;
		pptProcessHoldSearchKey_struct pptProcessHoldSearchKey_struct = new pptProcessHoldSearchKey_struct();
		pptProcessHoldSearchKey_struct.routeID = id(paramString1);
		pptProcessHoldSearchKey_struct.userID = id("");
		pptProcessHoldSearchKey_struct.operationNumber = "";
		pptProcessHoldSearchKey_struct.holdType = "";
		pptProcessHoldSearchKey_struct.productID = id("");
		pptProcessHoldSearchKey_struct.siInfo = getSiInfo();
		pptProcessHoldListInqResult_struct pptProcessHoldListInqResult_struct = siViewConn.TxProcessHoldListInq(user,
				pptProcessHoldSearchKey_struct, 10);
		for (pptProcHoldListAttributes_struct pptProcHoldListAttributes_struct : pptProcessHoldListInqResult_struct.strProcHoldListAttributes) {
			if (paramString2.equalsIgnoreCase("All"))
				paramString2 = pptProcHoldListAttributes_struct.reasonCodeID.identifier;
			if (pptProcHoldListAttributes_struct.holdType.equalsIgnoreCase("ProcessHold")
					&& pptProcHoldListAttributes_struct.withExecHoldFlag == true
					&& pptProcHoldListAttributes_struct.reasonCodeID.identifier.equals(paramString2)) {
				pptProcessHoldCancelReqResult_struct pptProcessHoldCancelReqResult_struct = siViewConn
						.TxProcessHoldCancelReq(user, pptProcHoldListAttributes_struct.holdType,
								pptProcHoldListAttributes_struct.routeID,
								pptProcHoldListAttributes_struct.operationNumber,
								pptProcHoldListAttributes_struct.productID,
								pptProcHoldListAttributes_struct.reasonCodeID, id("X-FG"), false, "Releasing Process");
				log.debug(paramString1 + " is hold released");
				bool = true;
			}
		}
		log.debug(paramString1 + " is not found");
		return bool;
	}

	public String getRouteHoldDetails(String paramString1, String paramString2, String paramString3) {
		log.info("Executing getRouteHoldDetails() with parameters:routeId:" + paramString1 + ",servTime:" + paramString2
				+ ",requiredParam:" + paramString3);
		String str = "";

		pptProcessHoldSearchKey_struct pptProcessHoldSearchKey_struct = new pptProcessHoldSearchKey_struct();
		pptProcessHoldSearchKey_struct.routeID = id(paramString1);
		pptProcessHoldSearchKey_struct.userID = id("");
		pptProcessHoldSearchKey_struct.operationNumber = "";
		pptProcessHoldSearchKey_struct.holdType = "";
		pptProcessHoldSearchKey_struct.productID = id("");
		pptProcessHoldSearchKey_struct.siInfo = getSiInfo();
		pptProcessHoldListInqResult_struct pptProcessHoldListInqResult_struct = siViewConn.TxProcessHoldListInq(user,
				pptProcessHoldSearchKey_struct, 10);
		log.info("Process holds exist. And the number of process holds are:"
				+ pptProcessHoldListInqResult_struct.strProcHoldListAttributes.length);
		for (pptProcHoldListAttributes_struct pptProcHoldListAttributes_struct : pptProcessHoldListInqResult_struct.strProcHoldListAttributes) {
			if (pptProcHoldListAttributes_struct.holdType.equalsIgnoreCase("ProcessHold") && verifyDateTime(
					paramString2, pptProcHoldListAttributes_struct.reportTimeStamp, "yyyy-MM-dd-HH.mm.ss")) {
				SIVIEW sIVIEW;
				log.info("Verify time successed");
				try {
					sIVIEW = SIVIEW.valueOf(paramString3.replaceAll(" ", "").toUpperCase().trim());
				} catch (Exception exception) {
					sIVIEW = SIVIEW.DEFAULT;
				}
				switch (sIVIEW) {
				case ROUTEID:
					str = pptProcHoldListAttributes_struct.routeID.identifier;
					break;
				case OPERNO:
					str = pptProcHoldListAttributes_struct.operationNumber;
					break;
				case TIMESTAMP:
					str = pptProcHoldListAttributes_struct.reportTimeStamp;
					break;
				case PRODUCTID:
					str = pptProcHoldListAttributes_struct.productID.identifier;
					break;
				case REASONCD:
					str = pptProcHoldListAttributes_struct.reasonCodeID.identifier;
					break;
				case USERID:
					str = pptProcHoldListAttributes_struct.userID.identifier;
					break;
				case HOLDTP:
					str = pptProcHoldListAttributes_struct.holdType;
					break;
				case EXECHOLD:
					str = pptProcHoldListAttributes_struct.withExecHoldFlag ? "Hold" : "Not Hold";
					break;
				case CLAIMMEMO:
					str = pptProcHoldListAttributes_struct.claimMemo;
					break;
				default:
					str = "EMPTY";
					break;
				}
			}
		}
		log.debug(paramString1 + paramString3 + " is :" + str);
		return str;
	}

	public boolean setEqupStatus(String paramString1, String paramString2) {
		pptEqpStatusChangeRptResult_struct pptEqpStatusChangeRptResult_struct = null;
		pptEqpStatusChangeRptResult_struct = siViewConn.TxEqpStatusChangeRpt(user, id(paramString1), id(paramString2),
				"Status Changed");
		if (pptEqpStatusChangeRptResult_struct.strResult.returnCode.equalsIgnoreCase("0")) {
			pptCandidateEqpStatusInqResult_struct pptCandidateEqpStatusInqResult_struct = siViewConn
					.TxCandidateEqpStatusInq(user, id(paramString1), true);
			if (pptCandidateEqpStatusInqResult_struct.currentStatusCode.identifier.equalsIgnoreCase(paramString2)) {
				log.debug(paramString1 + " status changed successfully " + paramString2);
				return true;
			}
		}
		log.debug(paramString1 + " status not changed to " + paramString2);
		return false;
	}

	public boolean verifyLotReserved(String paramString1, String paramString2) {
		objectIdentifier_struct[] arrayOfObjectIdentifier_struct = { id(paramString1) };
		pptLotInfoInqResult_struct pptLotInfoInqResult_struct = siViewConn.TxLotInfoInq(user,
				arrayOfObjectIdentifier_struct, true, true, true, true, true, true, true, true, true, true, true, true,
				true, true, false);
		if (pptLotInfoInqResult_struct.strResult.returnCode == "") {
			log.debug("successfully retrieved lot information for LOT: " + paramString1);
		} else {
			log.error("unable to retrieve lot information for LOT: " + paramString1 + " Reason code: "
					+ pptLotInfoInqResult_struct.strResult.returnCode);
		}
		pptStartCassette_struct[] arrayOfPptStartCassette_struct = new pptStartCassette_struct[1];
		arrayOfPptStartCassette_struct[0] = new pptStartCassette_struct();
		pptLotInCassette_struct pptLotInCassette_struct = new pptLotInCassette_struct();
		(arrayOfPptStartCassette_struct[0]).strLotInCassette = new pptLotInCassette_struct[1];
		(arrayOfPptStartCassette_struct[0]).strLotInCassette[0] = pptLotInCassette_struct;
		((arrayOfPptStartCassette_struct[0]).strLotInCassette[0]).lotID = (pptLotInfoInqResult_struct.strLotInfo[0]).strLotBasicInfo.lotID;
		((arrayOfPptStartCassette_struct[0]).strLotInCassette[0]).lotType = (pptLotInfoInqResult_struct.strLotInfo[0]).strLotBasicInfo.lotType;
		((arrayOfPptStartCassette_struct[0]).strLotInCassette[0]).subLotType = (pptLotInfoInqResult_struct.strLotInfo[0]).strLotBasicInfo.subLotType;
		pptStartRecipe_struct pptStartRecipe_struct = new pptStartRecipe_struct();
		((arrayOfPptStartCassette_struct[0]).strLotInCassette[0]).strStartRecipe = pptStartRecipe_struct;
		((arrayOfPptStartCassette_struct[0]).strLotInCassette[0]).strStartRecipe.logicalRecipeID = (pptLotInfoInqResult_struct.strLotInfo[0]).strLotRecipeInfo.logicalRecipeID;
		((arrayOfPptStartCassette_struct[0]).strLotInCassette[0]).strStartRecipe.strStartReticle = new com.amd.jcs.siview.code.pptStartReticle_struct[1];
		((arrayOfPptStartCassette_struct[0]).strLotInCassette[0]).strStartRecipe.strStartFixture = new com.amd.jcs.siview.code.pptStartFixture_struct[1];
		((arrayOfPptStartCassette_struct[0]).strLotInCassette[0]).strStartRecipe.strDCDef = new com.amd.jcs.siview.code.pptDCDef_struct[1];
		int i = pptLotInfoInqResult_struct.strWaferMapInCassetteInfo.length;
		((arrayOfPptStartCassette_struct[0]).strLotInCassette[0]).strLotWafer = new com.amd.jcs.siview.code.pptLotWafer_struct[i];
		for (byte b = 0; b < i; b++) {
			(((arrayOfPptStartCassette_struct[0]).strLotInCassette[0]).strLotWafer[b]).waferID = (pptLotInfoInqResult_struct.strWaferMapInCassetteInfo[b]).waferID;
			(((arrayOfPptStartCassette_struct[0]).strLotInCassette[0]).strLotWafer[b]).slotNumber = (pptLotInfoInqResult_struct.strWaferMapInCassetteInfo[b]).slotNumber;
		}
		((arrayOfPptStartCassette_struct[0]).strLotInCassette[0]).productID = (pptLotInfoInqResult_struct.strLotInfo[0]).strLotProductInfo.productID;
		((arrayOfPptStartCassette_struct[0]).strLotInCassette[0]).strStartOperationInfo.routeID = (pptLotInfoInqResult_struct.strLotInfo[0]).strLotOperationInfo.routeID;
		((arrayOfPptStartCassette_struct[0]).strLotInCassette[0]).strStartOperationInfo.operationID = (pptLotInfoInqResult_struct.strLotInfo[0]).strLotOperationInfo.operationID;
		((arrayOfPptStartCassette_struct[0]).strLotInCassette[0]).strStartOperationInfo.operationNumber = (pptLotInfoInqResult_struct.strLotInfo[0]).strLotOperationInfo.operationNumber;
		objectIdentifier_struct objectIdentifier_struct = null;
		pptBaseResult_struct pptBaseResult_struct = siViewConn.TxArrivalCarrierNotificationForInternalBufferReq(user,
				id(paramString2), "", objectIdentifier_struct, arrayOfPptStartCassette_struct, "");
		return false;
	}

	public String getFixtureInhibitAttributes(String paramString1, String paramString2) {
		log.debug("Getting fixture inhibit attributes " + paramString1);
		return getInhibitAttributes("Fixture", paramString1, paramString2);
	}

	public String getFixtureGroupInhibitAttributes(String paramString1, String paramString2) {
		log.debug("Getting fixture group inhibit attributes " + paramString1);
		return getInhibitAttributes("Fixture Group", paramString1, paramString2);
	}

	public String getProductGroupInhibitAttributes(String paramString1, String paramString2) {
		log.debug("Getting Route Inhibit Attributes " + paramString1);
		return getInhibitAttributes("Product Group", paramString1, paramString2);
	}

	public String getNextOperationNumber(String paramString) throws Exception {
		log.info("Executing the \"getNextOperationNumber\" with the parameters:" + paramString);
		String str1 = null;
		String str2 = getOperationNumber(paramString);
		Map<String, String> map = getAllOperationIds(paramString);
		Set<String> set = map.keySet();
		Iterator<String> iterator = set.iterator();
		while (iterator.hasNext()) {
			String str = iterator.next();
			if (str.equalsIgnoreCase(str2)) {
				if (iterator.hasNext()) {
					str1 = iterator.next();
					log.info("The next operation number of the lot id:" + paramString + " is :" + str1);
					break;
				}
				throw new Exception(paramString + "does not have the next operation number");
			}
		}
		return str1;
	}

	public String getNextPD(String paramString) throws Exception {
		log.info("Executing the \"getNextOperationID\" with the parameters:" + paramString);
		Map<String, String> map = getAllOperationIds(paramString);
		return map.get(getNextOperationNumber(paramString));
	}

	public Map<String, String> getAllOperationIds(String paramString) {
		log.info("Executing \"getAllOperationIds\" with parameter: " + paramString);
		HashMap<String, String> hashMap = new HashMap<String, String>();
		pptLotOperationListInqResult_struct pptLotOperationListInqResult_struct = siViewConn.TxLotOperationListInq(user,
				true, true, 999, true, id(paramString));
		pptOperationNameAttributes_struct[] arrayOfPptOperationNameAttributes_struct = pptLotOperationListInqResult_struct.strOperationNameAttributes;
		for (pptOperationNameAttributes_struct pptOperationNameAttributes_struct : arrayOfPptOperationNameAttributes_struct) {
			String str1 = pptOperationNameAttributes_struct.operationNumber;
			String str2 = pptOperationNameAttributes_struct.operationID.identifier;
			log.info("Collecting the details of lot: " + paramString + ". Operation number is:" + str1
					+ ". Operation identifier is:" + str2);
			hashMap.put(str1, str2);
		}
		return (Map) hashMap;
	}

	public void verifySTBLot(String paramString) {
		pptLotOperationListInqResult_struct pptLotOperationListInqResult_struct = siViewConn.TxLotOperationListInq(user,
				true, true, 99, true, id(paramString));
		pptOperationNameAttributes_struct[] arrayOfPptOperationNameAttributes_struct = pptLotOperationListInqResult_struct.strOperationNameAttributes;
		for (pptOperationNameAttributes_struct pptOperationNameAttributes_struct : arrayOfPptOperationNameAttributes_struct)
			System.out.println(pptOperationNameAttributes_struct.operationID.identifier + "  "
					+ pptOperationNameAttributes_struct.operationNumber);
	}

	public String getExperimentLotInfo(String paramString) {
		pptExperimentalLotListInqResult_struct pptExperimentalLotListInqResult_struct = siViewConn
				.TxExperimentalLotListInq(user, id(paramString));
		siViewConn.TxLotOperationListInq(user, true, true, 99, true, id(paramString));
		pptExperimentalLot_struct[] arrayOfPptExperimentalLot_struct = pptExperimentalLotListInqResult_struct.strExperimentalLotList;
		for (pptExperimentalLot_struct pptExperimentalLot_struct : arrayOfPptExperimentalLot_struct)
			System.out.println(pptExperimentalLot_struct.splitOperationNumber);
		return "";
	}

	public void testScenarios() {
	}

	public boolean verifyLotNoteDesc(String paramString1, String paramString2) {
		log.info("Executing verifyLotNoteDesc with the parameters: lot id - " + paramString1
				+ " and the description is - " + paramString2);
		pptLotNoteInfoInqResult_struct pptLotNoteInfoInqResult_struct = siViewConn.TxLotNoteInfoInq(user,
				id(paramString1));
		pptLotNoteInfo_struct[] arrayOfPptLotNoteInfo_struct = pptLotNoteInfoInqResult_struct.strLotNoteInfo;
		for (pptLotNoteInfo_struct pptLotNoteInfo_struct : arrayOfPptLotNoteInfo_struct) {
			log.info("Getting the lot note description - " + pptLotNoteInfo_struct.lotNoteDescription);
			if (pptLotNoteInfo_struct.lotNoteDescription.contains(paramString2)) {
				log.info("Requested description is - " + paramString2 + " is available in lot id - " + paramString1);
				return true;
			}
		}
		log.warn("Requested description is - " + paramString2 + " is not available in lot id - " + paramString1);
		return false;
	}

	public boolean verifyLotNoteTitle(String paramString1, String paramString2) {
		log.info("Executing verifyLotNoteTitle with the parameters: lot id - " + paramString1
				+ " and the description is - " + paramString2);
		pptLotNoteInfoInqResult_struct pptLotNoteInfoInqResult_struct = siViewConn.TxLotNoteInfoInq(user,
				id(paramString1));
		pptLotNoteInfo_struct[] arrayOfPptLotNoteInfo_struct = pptLotNoteInfoInqResult_struct.strLotNoteInfo;
		for (pptLotNoteInfo_struct pptLotNoteInfo_struct : arrayOfPptLotNoteInfo_struct) {
			log.info("Getting the lot note description - " + pptLotNoteInfo_struct.lotNoteTitle);
			if (pptLotNoteInfo_struct.lotNoteTitle.contains(paramString2)) {
				log.info("Requested description is - " + paramString2 + " is available in lot id - " + paramString1);
				return true;
			}
		}
		log.warn("Requested description is - " + paramString2 + " is not available in lot id - " + paramString1);
		return false;
	}

	public void test() {
	}

	public static void main(String[] paramArrayOfString) throws ParseException {
		try {
			SiView siView = new SiView();
			siView.test();
		} catch (Exception exception) {
			exception.printStackTrace();
		}

	}

	public Calendar getCalendar(String paramString) throws Exception {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
		calendar.setTime(simpleDateFormat.parse(paramString));
		return calendar;
	}

	public Calendar getCalendar(String paramString1, String paramString2) throws Exception {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
		calendar.setTime(simpleDateFormat.parse(getConvertedDate(paramString1, paramString2)));
		return calendar;
	}

	public boolean verifyDateTime(String paramString1, String paramString2, String paramString3) {
		try {
			Calendar calendar1 = getCalendar(paramString1);
			Calendar calendar2 = getCalendar(paramString2, paramString3);
			return (calendar1.before(calendar2) || calendar1.equals(calendar2));
		} catch (Exception exception) {
			return false;
		}
	}

	public String getConvertedDate(String paramString1, String paramString2) throws Exception {
		Date date = new Date();
		String str = new String(paramString1);
		SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
		SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(paramString2);
		date = simpleDateFormat2.parse(str);
		return simpleDateFormat1.format(date).toString();
	}

	public String getFutureDate(String paramString1, String paramString2) {
		Date date = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(paramString1);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(5, Integer.parseInt(paramString2));
		return simpleDateFormat.format(calendar.getTime());
	}

	private enum SIVIEW {
		CANCELABLE, CARRIERCHANGE, CHAMBER, CHAMBERID, CLAIMMEMO, COMMENT, CONTROLWAFER, DEFAULT, DESCRIPTION,
		DUMMYBANK, ENDTIME, ENTITY, EQPCATEGORY, EQPOWNER, EQPTOEQP, EXTERNALPRIORITY, HOLDTIMING, HOLDTRIGGER,
		HOLDTYPE, INHIBIT, INTERNALPRIORITY, LASTCHANGETIME, LASTRECIPE, LOTID, LOTSTATUS, LOTTYPE, MAXCOUNTOFFLOWBATCH,
		MEMO, MONITORBANK, MONITORCREATION, OPERATIONNAME, OPERATIONNUMBER, OPERATIONID, OWNER, OWNERID, PRIORITY,
		PRIORITYCLASS, PRODUCTWAFER, REASONCODE, REASONCODEID, REASONDESC, REASONDESCRIPTION, RECIPE, REGISTRATIONTIME,
		RELATEDLOT, RELATEDLOTID, REPORTTIMESTAMP, RESERVEDCONTROLJOB, RESERVEDFLOWBATCH, RESPONSIBLEOPERATIONMARK,
		RESPONSIBLEOPERATIONNUMBER, RESPONSIBLEROUTEID, RETICLE, RETICLEUSE, ROUTE, RELATEDROUTEID,ROUTEID, SIINFO, SLMCAPABILITY,
		SLMSWITCH, SPECIALCONTROL, STARTTIME, STATUS, STATUSDESCRIPTION, STATUSNAME, SUBLOTTYPE, BANKID, SUBSTATUS,
		TAKEINOUT, TCSRESOURCE, TOTALWAFER, USERNAME, WORKAREA, PRODUCTID, PRODUCTGROUP, MFGLAYER, TECHCODE, RETICLESET,
		OPERNO, TIMESTAMP, REASONCD, USERID, HOLDTP, EXECHOLD,RELATEDCONTROLJOB,RELATEDFAB,RELATEDOPERID,RELATEDOPERNO;
	}
}

/*
 * Location: C:\Users\smallika\Downloads\SIL_Automation\SIL-Autmation_Lrng-
 * 20200330T101037Z-001\SIL-Autmation_Lrng\library\GlobalFoundries\gf_xAFT_SIL.
 * jar!\com\ags\gf\mtqa\app\SiView.class Java compiler version: 6 (50.0) JD-Core
 * Version: 1.1.3
 */