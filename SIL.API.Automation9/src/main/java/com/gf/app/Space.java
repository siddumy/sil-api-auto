package com.gf.app;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.aventstack.extentreports.Status;
import com.camline.space.api.CommunicationException;
import com.camline.space.api.ControlLimitsConfig;
import com.camline.space.api.CorrectiveActionConfig;
import com.camline.space.api.CustomerField;
import com.camline.space.api.IAlarm;
import com.camline.space.api.ICKC;
import com.camline.space.api.IChart;
import com.camline.space.api.ICorrectiveAction;
import com.camline.space.api.IEventFolder;
import com.camline.space.api.ILDS;
import com.camline.space.api.ILDSFolder;
import com.camline.space.api.IOrganizationalFolder;
import com.camline.space.api.ISPCChannel;
import com.camline.space.api.ISample;
import com.camline.space.api.ISource;
import com.camline.space.api.ISpaceAPISession;
import com.camline.space.api.ITSG;
import com.camline.space.api.InvalidConfigException;
import com.camline.space.api.NotPermittedException;
import com.camline.space.api.SPCChannelConfig;
import com.camline.space.api.SPCChannelSettingsConfig;
import com.camline.space.api.SPCChannelState;
import com.camline.space.api.SampleViolation;
import com.camline.space.api.ServerException;
import com.camline.space.api.SpaceAPISessionFactory;
import com.camline.space.api.Valuation;
import com.camline.space.api.ValuationEventIdentifier;
import com.gf.util.GlobalVariables;
import com.gf.util.Helper;
import com.gf.util.ReadPropertyFile;
import com.gf.util.TestReport;

public class Space {
	static Logger logger = LogManager.getLogger(Space.class);
	private static Space space = null;
	private static ISpaceAPISession spaceConn = null;
	private static String spUserName = "";
	private static String spPassword = "";
	private static String spacenavProperties = "";
	private static Valuation[] DefinedValuation = null;
	private static ICorrectiveAction[] Arrayof_CorrectiveAction;
	public static boolean Reverify_Flag = false;

	public Space() {
		openSpaceConnection();
	}

	public static Space getInstance() {
		if(space==null)
		space = new Space();
		return space;
	}

	private static void GetSpaceServerProperty() {
		try {
			spUserName = ReadPropertyFile.getInstance().getProperty("SP_UsrName");
			spPassword = ReadPropertyFile.getInstance().getProperty("SP_Pswrd");
			spacenavProperties = ReadPropertyFile.getInstance().getProperty("spacenavProperties");
		} catch (Exception e) {
			logger.error("Error occured while getting Space server details: " + e);
		}
	}

	public ISpaceAPISession openSpaceConnection() {
		return (null == spaceConn || spaceConn.isTerminated()) ? spaceConn() : spaceConn;
	}

	private ISpaceAPISession spaceConn() {
		logger.debug("Get current SPACE session");
		GetSpaceServerProperty();
		try {
			spaceConn = SpaceAPISessionFactory.createSpaceAPISession(spUserName, spPassword, spacenavProperties);
			logger.debug("Space Current session is " + spaceConn.toString());
		} catch (Exception exception) {
			logger.error("Exception occured while getting the space connection: " + exception);
		}
		return spaceConn;
	}

	public boolean CloseSpaceConnection() {
		boolean Rslt = false;
		try {
			try {
				spaceConn.terminate();
			} catch (Exception exception) {
				logger.error("Exception occured while closing the space connection:" + exception);
			}

			if (spaceConn.isTerminated()) {
				TestReport.GetInstance().log(Status.PASS, "Space Connection is closed");
				return Rslt = spaceConn.isTerminated();

			} else {
				logger.debug("Space connection is not terminated...");
			}
		} catch (Exception exception) {
			logger.error("Space is connection is null:" + exception);
		}
		TestReport.GetInstance().log(Status.FAIL, "Failed to Close the connection");
		return Rslt;
	}

	private ISource GetLDSParentFolder(String SourceFolder) {
		try {
			ISource[] ArraofySourceFolder = spaceConn.getAllSources();
			for (ISource Source_Folder : ArraofySourceFolder) {
				if (Source_Folder.getConfig().getName().equalsIgnoreCase(SourceFolder)) {
					return Source_Folder;
				}
			}
		} catch (CommunicationException e) {
			logger.error("Error occured while getting " + SourceFolder + " parent folder " + e);
		} catch (ServerException e) {
			logger.error("Error occured while getting " + SourceFolder + " parent folder " + e);
		}
		logger.warn(SourceFolder + " Soruce folder is not found");
		return null;
	}

	private ILDS getLDSByName(String paramString) {
		try {
			String str = paramString;
			if (str.contains("/")) {
				ISource SourceFolder = GetLDSParentFolder(str.split("/")[0].trim());
				ILDS[] ArrayOflDS = SourceFolder.getAllLDSNamed(str.split("/")[1].trim());
				logger.debug(str + " LDs is found");
				return ArrayOflDS[0];
			} else {
				ILDS[] arrayoflDS = spaceConn.getAllLDSesNamed(str);
				logger.debug(str + " LDs is found");
				return arrayoflDS[0];
			}

		} catch (Exception exception) {
			logger.error("Exception occured while getting the LDS names by using regular expressions: " + exception);

		}
		return null;
	}

	private IOrganizationalFolder GetOrganizationFolder(ILDS LDS_Name, String FolderName) {
		IOrganizationalFolder iOrganizationalFolder = null;
		if (FolderName.trim() == "")
			return null;

		try {
			String str;
			if (FolderName.indexOf("/") == -1) {
				str = FolderName.trim();
			}

			else {
				str = FolderName.substring(0, FolderName.indexOf("/")).trim();
			}
			IOrganizationalFolder[] arrayOfIOrganizationalFolder = LDS_Name.getAllOrganizationalFoldersNamed(str);
			if (arrayOfIOrganizationalFolder.length != 0) {
				if (FolderName.indexOf("/") == -1) {
					logger.debug(arrayOfIOrganizationalFolder[0].getConfig().getName() + " Folder is found in "
							+ LDS_Name.getParent().getName());
					return arrayOfIOrganizationalFolder[0];
				}

				if (arrayOfIOrganizationalFolder != null)
					str = FolderName.substring(FolderName.indexOf("/") + 1);
				IOrganizationalFolder[] arrayOfIOrganizationalFolder1 = arrayOfIOrganizationalFolder;
				int i = arrayOfIOrganizationalFolder1.length;
				byte b = 0;
				while (b < i) {
					IOrganizationalFolder iOrganizationalFolder1 = arrayOfIOrganizationalFolder1[b];
					iOrganizationalFolder = getRecursiveFolder(iOrganizationalFolder1, str);
					logger.debug(iOrganizationalFolder.getConfig().getName() + " Folder is found in "
							+ LDS_Name.getParent().getName());
					b++;
				}
			}

			if (iOrganizationalFolder == null)
				logger.warn("Unable to find following folder " + FolderName);
		} catch (Exception exception) {
			logger.error("Exception occured while getting the parent folder: " + exception);

		}

		return iOrganizationalFolder;
	}

	private IOrganizationalFolder getRecursiveFolder(IOrganizationalFolder paramIOrganizationalFolder,
			String paramString) {
		try {
			String str;
			if (paramString.indexOf("/") == -1) {
				str = paramString.trim();
			} else {
				str = paramString.substring(0, paramString.indexOf("/")).trim();
			}
			IOrganizationalFolder[] arrayOfIOrganizationalFolder1 = paramIOrganizationalFolder
					.getAllOrganizationalFoldersNamed(str);
			IOrganizationalFolder[] arrayOfIOrganizationalFolder2 = arrayOfIOrganizationalFolder1;
			int i = arrayOfIOrganizationalFolder2.length;
			byte b = 0;
			if (b < i) {
				IOrganizationalFolder iOrganizationalFolder = arrayOfIOrganizationalFolder2[b];
				return (paramString.indexOf("/") == -1) ? iOrganizationalFolder
						: getRecursiveFolder(iOrganizationalFolder,
								paramString.substring(paramString.indexOf("/") + 1));
			}

			return null;
		}

		catch (CommunicationException var1) {
			logger.error("Errror occured while getting IOrganizationFolder details " + var1);
		} catch (ServerException var2) {
			logger.error("Errror occured while getting IOrganizationFolder details " + var2);
		}

		return null;
	}

	private ILDSFolder GetLDSfolder(IOrganizationalFolder IorgFolderName, String LDSFolder) {
		ILDSFolder[] LDS_FolderName;
		try {
			LDS_FolderName = IorgFolderName.getAllLDSFoldersNamed(LDSFolder);
			if (LDS_FolderName.length != 0) {
				logger.debug(LDS_FolderName[0].getConfig().getName() + "LDS folder is found");
				return LDS_FolderName[0];

			}
		} catch (CommunicationException e) {
			logger.error("Error occuered while getting " + LDSFolder + " LDS folder>>" + e);
			return null;
		} catch (ServerException e) {
			logger.error("Error occuered while getting " + LDSFolder + " LDS folder>>" + e);
			return null;
		}
		logger.warn("Unable to find " + LDSFolder + " LDS folder in Space");
		return null;
	}

	private ISPCChannel getChannel(String LDSName, String OrganizedFolder, String LSDFolderName,
			String ChannelName) {
		try {

			ILDS LDS = getLDSByName(LDSName);
			IOrganizationalFolder IOGfolderName = GetOrganizationFolder(LDS, OrganizedFolder);
			ILDSFolder LdschannelPathName = GetLDSfolder(IOGfolderName, LSDFolderName);
			ISPCChannel[] List_SPCChannels = LdschannelPathName.getAllSPCChannels();
			for (ISPCChannel spcChannel : List_SPCChannels) {
				if (spcChannel.getConfig().getName().equalsIgnoreCase(ChannelName)) {
					logger.info(ChannelName + " is  found in spc channel section:- " + LSDFolderName);
					return spcChannel;
				}
			}
			if (!Reverify_Flag) {
				Reverify_Flag = true;
				Thread.sleep(GlobalVariables.WaitInMilSec);
				 getChannel(LDSName, OrganizedFolder, LSDFolderName, ChannelName);
			}
			logger.warn(ChannelName + " is not found in spc channel section");

		} catch (Exception exception) {

			logger.error("Exception occured while getting the ISPC channel: ", exception);

		} finally {
			Reverify_Flag = false;
		}
		return null;
	}

	public boolean CreateTemplate(String LDSName, String TemplateFolder, String TemplateName, String ChannelName,String ParameterName) {
		try {
			ISPCChannel template = null;
			ILDS LDS = getLDSByName(LDSName);
			ILDSFolder[] Ldsfolders = LDS.getAllLDSFoldersNamed(TemplateFolder);
			ISPCChannel[] List_SPCChannels = Ldsfolders[0].getAllSPCChannels();
			if (TemplateName == null | TemplateName.isEmpty() | TemplateName.equalsIgnoreCase("empty")) {
				template = List_SPCChannels[0];
			} else
				for (ISPCChannel spcchannel : List_SPCChannels)
					if (spcchannel.getConfig().getName().equalsIgnoreCase(TemplateName)) {
						template = spcchannel;
						break;
					}
			if (template != null) {
				SPCChannelConfig templateConfig = template.getConfig();
				templateConfig.setParameterName(ParameterName);
				if(ChannelName!=null&&!ChannelName.isEmpty()&&!ChannelName.equalsIgnoreCase("empty"))
				templateConfig.setName(ChannelName);
				Ldsfolders[0].createSPCChannel(templateConfig);
				
				TestReport.GetInstance().log(Status.PASS, ParameterName + " New template is created");
				logger.info( ParameterName + " New template is created");
				return true;
			}

			TestReport.GetInstance().log(Status.FAIL,
					"Existing template [ " + TemplateName + " ] is not found in LDS folder:- " + TemplateFolder);
			logger.debug("Existing template [ " + TemplateName + " ] is not found in LDS folder:- " + TemplateFolder);

		} catch (Exception e) {
			TestReport.GetInstance().log(Status.FAIL, "Failed while Creating new template:- " + e);
			logger.debug("Failed while Creating new template:- " + e);
		}

		return false;
	}

	public boolean CreateTemplateWithSameChannel(String LDSName, String TemplateFolder, String TemplateName,String ParameterName) {
		return CreateTemplate(LDSName,TemplateFolder,TemplateName,"",ParameterName);
	}
	public boolean CreateTemplate(String LDSName, String TemplateFolder, String ChannelName,String ParameterName) {
		return CreateTemplate(LDSName,TemplateFolder,"",ChannelName,ParameterName);
	}
	
	public boolean VerifyChannel(String LDSName, String OrganizedFolder, String LSDFolderName, String ChannelName) {
		if (getChannel(LDSName, OrganizedFolder, LSDFolderName, ChannelName) != null) {
			TestReport.GetInstance().log(Status.PASS, ChannelName + ": Channel is Found in " + LSDFolderName);
			return true;
		} else {
			TestReport.GetInstance().log(Status.FAIL, ChannelName + ": Channel is not Found in " + LSDFolderName);
			return false;
		}
	}

	public boolean DeleteChannel(String LDSName, String OrganizedFolder, String LSDFolderName, String ChannelName) {
		try {
			ILDS LDS = getLDSByName(LDSName);
			ILDSFolder[] LdschannelPathName = LDS.getAllLDSFoldersNamed(LSDFolderName);
			ISPCChannel[] List_SPCChannels = LdschannelPathName[0].getAllSPCChannels();
			boolean flag = false;
			if (List_SPCChannels.length != 0) {
				for (ISPCChannel spcChannel : List_SPCChannels)
					if (spcChannel.getConfig().getName().equalsIgnoreCase(ChannelName))
					{	spcChannel.delete(true, true);flag = true;}
				
				// ISPCChannel Channel = getChannel(LDSName, OrganizedFolder, LSDFolderName,
				// ChannelName);
				// Channel.delete(true, true);
				if (flag) {
					TestReport.GetInstance().log(Status.PASS,
							ChannelName + "-Channel is deleted from: " + LSDFolderName);

					logger.info(ChannelName + ":- Channels is deleted from: " + LSDFolderName);
				}

				else
					TestReport.GetInstance().log(Status.PASS, "[ "+ChannelName + " ] Channel is not found to delete ");
			}
			return true;
		} catch (Exception e) {

			TestReport.GetInstance().log(Status.FAIL,
					ChannelName + "Failed to while deleting the channel due to :-" + e);
			logger.error("Failed to delete the channel :" + e);
			return false;
		}

	}
public boolean DeleteChannel(String LDSName,  String LSDFolderName, String SPCChannelName)
{
 return DeleteChannel( LDSName,  "",  LSDFolderName,  SPCChannelName);
}

public boolean DeleteChannelByFolder(String LDSName, String OrganizedFolder, String LSDFolderName) {
		try {
			ILDS LDS = getLDSByName(LDSName);
			IOrganizationalFolder IOGfolderName = GetOrganizationFolder(LDS, OrganizedFolder);
			ILDSFolder LdschannelPathName = GetLDSfolder(IOGfolderName, LSDFolderName);
			ISPCChannel[] List_SPCChannels = LdschannelPathName.getAllSPCChannels();
			if (List_SPCChannels != null && List_SPCChannels.length != 0)
				for (ISPCChannel spcChannel : List_SPCChannels)
					spcChannel.delete(true, true);
			logger.info("All the Channels are deleted from: " + LSDFolderName);
			TestReport.GetInstance().log(Status.PASS, "All the Channels are deleted from: " + LSDFolderName);
			return true;
		} catch (Exception e) {
			TestReport.GetInstance().log(Status.FAIL,
					"Failed to delete the channels from " + LSDFolderName + "due to :- " + e);
			logger.error("Failed to delete the channels from " + LSDFolderName + "due to :- " + e);
			return false;
		}

	}

	private Valuation[] DefinedValuationList() {
		if (DefinedValuation == null)
			DefinedValuation = SPCChannelConfig.getAllDefinedValuations();
		return DefinedValuation;
	}

	private Valuation[] GetAttachedValuations(ISPCChannel SPCChannel) {
		try {
			logger.debug("Getting all attached valuation for channel" + SPCChannel.getConfig().getName());
			return SPCChannel.getConfig().getAttachedValuations();
		} catch (CommunicationException e) {
			logger.error("Error occured while getting attached valuation list " + e);

		} catch (ServerException e) {
			logger.error("Error occured while getting attached valuation list " + e);

		}
		return null;

	}

	public boolean RemoveAttachedValuations(String LDSName, String OrganizedFolder, String LSDFolderName,
			String ChannelName) {
		try {
			ISPCChannel SPCChannel = getChannel(LDSName, OrganizedFolder, LSDFolderName, ChannelName);
			if (SPCChannel != null) {
				SPCChannelConfig spcchannelConfig = SPCChannel.getConfig();
				Valuation[] arrayOfValuation = new Valuation[0];
				spcchannelConfig.setAttachedValuations(arrayOfValuation);
				SPCChannel.setConfig(spcchannelConfig, "By Automated tests", true);
				logger.debug("Removed all the attached valuations for the channel: " + ChannelName);
				TestReport.GetInstance().log(Status.PASS, "Attached Valuation events are removed from " + ChannelName);
				return true;
			} else {
				logger.error(ChannelName + " Channel is not found! ");
				TestReport.GetInstance().log(Status.WARNING, ChannelName + " Channel is not found ");
				return false;
			}
		} catch (Exception exception) {
			TestReport.GetInstance().log(Status.FAIL, "Unable to remove valuations events: " + exception);
			logger.error("Exception occured while updating the valuations and events: " + exception);
			return false;

		}

	}

	public boolean AddValuationts(String LDSName, String OrganizedFolder, String LSDFolderName, String ChannelName,
			String Valuations) {
		// multiple valuation will sent with , ex.Row above specification,Row below
		// specification
		try {
			ISPCChannel SPCChannel = getChannel(LDSName, OrganizedFolder, LSDFolderName, ChannelName);
			String[] ArrayofValuation = Valuations.split(",");
			int size = ArrayofValuation.length;
			Valuation[] NewArrayOfvaluations = new Valuation[size];
			Valuation[] AlldefinedValuation = DefinedValuationList();
			int temp = 0;
			if (SPCChannel != null) {
				Va_skip: for (String str : ArrayofValuation) {
					for (Valuation Val : AlldefinedValuation) {
						if (Val.getName().equalsIgnoreCase(str)) {
							NewArrayOfvaluations[temp] = Val;
							temp++;
							continue Va_skip;
						}
					}
					logger.warn(str + "valuation is not found in defined valuation list");
					TestReport.GetInstance().log(Status.WARNING, "valuation is not found in defined valuation list");
				}

				SPCChannelConfig spcchannelConfig = SPCChannel.getConfig();
				spcchannelConfig.setAttachedValuations(NewArrayOfvaluations);
				SPCChannel.setConfig(spcchannelConfig, "By Automated tests", true);
				TestReport.GetInstance().log(Status.PASS,
						"[ " + Valuations + " ] " + "valuation are added to the channel " + ChannelName);
				return true;
			}
			TestReport.GetInstance().log(Status.SKIP, ChannelName + "Channel is not found");

		} catch (Exception e) {
			TestReport.GetInstance().log(Status.WARNING,
					"valuation is not found in defined valuation list: " + e.getMessage());
			logger.error("Exception occuered while adding valuations" + e);
		}
		return false;

	}

	@SuppressWarnings("unused")
	public boolean RemoveAllAttachedValuationEvents(String LDSName, String OrganizedFolder, String LSDFolderName,
			String ChannelName) {
		try {

			ISPCChannel SPCChannel = getChannel(LDSName, OrganizedFolder, LSDFolderName, ChannelName);
			Valuation[] valuation = SPCChannel.getConfig().getAttachedValuations();
			if (SPCChannel != null) {
				for (Valuation val : valuation) {
					val.setAttachedValuationEvents(new ValuationEventIdentifier[0]);
				}
				SPCChannelConfig spcchannelConfig = SPCChannel.getConfig();
				spcchannelConfig.setAttachedValuations(valuation);
				SPCChannel.setConfig(spcchannelConfig, "By Automated tests", true);
				TestReport.GetInstance().log(Status.PASS,
						"All the attached events are removed from the channel" + ChannelName);
				return true;
			}
			TestReport.GetInstance().log(Status.SKIP, ChannelName + "Channel is not");
			return false;

		} catch (Exception e) {
			logger.error("Error occured while removeing attached valuation events" + e);
			TestReport.GetInstance().log(Status.FAIL, "Error occured while removing the valuation events: " + e);
			return false;
		}

	}

	public boolean AddValuationEvents(String LDSName, String OrganizedFolder, String LSDFolderName,
			String ChannelName, String EventFldr, String Events) {
		try {
			ISPCChannel SPCChannel = getChannel(LDSName, OrganizedFolder, LSDFolderName, ChannelName);
			Valuation[] valuations = GetAttachedValuations(SPCChannel);
			int temp = 0;
			String[] Evnt1 = Events.split(",");
			ValuationEventIdentifier[] VEI = new ValuationEventIdentifier[Evnt1.length];
			if (SPCChannel != null) {
				for (String E : Evnt1) {
					VEI[temp] = GetEventvaluationidentifier(EventFldr, E);
					temp++;
				}
				for (Valuation v1 : valuations) {
					v1.setAttachedValuationEvents(VEI);
				}
				SPCChannelConfig sPCChannelConfig = SPCChannel.getConfig();
				sPCChannelConfig.setAttachedValuations(valuations);
				SPCChannel.setConfig(sPCChannelConfig, "By Automated tests", true);
				TestReport.GetInstance().log(Status.PASS,
						"[ " + Events + " ]" + " Events are added to the channel-" + ChannelName + " properties");
				logger.info("[ " + Events + " ]" + " Events are added to the channel-" + ChannelName + " properties");
				return true;
			}
			TestReport.GetInstance().log(Status.SKIP, ChannelName + "Channel is not found");
			return false;
		} catch (Exception e) {
			TestReport.GetInstance().log(Status.FAIL, Events + " events are not added due to :" + e);
			logger.error(" error occured while updating events" + e);
			return false;
		}

	}

	public ICorrectiveAction[] getArrayofCorrectiveActions(String CA_Names) {
		String[] ArrayofCANames = CA_Names.split(",");
		Arrayof_CorrectiveAction = new ICorrectiveAction[ArrayofCANames.length];
		int temp = 0;
		for (String str : ArrayofCANames) {
			Arrayof_CorrectiveAction[temp] = GetRequiredCA(str);
			temp++;
		}
		return Arrayof_CorrectiveAction;
	}

	public boolean AddValuationEvents(String LDSName, String OrganizedFolder, String LSDFolderName,
			String ChannelName, String Events) {
		// CA will added with , if multiple CA's to add
		String EventFolder = GlobalVariables.EventFolder;
		if (EventFolder != null && !EventFolder.isEmpty())
			return AddValuationEvents(LDSName, OrganizedFolder, LSDFolderName, ChannelName, EventFolder, Events);
		else
			TestReport.GetInstance().log(Status.FAIL, "Event folder is not defined");
		return false;
	}

	private ValuationEventIdentifier GetEventvaluationidentifier(String EventFolderName, String Event) {
		IEventFolder EventFldr = GetEventFolder(EventFolderName);

		try {
			if (EventFldr != null) {

				ICorrectiveAction[] correctiveAction = EventFldr.getAllCorrectiveActions();
				for (ICorrectiveAction CA : correctiveAction)
					if (CA.getConfig().getName().equalsIgnoreCase(Event.trim()))
						return CA.getValuationEventIdentifier();

				IAlarm[] Alarm = EventFldr.getAllAlarms();
				for (IAlarm A : Alarm)
					if (A.getConfig().getName().equalsIgnoreCase(Event.trim()))
						return A.getValuationEventIdentifier();

				ITSG[] Tsg = EventFldr.getAllTSGs();
				for (ITSG tsg : Tsg)
					if (tsg.getConfig().getName().equalsIgnoreCase(Event.trim()))
						return tsg.getValuationEventIdentifier();

				logger.debug(Event + "is not exist in the mentioned configuration folder: "
						+ EventFldr.getConfig().getName());
			} else {
				logger.warn("Event folder is null ");
				return null;
			}
		} catch (CommunicationException e) {
			logger.error("Error occured while getting eventIdentifier for the event " + Event + " " + e);

		} catch (ServerException e) {
			logger.error("Error occured while getting eventIdentifier for the event " + Event + " " + e);
		}
		return null;

	}

	private IEventFolder GetEventFolder(String EventFolderName) {
		try {
			IEventFolder[] EvntFolder = spaceConn.getAllEventFolders();
			for (IEventFolder Eventfolder1 : EvntFolder) {
				if (Eventfolder1.getConfig().getName().trim().equalsIgnoreCase(EventFolderName)) {
					logger.debug(EventFolderName + "Event folder is found");

					return Eventfolder1;
				}
			}
			logger.warn(EventFolderName + " Event folder is not present in space");
			return null;
		} catch (Exception e) {
			logger.error("Error occured while getting Eventfolder " + EventFolderName + " " + e);
			return null;
		}

	}

	private ISample[] GetChartSample(ISPCChannel Channel, String Startdate, String Enddate) {
		try {
			logger.debug("Getting Chart Samples for channel" + Channel.getConfig().getName());
			String StartDate = Startdate.trim();
			String EndDate = Enddate.trim();
			Date Sdate = null;
			Date Edate = null;

			if (StartDate != null && !StartDate.isEmpty() && !StartDate.equalsIgnoreCase("EMPTY")) {
				Sdate = new SimpleDateFormat("dd/MM/yyyy").parse(Startdate);
			}

			if (!(EndDate == null) && !EndDate.isEmpty() && !EndDate.equalsIgnoreCase("EMPTY")) {
				Edate = new SimpleDateFormat("dd/MM/yyyy").parse(Startdate);
			}
			Channel.refresh();
			ISample[] sample = Channel.getSamples(Sdate, Edate, 0, 0, true);

			logger.debug(sample.length + " samples found in " + Channel.getConfig().getName());
			return sample;
		} catch (CommunicationException e) {
			logger.error("Error occured while getting Chart Sample for the channel>>>" + e);

		} catch (ServerException e) {
			logger.error("Error occured while getting Chart Sample for the channel>>>" + e);
		} catch (ParseException e) {
			logger.error("Error occured while While converting Start or end Date>>>" + e);
		}
		return null;
	}

	private ISample[] GetCKCSample(ISPCChannel Channel, String Startdate, String Enddate, ISample[] ChartSamples) {
		try {
			logger.debug("Getting CKC Chart Samples for channel" + Channel.getConfig().getName());
			String StartDate = Startdate.trim();
			String EndDate = Enddate.trim();
			Date Sdate = null;
			Date Edate = null;
			if (ChartSamples == null) {
				logger.warn(" Chart Sample is null");
				return null;
			}
			ISample[] sample = new ISample[ChartSamples.length];
			int temp = 0;
			Channel.refresh();
			ICKC[] ArrayofiCKCs = Channel.getAllCKCs(); // need to check the ckc details

			if (StartDate != null && !StartDate.isEmpty() && !StartDate.equalsIgnoreCase("EMPTY")) {
				Sdate = new SimpleDateFormat("dd/MM/yyyy").parse(Startdate);
			}

			if (!(EndDate == null) && !EndDate.isEmpty() && !EndDate.equalsIgnoreCase("EMPTY")) {
				Edate = new SimpleDateFormat("dd/MM/yyyy").parse(Startdate);
			}
			for (ICKC iCKC : ArrayofiCKCs) {
				for (ISample sa : iCKC.getSamples(Sdate, Edate, 0, 0, true)) {
					sample[temp] = sa;
					temp++;
				}
			}

			logger.debug(sample.length + " CKC samples found in " + Channel.getConfig().getName());
			return sample;
		} catch (ServerException e) {
			logger.error("Error occured while getting Chart Sample for the channel>>>" + e);

		} catch (CommunicationException e) {
			logger.error("Error occured while getting Chart Sample for the channel>>>" + e);
		} catch (ParseException e) {
			logger.error("Error occured while While converting Start or end Date>>>" + e);
		}
		return null;
	}

	private String GetAttributeValue(ISample[] CKCSample, int index, String Attributes) {
		try {

			if (Attributes == null || CKCSample == null) {
				if (CKCSample == null)
					logger.error("samples are null hence returning with blank ");
				else
					logger.error("Verifying Attributes are not present or null ");
				return null;
			}
			ISample[] ArrayofCKCSample = CKCSample;
			String str1 = "";
			
				String[] str = Attributes.split("=");
					SPACE sPACE;
					sPACE = SPACE.valueOf(str[0].replaceAll(" ", "").toUpperCase().trim());
					switch (sPACE) {
					case MAX:
					case MAXVALUE:
							str1 = "MAXVALUE: " + ArrayofCKCSample[index].getStatistics().getMax();
						break;
					case MIN:
					case MINVALUE:
							str1 = "MINVALUE:" + ArrayofCKCSample[index].getStatistics().getMin();
						break;
					case MEAN:
					case MEANVALUE:
							str1 = "MEANVALUE:" + ArrayofCKCSample[index].getStatistics().getMean();
						break;

					case UPPERSPEC:
						if (ArrayofCKCSample[index].getSpecifications().isLimitEnabled(0)) {
								str1 = "Upper Spec value is "
										+ ArrayofCKCSample[index].getSpecifications().getLimitValue(0);
						} else {
							logger.warn("Sample channel specification is not enabled");
							str1 = "Sample channel specification is not enabled";
						}
						break;

					case LOWERSPEC:

						if (ArrayofCKCSample[index].getSpecifications().isLimitEnabled(2)) {

							str1 = "Lower Spec value is "
										+ ArrayofCKCSample[index].getSpecifications().getLimitValue(2);
												} else {
							logger.warn("Sample channel specification is not enabled");
							str1 = "**Sample channel specification is not enabled";
						}
						break;

					case SPECTARGET:
						if (ArrayofCKCSample[index].getSpecifications().isLimitEnabled(0)
								|| ArrayofCKCSample[index].getSpecifications().isLimitEnabled(2)) {
							str1 = "Lower Spec value is "
										+ ArrayofCKCSample[index].getSpecifications().getLimitValue(1);
													} else {
							logger.warn("Sample channel specification is not enabled");
							str1 = "**Failed due to**  Sample channel specification is not enabled";
						}
						break;

					case EXTERNALCOMMENT:
							if (ArrayofCKCSample[index].getExternalComment() != null) {
									str1 = "EXTERNALCOMMENT:" + ArrayofCKCSample[index].getExternalComment();
								} 
							 else {
								logger.warn("External comment is null");
								str1 = "empty or null";
							}
						break;

					case INTERNALCOMMENT:
							if (ArrayofCKCSample[index].getInternalComment() != null) {
									str1 = ArrayofCKCSample[index].getInternalComment();
									}
								  else {
									  logger.warn("Internal comment is null :- "+ " or or Internal flag:-"
										+ ArrayofCKCSample[index].isInternallyFlagged());
								str1 = "empty or null";;
							}
						break;
					case INTERNALFLAG:
						str1=Boolean.toString(ArrayofCKCSample[index].isInternallyFlagged());
						break;
					case EXTERNALFLAG:
						str1=Boolean.toString(ArrayofCKCSample[index].isExternallyFlagged());
						break;

					case CKCVIOLATIONCOMMENT:
					case VIOLATIONCOMMENT:
						SampleViolation[] ArrayViolanceCmnt = ArrayofCKCSample[index]
								.getViolations(ArrayofCKCSample[index].getCharts()[0]);
						int temp = 0;
						for (SampleViolation sv : ArrayViolanceCmnt) {
							if (sv.getComment() != null
									&& sv.getComment().toUpperCase().contains(str[1].toUpperCase())) {
								str1 = "VIOLATIONCOMMENT: " + sv.getComment();
								
								break;
							} else if (temp == ArrayViolanceCmnt.length - 1) {
								logger.warn("**Failed due to**  " + "Expected Violation Comment is " + str[1].trim()
										+ " Actaul Violation comment  is " + sv.getComment());
								str1 = "**Failed due to**  " + "Expected Violation Comment is " + str[1].trim()
										+ " Actaul Violation comment  is " + sv.getComment();
								
							}
							temp++;
						}
						break;
					case CKCVIOLATIONTEXTS:
					case VIOLATIONTEXT:
						SampleViolation[] ArrayviolanceTxt = ArrayofCKCSample[index]
								.getViolations(ArrayofCKCSample[index].getCharts()[0]);
						int temp1 = 0;
						for (SampleViolation st : ArrayviolanceTxt) {
							if (st.getText() != null && st.getText().toUpperCase().contains(str[1].toUpperCase())) {
								str1 = "VIOLATIONTEXT: " + st.getText();
								
								break;
							}

							else if (temp1 == ArrayviolanceTxt.length - 1) {
								logger.warn("**Failed due to**  " + "Expected Violation text is[ " + str[1].trim()
										+ " Actaul Violation text  is " + st.getText());
								str1 = "**Failed due to**  " + "Expected Violation text is [ " + str[1].trim()
										+ " Actaul Violation text  is " + st.getText()+" ]";
								
							}
							temp1++;
						}
						break;
					case LOT:
						str1 =ArrayofCKCSample[index].getExtractorKeys()[0];
						
						break;
					case WAFER:
						str1 =ArrayofCKCSample[index].getExtractorKeys()[1];
						
						break;
					case PTOOL:
						str1 =ArrayofCKCSample[index].getExtractorKeys()[2];
						
						break;
					case PCHAMBER:
						str1 =ArrayofCKCSample[index].getExtractorKeys()[3];
						
						break;
					case TECHNOLOGY:
						str1 =ArrayofCKCSample[index].getExtractorKeys()[4];
						
						break;
					case ROUTE:
						str1 =ArrayofCKCSample[index].getExtractorKeys()[5];
						
						break;
					case PRODUCTGROUP:
						str1 =ArrayofCKCSample[index].getExtractorKeys()[6];
						
						break;
					
					case PRODUCTEC:
						str1 =ArrayofCKCSample[index].getExtractorKeys()[7];
						
						break;
					case POPERATIONID:
						str1 =ArrayofCKCSample[index].getExtractorKeys()[8];
						
						break;
					case EVENT:
						str1 =ArrayofCKCSample[index].getExtractorKeys()[9];
						
						break;
					case PHOTOLAYER:
						str1 =ArrayofCKCSample[index].getExtractorKeys()[10];
						
						break;
					case RESERVE1:
						str1 =ArrayofCKCSample[index].getExtractorKeys()[11];
						
						break;
					
					case RESERVE2:
						str1 =ArrayofCKCSample[index].getExtractorKeys()[13];
						
						break;
					case RESERVE3:
						str1 =ArrayofCKCSample[index].getExtractorKeys()[14];
						
						break;
					case RESERVE4:
						str1 =ArrayofCKCSample[index].getExtractorKeys()[15];
						
						break;
					case RESERVE5:
						str1 =ArrayofCKCSample[index].getExtractorKeys()[16];
						
						break;
					case RESERVE6:
						str1 =ArrayofCKCSample[index].getExtractorKeys()[17];
						
						break;
					case RESERVE7:
						str1 =ArrayofCKCSample[index].getExtractorKeys()[18];
						
						break;
					case RESERVE8:
						str1 =ArrayofCKCSample[index].getExtractorKeys()[19];
						
						break;
					case MTIME:
						str1 =ArrayofCKCSample[index].getDataKeys()[0];
						
						break;
					case MTOOL:
						str1 =ArrayofCKCSample[index].getDataKeys()[1];
						
						break;
					case MOPERATIONID:
						str1 =ArrayofCKCSample[index].getDataKeys()[2];
						
						break;
					case SITEID:
						str1 =ArrayofCKCSample[index].getDataKeys()[3];
						
						break;
					case X:
						str1 =ArrayofCKCSample[index].getDataKeys()[4];
						
						break;
					case Y:
						str1 =ArrayofCKCSample[index].getDataKeys()[5];
						
						break;
					case FOUP:
						str1 =ArrayofCKCSample[index].getDataKeys()[6];
						
						break;
					case RETICLE:
						str1 =ArrayofCKCSample[index].getDataKeys()[7];
						
						break;
					case MSLOT:
						str1 =ArrayofCKCSample[index].getDataKeys()[8];
						
						break;
					case SUBLOTTYPE:
						str1 =ArrayofCKCSample[index].getDataKeys()[9];
						
						break;
					case PSEQUENCE:
						str1 =ArrayofCKCSample[index].getDataKeys()[10];
						
						break;
					case AUTOFLAGLOTTYPE:
						str1 =ArrayofCKCSample[index].getDataKeys()[11];
						
						break;
					case DATRESERVE1:
						str1 =ArrayofCKCSample[index].getDataKeys()[12];
						
						break;
					case DATRESERVE2:
						str1 =ArrayofCKCSample[index].getDataKeys()[13];
						
						break;
					case DATRESERVE3:
						str1 =ArrayofCKCSample[index].getDataKeys()[4];
						
						break;
					case DATRESERVE4:
						str1 =ArrayofCKCSample[index].getDataKeys()[15];
						
						break;
					case DATRESERVE5:
						str1 =ArrayofCKCSample[index].getDataKeys()[16];
						
						break;
					case DATRESERVE6:
						str1 =ArrayofCKCSample[index].getDataKeys()[17];
						
						break;
					case DATRESERVE7:
						str1 =ArrayofCKCSample[index].getDataKeys()[18];
						
						break;
					case DATRESERVE8:
						str1 =ArrayofCKCSample[index].getDataKeys()[19];
						
						break;
		
					default:
						logger.warn("No Samples attributes found to verify");
						break;
					}

				return str1;
		}

		catch (Exception e) {
			logger.error("Error occured while validating the Sample attributes:-" + e);
			String str =  "**Failed due to**  " + e ;
			return str;
		}

	}

	public boolean VerifyAttributes(String LDSName, String OrganizedFolder, String LSDFolderName, String ChannelName,
			String Startdate, String Enddate, String SampleIndex, String Attributes) {
		try {

			ISPCChannel SPCChannel = getChannel(LDSName, OrganizedFolder, LSDFolderName, ChannelName);

			if (SPCChannel != null) {

				SPCChannel.refresh();
				ISample[] ChartSample = GetChartSample(SPCChannel, Startdate, Enddate);
				ISample[] CKCSample = GetCKCSample(SPCChannel, Startdate, Enddate, ChartSample);
				// ***
				String[] ArrayofAttribute = Attributes.split("~#~");
				String[] SampleNumbers = SampleIndex.split(",");
				for (String Sample_index : SampleNumbers) {
					int index = CKCSample.length - Integer.parseInt(Sample_index);
					if (index < 0) {
						logger.warn("Sample index is greater than total number of samples present in channel");
						continue;
					}
					
					for (String Attribute : ArrayofAttribute) {
						String ArrayofStringvalue = GetAttributeValue(CKCSample, index, Attribute);
						if (ArrayofStringvalue.toUpperCase().contains(Attribute.split("=")[1].toUpperCase())) {
							TestReport.GetInstance().log(Status.PASS,
									ChannelName + " Channel attributes value are verified and [ "
											+ Attribute.split("=")[0] + " ] value: [ " + ArrayofStringvalue + " ]");
							logger.info(ChannelName + " Channel attributes value are verified and [ "
									+ Attribute.split("=")[0] + " ] value: [ " + ArrayofStringvalue + " ]");
						} else {
							if (!GlobalVariables.Reverify_Flag) {
								GlobalVariables.Reverify_Flag = true;
								Thread.sleep(GlobalVariables.WaitInMilSec);
								VerifyAttributes(LDSName, OrganizedFolder, LSDFolderName, ChannelName, Startdate,
										Enddate, SampleIndex, Attributes);
							}
							TestReport.GetInstance().log(Status.WARNING,
									ChannelName + " Channel attributes value are verified and Expected [ "
											+ Attribute.split("=")[0] + " ] value: [ " + Attribute.split("=")[1]
											+ " ] but Actual [ " + Attribute.split("=")[0] + " ] value is [ "
											+ ArrayofStringvalue + " ]");
							logger.info(ChannelName + " Channel attributes value are verified and Expected [ "
									+ Attribute.split("=")[0] + " ] value: [ " + Attribute.split("=")[1]
									+ " ] but Actual [ " + Attribute.split("=")[0] + " ] value is [ "
									+ ArrayofStringvalue + " ]");
						}
					}
				}
				return true;

			}
			TestReport.GetInstance().log(Status.FAIL,
					"Channel is not found and Please check the Channel is created or enter valid channel name!");

		} catch (Exception e) {
			logger.error("Error occured while verifying the attributes" + e);
			TestReport.GetInstance().log(Status.FAIL, "Error occured while verifying the attributes" + e);

		} finally {
			GlobalVariables.Reverify_Flag = false;
		}
		return false;

	}

	private ICorrectiveAction GetRequiredCA(String EventFolder, String CorrectiveAction) {
		if (CorrectiveAction == null) {
			logger.error("Corrective action is blank or null");
			return null;
		}
		try {

			IEventFolder EvntFldr = GetEventFolder(EventFolder);
			ICorrectiveAction[] ListOfCAs = EvntFldr.getAllCorrectiveActions();
			for (ICorrectiveAction CA : ListOfCAs)
				if (CA.getConfig().getName().equalsIgnoreCase(CorrectiveAction)) {
					return CA;
				}
			logger.warn(CorrectiveAction + " CA is not found in Corrective Action list in the event folder");

		} catch (Exception e) {
			logger.error("Error occured while getting the corrective" + e);
		}
		return null;
	}

	private ICorrectiveAction GetRequiredCA(String CorrectiveAction) {
		String EventFolder = GlobalVariables.EventFolder;
		return GetRequiredCA(EventFolder, CorrectiveAction);
	}

	private String AssignCA(String LDSName, String OrganizedFolder, String LSDFolderName, String ChannelName,
			String Startdate, String Enddate, String CA, String SampleIndex, String ViolationComment) {
		try {
			ISPCChannel spcchannel = getChannel(LDSName, OrganizedFolder, LSDFolderName, ChannelName);
			if (spcchannel != null) {
				ISample[] ChartSamples = GetChartSample(spcchannel, Startdate, Enddate);
				ISample[] CKCSamples = GetCKCSample(spcchannel, Startdate, Enddate, ChartSamples);

				int i = 0;
				String Violation_comment;
				if (SampleIndex.trim().equalsIgnoreCase("") || SampleIndex.trim().isEmpty()) {
					i = CKCSamples.length - 1;
				} else
					i = CKCSamples.length - Integer.parseInt(SampleIndex.trim());
				ISample CKC_Sample = CKCSamples[i];
				IChart[] charts = CKC_Sample.getCharts();

				for (IChart chart : charts) {

					if (ViolationComment == null || ViolationComment.equalsIgnoreCase("")
							|| ViolationComment.equalsIgnoreCase("EMPTY"))
						Violation_comment = "Auto script voilation ";
					else
						Violation_comment = ViolationComment;
					ICorrectiveAction CorrectiveAction = GetRequiredCA(CA);
					if (CorrectiveAction == null)
						throw new Exception("assignCorrectiveAction: Specified corrective action " + CA
								+ " is not available/enabled");

					CKC_Sample.assignCorrectiveAction(chart, CorrectiveAction, Violation_comment);
					logger.debug(CA + " corrective action is assigned to sample from the channel "
							+ spcchannel.getConfig().getName());
				}
				return "NormalEnd";

			}
			return "Channel not Defined";
		}

		catch (Exception e) {
			logger.error("The problem occured while assigning the corrective actions:" + e);
			return " CA not assigned: " + e.getMessage().replace("\n", "");
		}
	}

	public boolean assignCAAttributes(String EventFolderName, String CA, String CAattributes) {
		logger.info("assignCAAttributes- parameters are::: event folder name: " + EventFolderName
				+ " -->>Corrective Action: " + CA + "--> and attributes: " + CAattributes);
		try {
			//check
			// ICorrectiveAction CorrectiveAction = GetRequiredCA(EventFolderName, CA);
			ResetCAAttributes(CA);// taking backup for restoring
			Arrayof_CorrectiveAction = getArrayofCorrectiveActions(CA);
			for (ICorrectiveAction CorrectiveAction : Arrayof_CorrectiveAction) {
				CorrectiveActionConfig correctiveActionConfig = CorrectiveAction.getConfig();

				if (CAattributes != null && !CAattributes.isEmpty() && !CAattributes.equalsIgnoreCase("EMPTY")) {
					String[] ArrayOfAttributes = CAattributes.split("~#~");
					String str = assignCAAttributes(CorrectiveAction, ArrayOfAttributes);
					if (str.contains("Failed due to")) {
						logger.error("Failed to Assign Attributes [ " + CAattributes + " ]  to the CA" + CA
								+ "in the Event Folder: " + EventFolderName + " " + str);
						TestReport.GetInstance().log(Status.FAIL, "Failed to Assign Attributes [ " + CAattributes
								+ " ]  to the CA" + CA + "in the Event Folder: " + EventFolderName + " " + str);
						return false;
					}
					logger.info("[ " + CAattributes + " ]" + "attributes are assigned to CA:- " + CA
							+ " in the EventFolders:- " + EventFolderName);
					TestReport.GetInstance().log(Status.PASS, "[ " + CAattributes + " ]"
							+ "attributes are assigned to CA:- " + CA + " in the EventFolders:- " + EventFolderName);

				} else {
					correctiveActionConfig.setAttributes(null);
					CorrectiveAction.setConfig(correctiveActionConfig);
					logger.info(
							"Removing the attributes from the CA- " + CA + " in the EventFolders:- " + EventFolderName);
					TestReport.GetInstance().log(Status.PASS,
							"Removing the attributes from the CA- " + CA + " in the EventFolders:- " + EventFolderName);

				}

			}
			return true;

		} catch (Exception e) {
			logger.error("Failed to Assign Attributes [ " + CAattributes + " ]  to the CA" + CA
					+ "in the Event Folder: " + EventFolderName + " -Due to:-" + e);
			TestReport.GetInstance().log(Status.FAIL, "Failed to Assign Attributes [ " + CAattributes + " ]  to the CA"
					+ CA + "in the Event Folder: " + EventFolderName + " -Due to:-" + e);
			return false;
		}

	}

	private String assignCAAttributes(ICorrectiveAction CA, String[] CAattributes) {
		
		try {
			
			ICorrectiveAction CorrectiveAction = CA;
			CorrectiveActionConfig correctiveActionConfig = CorrectiveAction.getConfig();
			correctiveActionConfig.setAttributes(CAattributes);
			CorrectiveAction.setConfig(correctiveActionConfig);
			logger.info("CA attributes values are updated successfully");
			return "Normal End";
			

		} catch (Exception e) {
			logger.error("Failed to Assign Attributes Due to:-" + e);
			TestReport.GetInstance().log(Status.FAIL, "Failed to Assign Attributes Due to:-" + e);
			return "Failed due to"+e.getMessage().replaceAll(" ", "");
		}

	}

	public boolean assignCAAttributes(String CA, String CAattributes) {
		return assignCAAttributes(GlobalVariables.EventFolder, CA, CAattributes);
	}

		public boolean RestoreCAAttributes()   {
		try {
			if(Arrayof_CorrectiveAction!=null)
			{
		String appConfigfile=ReadPropertyFile.getInstance().getProperty("AppConfigPropertiespath").trim()+"//"+ReadPropertyFile.getInstance().getProperty("AppConfigPropertiesName").trim();
		String BackupFileName=ReadPropertyFile.getInstance(appConfigfile).getProperty("SIL_CAAttributeBackupFile").trim();
		Properties Pr=ReadPropertyFile.getInstance(BackupFileName).PropertyFile();
		String[] ArrayOfAttributes;
		for(ICorrectiveAction ca:Arrayof_CorrectiveAction)
		{
			String str=Pr.getProperty(ca.getConfig().getName());
			if(str!=null&&!str.isEmpty())
			ArrayOfAttributes = str.split("~#~");
			else
			ArrayOfAttributes=null;	
			assignCAAttributes(ca, ArrayOfAttributes);
		}
		TestReport.GetInstance().log(Status.PASS, "CA attributes values are restored successfully");
			}
			else
				TestReport.GetInstance().log(Status.PASS, "No valid CA's are selected to restore attributes");
		return true;
		}
		catch(CommunicationException|ServerException e)
		{
			logger.error("Failed to restore Attributes Due to:-" + e);
			TestReport.GetInstance().log(Status.FAIL, "Failed to restore Attributes Due to:-" + e);
			return false;
		}
	}

	public boolean ResetCAAttributes(String CA) {
		try {
			if(CA!=null&&!CA.isEmpty()&&!CA.equalsIgnoreCase("EMPTY"))
			{
			//	Helper.getInstance().ClearBackup_CAattributeFile();
				Arrayof_CorrectiveAction = getArrayofCorrectiveActions(CA);
				for (ICorrectiveAction ca : Arrayof_CorrectiveAction) {
					Helper.getInstance().Backup_CAAttributes(ca.getConfig().getName(), ca.getConfig().getAttributes());
					CorrectiveActionConfig CA_Config= ca.getConfig();
					CA_Config.setAttributes(null);
					ca.setConfig(CA_Config);
					logger.info("[ " + ca.getConfig().getName() + " ] attributes is reset to nil");
				}
				TestReport.GetInstance().log(Status.PASS, "[ "+CA + " ] CA's attributes are removed");
			}
			else 
			{
				TestReport.GetInstance().log(Status.PASS, "No valid CA's are selected to reset attributes");
				Arrayof_CorrectiveAction=null;
			}
			return true;
		} catch (CommunicationException | ServerException | InvalidConfigException | NotPermittedException e) {
			TestReport.GetInstance().log(Status.FAIL, "[ "+CA + " ] CA's attributes are not removed due to: "+ e);
			logger.warn("Failed to reset the attributes due to :- " + e);
		}
		return false;
	}

	public boolean AssignCorrectiveAction(String LDSName, String OrganizedFolder, String LSDFolderName,
			String ChannelName, String Startdate, String Enddate, String CA, String SampleIndex,
			String ViolationComment, String ExptMsg) {
		String Str = AssignCA(LDSName, OrganizedFolder, LSDFolderName, ChannelName, Startdate, Enddate, CA,
				SampleIndex, ViolationComment);
		if (Str.equalsIgnoreCase("NormalEnd")) {
			TestReport.GetInstance().log(Status.PASS, CA + "is assigned to the channel:" + ChannelName + " Samples");
			return true;
		} else if (Str.contains(ExptMsg)) {
			TestReport.GetInstance().log(Status.PASS,
					"[ " + Str + " ]" + ":-Exception message is dispaying For CA " + " [ " + CA + " ]");
			return true;
		} else {
			TestReport.GetInstance().log(Status.FAIL, CA + " is not assigned to Channel samples due to : " + Str);
			return false;
		}
	}

	public boolean AssignCorrectiveAction(String LDSName, String OrganizedFolder, String LSDFolderName,
			String ChannelName, String Startdate, String Enddate, String CA, String ViolationComment, String ExptMsg) {
		String Str = AssignCA(LDSName, OrganizedFolder, LSDFolderName, ChannelName, Startdate, Enddate, CA, "",
				ViolationComment);
		if (Str.equalsIgnoreCase("NormalEnd")) {
			TestReport.GetInstance().log(Status.PASS, CA + "is assigned to the channel:" + ChannelName + " Samples");
			return true;
		} else if (Str.toUpperCase().contains(ExptMsg.toUpperCase())) {
			TestReport.GetInstance().log(Status.PASS,
					"[ " + Str + " ]" + ":-Exception message is dispaying For CA " + " [ " + CA + " ]");
			return true;
		} else {
			TestReport.GetInstance().log(Status.FAIL, CA + " is not assigned to Channel samples due to : " + Str);
			return false;
		}
	}

	public boolean AssignCorrectiveAction(String LDSName, String OrganizedFolder, String LSDFolderName,
			String ChannelName, String Startdate, String Enddate, String CA, String ViolationComment) {
		return AssignCorrectiveAction(LDSName, OrganizedFolder, LSDFolderName, ChannelName, Startdate, Enddate, CA,
				"", ViolationComment, "N/A");

	}

	public boolean editControlLimits(String LDSName, String OrganizedFolder, String LSDFolderName,
			String ChannelName, String ControlLimits) {
		
			ISPCChannel iSPCChannel = getChannel(LDSName, OrganizedFolder, LSDFolderName, ChannelName);
			return editControlLimits(iSPCChannel,ControlLimits);
	}
	private boolean editControlLimits(ISPCChannel iSPCChannel, String ControlLimits) {
		try {
			if (iSPCChannel != null) {
				SPCChannelConfig sPCChannelConfig = iSPCChannel.getConfig();
				ControlLimitsConfig controlLimitsConfig = sPCChannelConfig.getControlLimitsConfig();
				controlLimitsConfig.setGlobalLimitsEnabled(true);
				String[] arrayOfString = ControlLimits.split("~#~");
				for (String str : arrayOfString) {
					if (str.contains("=")) {
						String[] arrayOfString1 = str.split("=");
						String[] arrayOfString2 = arrayOfString1[1].split(",");
						if ("mean".equalsIgnoreCase(arrayOfString1[0].trim())) {
							controlLimitsConfig.setLimitValue(0, Float.parseFloat(arrayOfString2[0].trim()));
							controlLimitsConfig.setLimitValue(1, Float.parseFloat(arrayOfString2[1].trim()));
							controlLimitsConfig.setLimitValue(2, Float.parseFloat(arrayOfString2[2].trim()));
						} else if ("sigma".equalsIgnoreCase(arrayOfString1[0].trim())) {
							controlLimitsConfig.setLimitValue(3, Float.parseFloat(arrayOfString2[0].trim()));
							controlLimitsConfig.setLimitValue(4, Float.parseFloat(arrayOfString2[1].trim()));
							controlLimitsConfig.setLimitValue(5, Float.parseFloat(arrayOfString2[2].trim()));
						} else if ("range".equalsIgnoreCase(arrayOfString1[0].trim())) {
							controlLimitsConfig.setLimitValue(6, Float.parseFloat(arrayOfString2[0].trim()));
							controlLimitsConfig.setLimitValue(7, Float.parseFloat(arrayOfString2[1].trim()));
							controlLimitsConfig.setLimitValue(8, Float.parseFloat(arrayOfString2[2].trim()));
						} else if ("raw".equalsIgnoreCase(arrayOfString1[0].trim())) {
							controlLimitsConfig.setLimitValue(24, Float.parseFloat(arrayOfString2[0].trim()));
							controlLimitsConfig.setLimitValue(25, Float.parseFloat(arrayOfString2[1].trim()));
							controlLimitsConfig.setLimitValue(26, Float.parseFloat(arrayOfString2[2].trim()));
						}
					} else {
						String[] arrayOfString1 = str.split(",");
						controlLimitsConfig.setLimitValue(0, Float.parseFloat(arrayOfString1[0].trim()));
						controlLimitsConfig.setLimitValue(1, Float.parseFloat(arrayOfString1[1].trim()));
						controlLimitsConfig.setLimitValue(2, Float.parseFloat(arrayOfString1[2].trim()));
					}
				}
				sPCChannelConfig.setControlLimitsConfig(controlLimitsConfig);
				iSPCChannel.setConfig(sPCChannelConfig, "By Automated tests", true);
				TestReport.GetInstance().log(Status.PASS,
						"[ " + ControlLimits + " ]" + "Control limits are added to the channel" + iSPCChannel.getConfig().getName());
				return true;
			} else {
				logger.warn("Channel not found! ");
				TestReport.GetInstance().log(Status.WARNING, " Channel not found! ");
			}

		} catch (Exception exception) {
			logger.error("Exception occured while editing the control limits: " + exception);
			TestReport.GetInstance().log(Status.FAIL, "Failed to Edit control limits due to :" + exception);

		}
		return false;
	}
	public boolean EnableControlLimits(String LDSName, String OrganizedFolder, String LSDFolderName,
			String ChannelName, String Flag) {
		try {
			ISPCChannel iSPCChannel = getChannel(LDSName, OrganizedFolder, LSDFolderName, ChannelName);
			boolean CL_Flag = true;
			if (Flag != null && !Flag.isEmpty() && Flag.equalsIgnoreCase("False"))
				CL_Flag = false;
			if (iSPCChannel != null) {
				SPCChannelConfig sPCChannelConfig = iSPCChannel.getConfig();
				ControlLimitsConfig controlLimitsConfig = sPCChannelConfig.getControlLimitsConfig();
				controlLimitsConfig.setGlobalLimitsEnabled(true);

				sPCChannelConfig.setControlLimitsConfig(controlLimitsConfig);
				iSPCChannel.setConfig(sPCChannelConfig, "By Automated tests", true);
				TestReport.GetInstance().log(Status.PASS,
						"Enabled Control Limits for the Channel [ " + ChannelName + "] is :- " + CL_Flag);
				logger.info("Enabled Control Limits for the Channel [ " + ChannelName + "] is :- " + CL_Flag);
				return true;
			} else {
				logger.warn(ChannelName + "Channel not found! ");
				TestReport.GetInstance().log(Status.WARNING, ChannelName + " Channel not found! ");
			}

		} catch (Exception exception) {
			logger.error("Exception occured while editing the control limits: " + exception);
			TestReport.GetInstance().log(Status.FAIL, "Failed to Edit control limits due to :" + exception);

		}
		return false;
	}

	public boolean CreateNewCorrectiveAction(String EventFolder, String CA_Name, String CA_Description,
			String CA_Author, String CA_Department, String CA_CostSection, String CA_Text, String[] CA_Attributes,
			String CA_Division) {
		try {
			IEventFolder event = GetEventFolder(EventFolder);
			if (GetRequiredCA(CA_Name) != null) {
				logger.info(CA_Name + ": Corrective is already created in the Event folder:- " + EventFolder);
				TestReport.GetInstance().log(Status.PASS,
						CA_Name + ": Corrective is already created in the Event folder:- " + EventFolder);
				return true;
			}
			ICorrectiveAction CA = event.getAllCorrectiveActions()[0];
			CorrectiveActionConfig CAconfig = CA.getConfig();
			CAconfig.setName(CA_Name);
			CAconfig.setDescription(CA_Description);
			CAconfig.setAuthor(CA_Author);
			CAconfig.setDepartment(CA_Department);
			CAconfig.setCostSection(CA_CostSection);
			CAconfig.setText(CA_Text);
			CAconfig.setAttributes(CA_Attributes);
			CAconfig.setOwner(ReadPropertyFile.getInstance().getProperty("SP_UsrName"));
			if (CA_Division != null && !CA_Division.isEmpty() && CA_Division.equalsIgnoreCase("EMPTY"))
				CAconfig.setDivision(CA_Division);
			event.createCorrectiveAction(CAconfig);

			logger.info(CA_Name + ": Corrective is created successfully in the Event folder:- " + EventFolder);
			TestReport.GetInstance().log(Status.PASS,
					CA_Name + ": Corrective is created successfully in the Event folder:- " + EventFolder);
			return true;
		} catch (CommunicationException | ServerException | InvalidConfigException | NotPermittedException e) {

			logger.error("Exception occured while Creating a new Corrective Action due to:- " + e);
			TestReport.GetInstance().log(Status.FAIL,
					"Exception occured while Creating a new Corrective Action due to:- " + e);
			return false;
		}
	}

	public boolean CreateNewCorrectiveAction(String CA_Name, String CA_Description, String CA_Author,
			String CA_Department, String CA_CostSection, String CA_Text, String CA_Attributes) {
		String[] CAAttrib = null;
		if (CA_Attributes != null && !CA_Attributes.isEmpty() && !CA_Attributes.equalsIgnoreCase("EMPTY"))
			CAAttrib = CA_Attributes.split("~#~");
		return CreateNewCorrectiveAction(GlobalVariables.EventFolder, CA_Name, CA_Description, CA_Author, CA_Department,
				CA_CostSection, CA_Text, CAAttrib, null);
	}

	public boolean UpdateCustomField(String LDSName, String OrganizedFolder, String LSDFolderName,
			String ChannelName,String CustomValues)
	{
		ISPCChannel iSPCChannel = getChannel(LDSName, OrganizedFolder, LSDFolderName, ChannelName);
		return UpdateCustomField(iSPCChannel,CustomValues);
	}
	private boolean UpdateCustomField(ISPCChannel iSPCChannel, String CustomValues) {
		try {
			if (iSPCChannel != null && CustomValues != null) {
				SPCChannelConfig sPCChannelConfig = iSPCChannel.getConfig();
				String[] ArrayofCustomvalues = CustomValues.split("\n|\n\r|~#~");
				CustomerField[] attachCustomField = new CustomerField[ArrayofCustomvalues.length];
				CustomerField[] AllCustomField = SPCChannelConfig.getAllCustomerFields();

				int temp = -1;
				for (String str : ArrayofCustomvalues) {
					temp++;
					for (CustomerField CustmFld : AllCustomField) {
						if (CustmFld.getName().equalsIgnoreCase(str.split("=")[0])) {
							attachCustomField[temp] = CustmFld;
							SPACE sPACE;
							try {
								sPACE = SPACE.valueOf(str.split("=")[2].replaceAll(" ", "").toUpperCase().trim());
							} catch (Exception exception) {
								sPACE = SPACE.DEFAULT;
							}
							switch (sPACE) {
							case DOUBLE:
								attachCustomField[temp].setValue(Double.valueOf(Double.parseDouble(str.split("=")[1])));
								break;
							case INT:
								attachCustomField[temp].setValue(Integer.valueOf(Integer.parseInt(str.split("=")[1])));
								break;
							case DEFAULT:
								attachCustomField[temp].setValue(str.split("=")[1]);
								break;
							default:
								break;
								
							}
							

						}
					}

				}
				sPCChannelConfig.setAttachedCustomerFields(attachCustomField);
				iSPCChannel.setConfig(sPCChannelConfig,"Updated by Auto Script", true);
				logger.info("[ "+CustomValues +" ] Custom fields are updated to the channel:- "+ iSPCChannel.getConfig().getName());
				TestReport.GetInstance().log(Status.PASS, "[ "+CustomValues +" ] Custom fields are updated to the channel:- "+ iSPCChannel.getConfig().getName());
				return true;
			} else {
				logger.warn("Channel is not found! ");
				TestReport.GetInstance().log(Status.FAIL, " Channel not found! ");
			}

		} catch (Exception exception) {
			logger.error("Exception occured while updating the Custom fields;- " + exception);
			TestReport.GetInstance().log(Status.FAIL,
					"Exception occured while updating the Custom fields:-  " + exception);
		}
		return false;

	}
	private boolean UpdateChannelState(ISPCChannel iSPCChannel, String Channel_State) {
		try {
			if (iSPCChannel != null) {
				SPCChannelConfig sPCChannelConfig = iSPCChannel.getConfig();
				SPCChannelState[] ListofChannelState = SPCChannelConfig.getAvailableSPCChannelStates2();
				for (SPCChannelState state : ListofChannelState)
					if (state.getState().equalsIgnoreCase(Channel_State)) {
						sPCChannelConfig.setCurrentSPCChannelState(state);
						iSPCChannel.setConfig(sPCChannelConfig, "By Automated tests", true);
						TestReport.GetInstance().log(Status.PASS,
								" [ " + iSPCChannel.getConfig().getName() + "]  channel udpated with Channel State :- " + Channel_State);
						logger.info(
								" [ " + iSPCChannel.getConfig().getName() + "]  channel is updated with Channel State :- " + Channel_State);
						return true;

					}
				TestReport.GetInstance().log(Status.WARNING,
						Channel_State + " Channel state is not found in channel state list");
				logger.warn(Channel_State + " Channel state is not found in channel state list");
				return false;
			} else {
				logger.warn("Channel is not found! ");
				TestReport.GetInstance().log(Status.FAIL,  " Channel not found! ");
			}

		} catch (Exception exception) {
			logger.error("Exception occured while updating the channel state;- " + exception);
			TestReport.GetInstance().log(Status.FAIL,
					"Exception occured while updating the channel state;- " + exception);

		}
		return false;
	}
	public boolean UpdateChannelState(String LDSName, String OrganizedFolder, String LSDFolderName,
			String ChannelName, String Channel_State) {
			ISPCChannel iSPCChannel = getChannel(LDSName, OrganizedFolder, LSDFolderName, ChannelName);
	return	UpdateChannelState(iSPCChannel,Channel_State);
	}
	private String UpdateChannelSettings(ISPCChannel spcchannel,String SettingsValues) throws CommunicationException, ServerException
	{
		SPCChannelConfig spcconfig = spcchannel.getConfig();
		SPCChannelSettingsConfig settingconfig = spcconfig.getSPCChannelSettingsConfig();
		System.out.println(settingconfig.getCollectionInterval());
		System.out.println(settingconfig.getSamplesOffline());
		System.out.println(settingconfig.getViolTemplateId());
		System.out.println(settingconfig.isAutocreateCKCs()+" autocreateckc");
		System.out.println(settingconfig.isInheritAutocreateCKCs()+" Inheritedckc");
		System.out.println(settingconfig.getControlLimitMode()+" control limited mode");
		System.out.println(settingconfig.getSamplesInChart()+" control limited mode");
		
		return "pass";
	}
	public boolean updatechannelsetting(String LDSName, String OrganizedFolder, String LSDFolderName,
			String ChannelName, String Channel_State)  
	{
		try {
		ISPCChannel iSPCChannel = getChannel(LDSName, OrganizedFolder, LSDFolderName, ChannelName);
		 UpdateChannelSettings(iSPCChannel,Channel_State);
		 return true;
	}
		catch(Exception e)
		{
			System.out.println(e);
		return false;	
		}
		}
	public static void main(String args[])
			throws CommunicationException, ServerException, InvalidConfigException, NotPermittedException {

	}

	private enum SPACE {
		DOUBLE, EXTERNALCOMMENT, INT, INTERNALCOMMENT,INTERNALFLAG,EXTERNALFLAG, MAX, MEAN, MIN, MEDIAN, MAXVALUE, MEANVALUE, MINVALUE,
		MEDIANVALUE, CKCMEANUCL, CKCMEANCENTER, CKCMEANLCL, CKCRAWUCL, CKCRAWCENTER, CKCRAWLCL, SIGMAVALUE, SAMPLESIZE,
		UPPERSPEC, SPECTARGET, LOWERSPEC, RAWVALUE, CKCVIOLATIONTEXTS, VIOLATIONTEXT, CKCVIOLATIONCOMMENT,
		VIOLATIONCOMMENT, DEFAULT,LOT,WAFER,PTOOL,PCHAMBER,TECHNOLOGY,ROUTE,PRODUCTGROUP,PRODUCTEC,POPERATIONID,EVENT,PHOTOLAYER,RESERVE1
		,RESERVE2,RESERVE3,RESERVE4,RESERVE5,RESERVE6,RESERVE7,RESERVE8,DATRESERVE1,DATRESERVE2,DATRESERVE3,DATRESERVE4,DATRESERVE5,DATRESERVE6,DATRESERVE7,DATRESERVE8,
		MTIME,MTOOL,MOPERATIONID,SITEID,X,Y,FOUP,RETICLE,MSLOT,SUBLOTTYPE,PSEQUENCE,AUTOFLAGLOTTYPE;
	}

}
